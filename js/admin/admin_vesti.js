$(document).ready(function(){

	$("input[name='news_img']").on("change", function(){
		if (this.files && this.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('.news-pic').attr('src', e.target.result);
        }
        reader.readAsDataURL(this.files[0]);
    	}
	});

     //Kategorije vesti
    $('input[name="kategorijaVesti"]').click(function(){
        if($('#JSListaKategorijeVesti').attr('hidden') == 'hidden'){
            $('#JSListaKategorijeVesti').removeAttr('hidden');
        }else{
            $('#JSListaKategorijeVesti').attr('hidden','hidden');   
        }   
    });
    $('.JSKategorijaVesti').click(function(){
        var kategorija = $(this).data('kategorija');
        $('input[name="kategorijaVesti"]').val(kategorija);
        $('#JSListaKategorijeVesti').attr('hidden','hidden');   
    });
    $(document).on('click', function(e){
        var target = $(e.target);
        if(!target.is($('input[name="kategorijaVesti"], #JSListaKategorijeVesti'))) { 
            $('#JSListaKategorijeVesti').attr('hidden','hidden')();
        } 
    }); 

});