$(document).ready(function(){
	$('#JSRegToggle').click(function(){
		if($('#JSRegToggleSec').attr('hidden') == 'hidden'){
			$('#JSRegToggleSec').removeAttr('hidden');
		}else{
			$('#JSRegToggleSec').attr('hidden','true');
		}
	});

	$('#JSAddCartSubmit').click(function(){
		$(this).attr('disabled',true);
		$('#JSAddCartForm').submit();
	});
	
	$(document).on('click','.product-count-minus',function(){
		var obj = $(this);
		var roba_id = $(this).data('roba_id');
		var web_b2c_korpa_stavka_id = $(this).data('web_b2c_korpa_stavka_id');

		$trenutna_kolicina = obj.parent().find('.round-quantity').val();
		if($trenutna_kolicina > 1) {
			$.post(base_url+'list-mini-cart-minus',{roba_id: roba_id, web_b2c_korpa_stavka_id: web_b2c_korpa_stavka_id },function(response){
				var results = $.parseJSON(response);
	
				obj.parent().find('.round-quantity').val(results);
				$('.JScart_num').text(results);	
			})
		}

	})

	$(document).on('click','.product-count-plus',function(){
		var obj = $(this);
		var roba_id = obj.data('roba_id');
		var web_b2c_korpa_stavka_id = obj.data('web_b2c_korpa_stavka_id');

		$.post(base_url+'list-mini-cart-plus',{roba_id: roba_id, web_b2c_korpa_stavka_id: web_b2c_korpa_stavka_id },function(response){
			if(response == -1) {
				bootboxDialog({ message: "<p>" + trans('Tražena količina nije dostupna') + ".</p>" }); 
			} else {
				obj.parent().find('.round-quantity').val(response);
				$('.JScart_num').text(response);	
			}
		})
	})



	$(document).on('click','.JSadd-to-cart',function(){
		var obj = $(this),
		    roba_id = obj.data('roba_id');

		$.post(base_url+'list-cart-add',{roba_id: roba_id},function(response){
	  
			bootboxDialog({ message: "<p>" + trans('Artikal je dodat u korpu') + ".</p>" }); 

			var results = $.parseJSON(response);
			
			if(parseInt(results.check_available) == 0){
				obj.after(
					`<button>
						<img src="${base_url + 'images/cart.svg'}" alt="not available" width="20" height="20" style='filter: contrast(0.2)' />
						<span class="tooltiptext" style="font-weight: 400;"> ${trans('Nije dostupno')} </span>
					</button>`
				)
				obj.remove();
			}
			$('.JSheader-cart-content').html(results.mini_cart_list);
			$('.JScart_num').text(results.broj_cart);	
			$('.header-cart-total').html('rsd. ' + results.cart_ukupno);
			if($('#livemode').val() == 1){
				gtagManagerAddToCart(results.article_details);
			}		

		});

		// Blink cart when is added to cart
		sessionStorage.setItem('blinkCart', '1');
		if(sessionStorage.getItem('blinkCart')) {
			$('.header-cart > .cart-me').addClass('blink_cart')
		}
		setTimeout(() => {
			$('.header-cart > .cart-me').removeClass('blink_cart');
			sessionStorage.removeItem('blinkCart');
		}, 4000)
	});


	$('.JSadd-to-cart-similar').click(function(){
		var obj = $(this),
		    roba_id = obj.data('roba_id'),
		    vezani_roba_id = obj.data('vezani_roba_id'),
		    kolicina = 1,
		    message = '';

		$.post(base_url+'vezani-cart-add',{roba_id: roba_id, vezani_roba_id: vezani_roba_id, kolicina: kolicina},function(response){
			var results = $.parseJSON(response); 
			if(results.success){ 
  
				message = "Artikal je dodat u korpu";
			 
				if(results.check_available <= 0){
					obj.after('<button class="button not-available">'+trans("Nije dostupno")+'</button>');
					obj.remove();			
				}
				$('.JSheader-cart-content').html(results.mini_cart_list);
				$('.JScart_num').text(results.broj_cart);
				if($('#livemode').val() == 1){
				gtagManagerAddToCart(results.article_details);
				}	
			} else {

				if(results.exists == 0){ 

					message = "Tražena količina nije dostupna";

				}else{ 

					message = "Artikal je već dodat u korpu";

				}
			}
			  
			bootboxDialog({ message: "<p>" + trans(message) + ".</p>" }); 

		});  
	});

	$('.JScart-less, .JScart-more, .JScart-less-gram, .JScart-more-gram').click(function(){
		var obj = $(this),
		    stavka_id = obj.data('stavka_id'),
		    kolicina_temp = obj.closest('td').find('.JScart-amount').val(),
		    kolicina,
		    message = '';

		if(obj.attr('class') == 'JScart-less'){
			kolicina = parseInt(kolicina_temp) - 1;
		}
		else if(obj.attr('class') == 'JScart-more'){
			kolicina = parseInt(kolicina_temp) + 1;
		}
		else if(obj.attr('class') == 'JScart-less-gram'){
			kolicina = (parseFloat(kolicina_temp) - 1/10).toFixed(2);
		}
		else if(obj.attr('class') == 'JScart-more-gram'){			
			kolicina = (parseFloat(kolicina_temp) + 1/10).toFixed(2);
		}

		if(kolicina > 0){		
			$.post(base_url+'cart-add-sub',{stavka_id: stavka_id, kolicina: kolicina},function(response){
				var results = $.parseJSON(response);
				if(results.changed){
					obj.closest('td').find('.JScart-amount').val(kolicina);					
					$('.JScart-item-price').html(results.cart_item_ukupno);

					console.log(obj.closest('ul'));					

					$('.JSdelivery_total_amount').html(results.cart_ukupno);
					$('.JSUkupno_sa_dostavom').html(results.cart_item_ukupno_dostava);
					$('.JStotal_amount_weight').html(results.cart_item_ukupno_tezina);
					$('.JSheader-cart-content').html(results.mini_cart_list);
					$('.JScart_num').text(results.broj_cart);
					$('#JSAchievePoints').text(results.bodovi_ostvareni_bodovi_korpa);
					$('#JSMaxUsingPoints').text(results.bodovi_popust_bodovi_korpa);
					$('#JSMaxUsingVoucherPrice').html(results.vauceri_popust_cena_korpa);
					$('.JSexpenses').html(results.troskovi_isporuke_valuta);

					if(results.troskovi_isporuke>0){
						$('.JSdelivery_total_amountParent').removeAttr('hidden');
						$('.JSDelivery').removeAttr('hidden');
					}else{
						$('.JSdelivery_total_amountParent').attr('hidden','hidden');
						$('.JSDelivery').attr('hidden','hidden');						
					}

					for (var i=0; i < results.vezane_stavke.length; i++) {
						var vezani_id = results.vezane_stavke[i].web_b2c_korpa_stavka_id;
						var vezani_kolicina = parseFloat(results.vezane_stavke[i].kolicina).toFixed(0);
						$('.JScart-amount[data-stavka_id='+vezani_id+']').val(vezani_kolicina);
						$('.JScart-item-price[data-stavka_id='+vezani_id+']').html(results.vezane_stavke[i].vezani_item_cena);
					}
 
					message = "Količina je promenjena";

				} else {

					message = "Tražena količina nije dostupna";
			 	}
  
				bootboxDialog({ message: "<p>" + trans(message) + ".</p>" }); 
				if(results.changed && $('#livemode').val() == 1){
					if(results.article_details.quantity > 0){
						gtagManagerAddToCart(results.article_details);
					}else{
						gtagManagerRemoveFromCart([results.article_details]);
					}
				}
			});

		} else {  
 
			bootboxDialog({ message: "<p>" + trans('Količina ne sme biti manja od 1') + ".</p>" }); 

		}  
	});


	$('.JScart-amount').keyup(function(){
		var obj = $(this),
		    stavka_id = obj.data('stavka_id'),
		    kolicina = $(this).val(),
		    message = '';
		
		if(kolicina > 0){		
			$.post(base_url+'cart-add-sub',{stavka_id: stavka_id, kolicina: kolicina},function(response){
				var results = $.parseJSON(response);
				if(results.changed){
					obj.closest('td').find('.JScart-amount').val(kolicina);					
					$('.JScart-item-price').html(results.cart_item_ukupno);					

					$('.JSdelivery_total_amount').html(results.cart_ukupno);
					$('.JSUkupno_sa_dostavom').html(results.cart_item_ukupno_dostava);
					$('.JStotal_amount_weight').html(results.cart_item_ukupno_tezina);
					$('.JSheader-cart-content').html(results.mini_cart_list);
					$('.JScart_num').text(results.broj_cart);	
					$('#JSAchievePoints').text(results.bodovi_ostvareni_bodovi_korpa);
					$('#JSMaxUsingPoints').text(results.bodovi_popust_bodovi_korpa);
					$('#JSMaxUsingVoucherPrice').html(results.vauceri_popust_cena_korpa);


					message = "Količina je promenjena";
				 	if(results.article_details.quantity > 0){
						gtagManagerAddToCart(results.article_details);
					}else{
						results.article_details.quantity = -results.article_details.quantity;
						gtagManagerRemoveFromCart([results.article_details]);
					}
				} else {
					
					message = "Tražena količina nije dostupna";
					 
				}
  
				bootboxDialog({ message: "<p>" + trans(message) + ".</p>" }); 

			});
		
		} else {
 
			bootboxDialog({ message: "<p>" + trans('Količina ne sme biti manja od 1') + ".</p>" }); 

 		}
	});


	$(document).on('click','.JSdelete_cart_item',function(){
		var stavka_id = $(this).data('stavka_id');

		bootbox.confirm({
            message: "<p>"+trans('Artikal će biti uklonjen iz korpe. Da li ste sigurni')+"?</p>",
            buttons: {
                cancel: {
                    label: trans('Ne')
                },
                confirm: {
                    label: trans('Da')
                }
            },
            callback: function (result) {  
        		if(result){
					$.post(base_url+'cart-stavka-delete',{stavka_id: stavka_id},function(response){ 
         
						bootboxDialog({ message: "<p>" + trans('Artikal je uklonjen iz korpe') + ".</p>" }, 2200); 

					    setTimeout(function(){
							location.reload(); 
						}, 600);  
					}); 
                }  
            }
        });
	});
	

	$('#JSDeleteCart').click(function(){
		bootbox.confirm({
            message: "<p>"+trans('Da li ste sigurni da želite da ispraznite korpu')+"?</p>",
            buttons: {
                cancel: {
                    label: trans('Ne')
                },
                confirm: {
                    label: trans('Da')
                }
            },
            callback: function (result) {
                if(result){
					$.post(base_url+'cart-delete',{},function(response){  
			  
						bootboxDialog({ message: "<p>" + trans('Korpa je ispražnjena') + ".</p>" }, 2200); 

						setTimeout(function(){
							location.reload(); 
						}, 600); 
					});
                }
            }
        });
	});


	$(document).on('click','.JSnot_logged',function(){
  
		bootboxDialog({ message: "<p>" + trans('Da biste dodali proizvod na listu želja, morate biti ulogovani') + ".</p>", closeButton: true }, 5000 ); 

	});


	$(document).on('click','.JSadd-to-wish',function(){ 
		var roba_id = $(this).data('roba_id');

		$.post(base_url+'wish-list-add',{roba_id: roba_id},function(response){
			var results = $.parseJSON(response);

			bootboxDialog({ message: "<p>" + trans(results.message) + "</p>" }); 

			$('.JSbroj_wish').text(results.broj_wish);
		});
	});


	$(document).on('click','.JSukloni',function(){
		var roba_id = $(this).data('roba_id');

		bootbox.confirm({
            message: "<p>"+trans('Artikal će biti uklonjen sa liste želja. Da li ste sigurni')+"?</p>",
            buttons: {
                cancel: {
                    label: trans('Ne')
                },
                confirm: {
                    label: trans('Da')
                }
            },
            callback: function (result) {
                if(result){
					$.post(base_url+'wish-list-delete',{roba_id: roba_id},function(response){
					  
						bootboxDialog({ message: "<p>" + trans('Artikal je uklonjen iz liste želja') + ".</p>" }); 
						
						setTimeout(function(){
							location.reload(); 
						}, 600);
					});
                }
            }
        });
	});

	$('.JSdeliveryInput').on('change', function(){
		// vrednost dostave
		var deliv_value = $('.JSexpenses');
		var deliv_value_repl = (deliv_value.text().replace(/[^\d]/g,'')/100);
		// ukupno
		var value = $('.JStotal_amount');
		var val2 = value.text();
		// cena arrtikla
		var art_price =  $('.JSdelivery_total_amount');
		var art_price_repl = (art_price.text().replace(/[^\d]/g,'')/100);
		var dec = art_price.text().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		// cena sa dostavom
		var original = art_price_repl+deliv_value_repl;		
		var original2 = new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'rsd' }).format(original);
		var original3 =original2.replace('RSD','rsd.');
		
		if($(this).val() == 2){ 
			$('.JSDelivery').hide();
			value.text(dec); 
		}else{ 
			$('.JSDelivery').show();
			value.text(original3);
		} 
	});

	
	$('.JS_total_price').on('DOMSubtreeModified', function(){
		var price = $(this).text(),
			divs = $(this).parent().prevAll('.JS_price_adds'),
			free_delivery = $(this).nextAll('.JS_free_del'), 
			original_price = $(this).next().data('dostava');
 
	 	if (price > original_price) { 
			$(this).hide();
			divs.hide(); 
			free_delivery.removeAttr('hidden');
	 	}else{
	 		$(this).show();
			divs.show(); 
			free_delivery.attr('hidden', 'hidden'); 
	 	}
		// var ukupno_repl = (ukupno.replace(/[^\d]/g,'')/100);
		// var dostava = $(this).data('dostava');
		// var JS_free_del = $('.JS_free_del'); 
		// var razlika = ukupno_repl-dostava;
		// console.log(dostava) 
		// if(razlika < 0){
		// $('.JSfree_delivery').hide(); 
		// }else{
		// $('.JSfree_delivery').show();  
		// } 
	});
 

	if($('select[name="web_nacin_placanja_id"]').val() == 3){
		$('#JSCaptcha').removeAttr('hidden');
	}
	$(document).on('change','select[name="web_nacin_placanja_id"]',function(){
		if($(this).val() == 3){
			$('#JSCaptcha').removeAttr('hidden');
		}else{
			$('#JSCaptcha').attr('hidden','hidden');
		}
	});
 

});

function gtagManagerAddToCart(productObj) {
	window.dataLayer = window.dataLayer || [];
	window.dataLayer.push({
	  'event': 'eec.add',
  	  'ecommerce': {
  	    'currencyCode': productObj.currency,
  	    'add': {
  	      'products': [{
  			'name': productObj.name,
            'content_type': 'product_group',
			"item_group_id": productObj.category_id,
  			'id': parseInt(productObj.id),
  			'price': productObj.price,
  			'brand': productObj.brand,
  			'category': productObj.category,
  			"variant": "Standard",
  	        'quantity': productObj.quantity
  	       }]
  	    }
  	  },
  	  'eventCallback': function() {
  
  	  }
	});
}

function gtagManagerRemoveFromCart(productObjects) {
	if(productObjects.length > 0){	
		var products = [];
		for (var i = 0; i < productObjects.length; i++) {
			products.push({
	  			'name': productObjects[i].name,
                'content_type': 'product_group',
				"item_group_id": productObjects[i].category_id,
	  			'id': parseInt(productObjects[i].id),
	  			'price': productObjects[i].price,
	  			'brand': productObjects[i].brand,
	  			'category': productObjects[i].category,
	  			"variant": "Standard",
	  	        'quantity': -productObjects[i].quantity
			});
		}

		window.dataLayer = window.dataLayer || [];
		window.dataLayer.push({
		  'event': 'eec.remove',
	  	  'ecommerce': {
	  	    'currencyCode': productObjects[0].currency,
	  	    'remove': {
	  	      'products': products
	  	    }
	  	  },
	  	  'eventCallback': function() {

	  	  }
		});
	}
}
function validate(evt) {
  var theEvent = evt || window.event;
  var key = theEvent.keyCode || theEvent.which;
  key = String.fromCharCode( key );
  var regex = /[0-9]|\./;
  if( !regex.test(key) ) {
    theEvent.returnValue = false;
    if(theEvent.preventDefault) theEvent.preventDefault();
  }
} 
 