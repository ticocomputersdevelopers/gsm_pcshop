
/*======= FIXED HEADER ==========*/ 
var header = $('header'),
	header_height = $('header').height(),
	offset_top = $('header').offset().top,
	offset_bottom = offset_top + header.outerHeight(true),
	height_sticky_header = $('#JSfixed_header').height(),
	outer_height_sticky_header = $('#JSfixed_header').height(),
	offset_bottom_sticky_header = $('#JSfixed_header').offset().top,
	admin_menu = $('#admin-menu');


$(window).scroll(function(){
 
 	if ($(window).width() > 1024 ) {
 
	  	if ($(window).scrollTop() >= offset_bottom) {
			$('.JSsticky_menu').css({
				'top': offset_bottom_sticky_header + 10 + 'px',
				'background': '#fff',
				'z-index': 10,
				'padding-top': '40px'
			})

	  		if(admin_menu.length){ 
				$('.JSsticky_header').css('top', admin_menu.outerHeight() + 'px'); 
				$('.JSsticky_menu').css({
					'top': outer_height_sticky_header + 5 + 'px',
					'background': '#fff',
					'z-index': 10,
					'padding-top': '20px'
				})
			}

			$('#JSfixed_header').addClass('JSsticky_header');
			$('#JSsticky_menu').addClass('JSsticky_menu');

			if($('#JSfixed_header').hasClass('JSsticky_header')) {
				$('.menu-background .JSlevel-1').fadeOut('100');
			} else {
				$('.menu-background .JSlevel-1').fadeIn('100');
			}


			header.css('height', header_height + 'px');	        

	    }else {

			$('.JSsticky_menu').css({
				'top': 'unset',
				'background': '#fff',
				'z-index': 10,
				'padding-top': '0px'
			})
			$('#JSfixed_header').removeClass('JSsticky_header');  
			$('#JSsticky_menu').removeClass('JSsticky_menu');

			
			if($('#JSfixed_header').hasClass('JSsticky_header')) {
				$('#start-page .JSlevel-1').fadeOut('500');
				$('.menu-background .JSlevel-1').removeClass('active-sticky-category');
				$('.stickyOverlay').removeClass('active');
			} else {
				$('#start-page .JSlevel-1').fadeIn('500');
				$('.stickyOverlay').removeClass('active');
				$('.menu-background .JSlevel-1').removeClass('active-sticky-category');
			}

			header.css('height', '');
	    }
	}
});    
 
 