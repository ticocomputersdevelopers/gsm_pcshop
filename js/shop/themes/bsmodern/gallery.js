$(document).ready(function () {
    if ($('.JSgallery_slick img').length){ 
        
        $('img').on('error', function(){ 
            $(this).attr('src', '/images/no-image.jpg').addClass('img-responsive');
        });

        // GALLERY SLICK CUSTOM, BECAUSE SLICK MAKE PROBLEM, BECAUSE MODAL IS DISPLAY NONE
       var  img_gallery_img = $('.modal-gallery-img').find('img'),
            main_img = $('.JSmain_quickimg'),
            img_arr = [], 
            next_prev = 0;

        img_gallery_img.each(function(i, img){
            // FOR START
            if(i == 0) { $(img).addClass('JSquick_active') }
            img_arr.push($(img).attr('src'));  
            $(img).on('click', function(){
                main_img.attr('src', $(img).attr('src'));
                // FOR INDEX TRACKING
                if( main_img.attr('src') ===  $(img).attr('src')) {
                    next_prev = img_gallery_img.index(this);
                    $(img).addClass('JSquick_active').closest('div').siblings().find('img').removeClass('JSquick_active');
                }
            })
        });

        // FOR START
        main_img.attr('src', img_arr[0]);

        if(img_arr.length > 1) {
            $('.JSleft_btn, .JSright_btn').removeClass('hidden');
        }

        $(document).on('click','.JSleft_btn',function(){
            if (next_prev == 0) {
              next_prev = img_gallery_img.length -1; 
            } else {
              next_prev --;
              main_img.attr('src', img_arr[next_prev]);
            } 
            main_img.attr('src', img_arr[next_prev]);

            img_gallery_img.each(function(i, img){
                // FOR INDEX TRACKING
                if( main_img.attr('src') ===  $(img).attr('src')) {
                    next_prev = img_gallery_img.index(this);
                    $(img).addClass('JSquick_active').closest('div').siblings().find('img').removeClass('JSquick_active');
                }
            });
        });  
    
        $(document).on('click','.JSright_btn',function(){
            if (next_prev != img_gallery_img.length -1) {
                next_prev++; 
              } else {
                next_prev = 0;
            } 
            main_img.attr('src', img_arr[next_prev]);

            img_gallery_img.each(function(i, img){
                // FOR INDEX TRACKING
                if( main_img.attr('src') ===  $(img).attr('src')) {
                    next_prev = img_gallery_img.index(this);
                    $(img).addClass('JSquick_active').closest('div').siblings().find('img').removeClass('JSquick_active');
                }
            });
        });  
    }
    else {
        $('.JSgallery_slick').hide();
        $('.JS-quick-no-image').removeClass('hidden').addClass('flex');
    }
});