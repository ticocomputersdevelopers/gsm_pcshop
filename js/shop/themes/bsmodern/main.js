
$(document).ready(function () {
	// GALLERY SLIDER
	(function(){
		var img_gallery_href = $('.JSimg-gallery'),
			img_gallery_img = img_gallery_href.children('img'),
			main_img = $('.JSmain_img'),
			fullscreenBtn = $('.fullscreen-btn'),
			modal_img = $('#JSmodal_img'),
			modal = $('.JSmodal'),

			img_arr = [], 
			next_prev = 0;

		img_gallery_href.eq(0).addClass('galery_Active');

		img_gallery_href.on('click', function(){
			var img = $(this).children('img');
			main_img.attr({ src: img.attr('src'), alt: img.attr('alt') });
			$(this).addClass('galery_Active').siblings().removeClass('galery_Active');
		});

		main_img.on('click', function(){
			var src = $(this).attr('src');
			if (src.toLowerCase().indexOf('no-image') <= 0){
				modal_img.attr('src', src);

				$('.JSmodal').show();

				//IF MAIN PHOTO (WHEN CLICKED ON IT) IS EQUAL TO ANY OF THE THUMBNAILS, FIND HER
				// ORDINAL NUMBER AND THEN ADD THAT NUMBER TO THE NEXT PREV

				img_gallery_img.each(function(i, img){
					img_arr.push($(img).attr('src'));  
					if( src ===  $(img).attr('src')) {
						next_prev = img_gallery_img.index(this);
					}
					
				});
			} 
		});

		fullscreenBtn.on('click', function(){
			var src = $('#JSmain-img').attr('src');
			if (src.toLowerCase().indexOf('no-image') <= 0){
				modal_img.attr('src', src);

				$('.JSmodal').show();

				//IF MAIN PHOTO (WHEN CLICKED ON IT) IS EQUAL TO ANY OF THE THUMBNAILS, FIND HER
				// ORDINAL NUMBER AND THEN ADD THAT NUMBER TO THE NEXT PREV

				img_gallery_img.each(function(i, img){
					img_arr.push($(img).attr('src'));  
					if( src ===  $(img).attr('src')) {
						next_prev = img_gallery_img.index(this);
					}
					
				});
			} 
		})

		$('.JSclose-modal').on('click', function(){
			modal.hide();
		});

		$(document).on('click', function(){
			if (modal.css('display') == 'block')  {
				$('body').css('overflow', 'hidden');
			} else {
				$('body').css('overflow', '');  
			} 
		});

		modal.on('click', function(e){
			if ( ! $('.JSmodal img, .JSmodal .btn, .JSmodal .btn > *').is(e.target)) {
				modal.hide();
				modal_img.attr('src', '');
			} 
		}); 

		img_gallery_img.each(function(i, img){
			img_arr.push($(img).attr('src'));  
		});


		$(document).on('click','.JSleft_btn',function(){
			if (next_prev == 0) {
				next_prev = img_gallery_img.length -1; 
			} else {
				next_prev --;
				modal_img.attr('src', img_arr[next_prev]);
			} 
			modal_img.attr('src', img_arr[next_prev]);
		});  
		
		$(document).on('click','.JSright_btn',function(){

			if (next_prev != img_gallery_img.length -1) {
				next_prev++; 
				} else {
				next_prev = 0;
			} 
			modal_img.attr('src', img_arr[next_prev]);

		});  


		if (img_gallery_href.length > 1) {
			$('.modal-cont .btn').show();
		}

	}());

// LAZY LOAD IMAGES
	$(function() {
		// change data-src to src to disable plugin
		// 1.product_on_grid
		// 2.product_on_list
		// 3.footer
        $('.JSlazy_load').Lazy({
        	scrollDirection: 'vertical'
        });
    });

	  // IF THERE IS NO IMAGE
	$('img').on('error', function(){ 
		$(this).attr('src', '/images/no-image.jpg');
	});
   
	// Facebook
  window.fbAsyncInit = function() {
    FB.init({
      appId            : '163422577814443',
      autoLogAppEvents : true,
      xfbml            : true,
      version          : 'v3.0'
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
 
 // if($('body').is('#start-page') || $('body').is('#artical-page')){		// START PAGE ==============
  
   // PREHEADER SLIDER TEXT
   var currentIndex = 0;
   var items = $(".JSpreheaderSliderText p");

   function showItem(index) {
	items.hide();
	items.eq(index).fadeIn();
   }

   function nextItem() {
	currentIndex = (currentIndex + 1) % items.length;
	showItem(currentIndex);
   }

    function prevItem() {
		currentIndex = (currentIndex - 1 + items.length) % items.length;
		showItem(currentIndex);
	}

//    setInterval(nextItem, 8000);

   showItem(currentIndex);

	$('.next').click(function() {
		nextItem();
	});

	$('.prev').click(function() {
		prevItem();
	});

   $('.close-preheader').on('click', function() {
	$('.preheader-slider-text').hide();
   })

	// SLICK SLIDER INCLUDE
	if($('.JSmain-slider')[0]) {  
		if($('#admin_id').val()){
			$('.JSmain-slider').slick({
				autoplay: false,
				draggable: false,
				dots: true
			});
		}else{
			$('.JSmain-slider').slick({
				autoplay: true,
				dots: true
			});
		} 
	}

 
	// PRODUCTS SLICK SLIDER 
	 if ($('.JSproducts_slick')[0]) { 
		$('.JSproducts_slick').slick({ 
			autoplay: true, 
			slidesToShow: 5,
			slidesToScroll: 1, 
			responsive: [
				{
					breakpoint: 1240,
					settings: {
					  slidesToShow: 4,
					  slidesToScroll: 1
					}
				  },
				{
				  breakpoint: 1160,
				  settings: {
					slidesToShow: 3,
					slidesToScroll: 1
				  }
				},
				{
				  breakpoint: 880,
				  settings: {
					slidesToShow: 2,
					slidesToScroll: 1
				  }
				},
				{
				  breakpoint: 480,
				  settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				  }
				}
			]
		}); 
	 }


	$('.JSproduct-gallery').each(function(i) {
        $(this).find('.img-gallery').each(function(i) {
            $(this).attr('JSgallery-index', i);
        });
    });

	if ($('.JSproduct-main-slick').length){ 
		$('.JSproduct-main-slick').slick({ 
			autoplay: false,
			slidesToShow: 1,
			draggable: true,
			asNavFor: $('.JSproduct-gallery-slick'),
			slidesToScroll: 1
		}); 
	}
	
	if($(window).width() < 768) {
		if ($('.JSproduct-gallery').length){ 
			$('.JSproduct-gallery').slick({
				vertical: false,
				slidesToShow: 4,
				asNavFor: $('.JSproduct-main-slick'),
			})
		}
	}

	if ($('.JSproduct-gallery').length){ 
		$('.JSproduct-gallery').slick({ 
			autoplay: false,
			slidesToShow: 5,
			vertical: true,
			draggable: true,
			asNavFor: $('.JSproduct-main-slick'),
			slidesToScroll: 1
		}); 
	}


	$('.JSproduct-gallery .img-gallery').on('click', function() {
		clickedGalleryIndex = $(this).attr('JSgallery-index');
		$('.JSproduct-main-slick').slickGoTo(clickedGalleryIndex);
	});

	// Articles and Banners
	if ($('.JSarticlesBanners')[0]) { 
		$('.JSarticlesBanners').slick({ 
			autoplay: true, 
			arrows: false,
			slidesToShow: 3,
			slidesToScroll: 1, 
			responsive: [
				{
					breakpoint: 1240,
					settings: {
					  slidesToShow: 3,
					  slidesToScroll: 1
					}
				  },
				{
				  breakpoint: 1160,
				  settings: {
					slidesToShow: 3,
					slidesToScroll: 1
				  }
				},
				{
				  breakpoint: 880,
				  settings: {
					slidesToShow: 2,
					slidesToScroll: 1
				  }
				},
				{
				  breakpoint: 480,
				  settings: {
					slidesToShow: 2,
					slidesToScroll: 1
				  }
				}
			]
		}); 
	}

	//  Subgroup Slick
	if ($('.JSsubs_slick').length){
		$('.JSsubs_slick').slick({ 
			autoplay: true, 
			arrows: false,
			slidesToShow: 5,
			slidesToScroll: 1, 
			responsive: [
				{
				  breakpoint: 1160,
				  settings: {
					slidesToShow: 4,
					slidesToScroll: 1
				  }
				},
				{
				  breakpoint: 880,
				  settings: {
					slidesToShow: 2,
					slidesToScroll: 1
				  }
				},
				{
				  breakpoint: 480,
				  settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				  }
				}
			]
		}); 
	 }

	 $('.JSslider_banner .slider_banner_left').each(function(i) {
		if(i === 0 || i === 1) {
			$(this).remove();
		}
	})

	//  Slider With Banners
	 if ($('.JSslider_banner')[0]) { 
		$('.JSslider_banner').slick({ 
			autoplay: false, 
			arrows: true,
			initialSlide: 2,
			slidesToShow: 1,
			slidesToScroll: 1, 
			responsive: [
				{
				  breakpoint: 1160,
				  settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				  }
				},
				{
				  breakpoint: 880,
				  settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				  }
				},
				{
				  breakpoint: 480,
				  settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				  }
				}
			]
		}); 
	 }
	
	if ($('.JSblog-slick').length){
		$('.JSblog-slick').slick({
			autoplay: true,
		    infinite: true,
		    speed: 600, 
			slidesToShow: 3,
			slidesToScroll: 1, 
			responsive: [
				{
				  breakpoint: 1100,
				  settings: {
					slidesToShow: 3,
					slidesToScroll: 1,
					infinite: true,
					dots: false
				  }
				},
				{
				  breakpoint: 800,
				  settings: {
					slidesToShow: 2,
					slidesToScroll: 2
				  }
				},
				{
				  breakpoint: 480,
				  settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				  }
				}
			]
		});
	}

// } 	// START PAGE END ==============
 
	// CATEGORIES - MOBILE
	if ($(window).width() < 991 ) {
		$('.JScategories .fa-bars').click(function () { 
	    	$('.JSlevel-1').slideToggle();
		});
	}


	$('#sidebar-left .JSlevel-1 li').each(function() {
		if($(this).find('ul').length > 0) {
			$(this).append('<span class="JScat_arrow"><span class="fas fa-chevron-down"></span></span>')
		}
	})

	
	if($(window).width() < 991) {
		$('.menu-background .JSlevel-1 > li, .main-menu li').each(function(i, li){
			if($(this).find('ul').children().length > 0){
				$(this).append('<span class="JSsubcat-toggle"><i class="fas fa-chevron-down"></i></span>');
			}
		});
	}

	if($(window).width() > 991) {
		$('.menu-background  .JSlevel-1 > li > a').each(function() {
			if($(this).siblings('ul').length > 0) {
				$(this).append('<span class="JScat_arrow"><span class="fas fa-chevron-right"></span></span>');
			}
		})
	}

	$('.cat-title-mobile').on('click', function() {
		$('#responsive-nav .JSlevel-2').removeClass('left-side-cat-active')
		$('#responsive-nav .drop-2').removeClass('left-side-cat-active')
	})

	$('.JSsubcat-toggle').on('click', function() {
	 	$(this).toggleClass('rotate');
	    // $(this).siblings('ul').slideToggle();
		// $(this).parent().siblings().children('ul').slideUp();

		var data_page_naziv = $(this).parent().find('a').data('naziv')
		$('.cat-title-mobile').find('span').text(data_page_naziv)

		var data_firstLevel = $(this).siblings().closest('li').data('firstlevel');
		$('#cat-title-mobile-page.cat-title-mobile').find('span').text(data_firstLevel);
		
		$(this).siblings('ul').toggleClass('left-side-cat-active');
		$(this).find('.drop-2').toggleClass('left-side-cat-active');
		$(this).parent().siblings().children('ul').removeClass('left-side-cat-active');
		$(this).parent().siblings().find('.JSsubcat-toggle').removeClass('rotate');
	}); 

	// Check Term
	$('.JScall-alert').on('click', function() {
		$('.JSinfo-popup').fadeIn().delay(500).fadeOut();
		$('.JSinfo-popup .JSpopup-inner').html("<p class='p-info'>"+trans('Prihvatite uslove isporuke')+"</p>")
		$('.JScheck-term-cart').addClass('JSterm-blink');
	});

	$('#checkout-btn').addClass("disabled-btn");

	$('#checkout-btn').on('click', function(e) {
		// Check if the checkbox is not checked
		if (!$('.JScheck-term-cart input').prop('checked')) {
			e.preventDefault();
			$('#checkout-btn').attr('disabled', false);
			$('.JSinfo-popup').fadeIn().delay(500).fadeOut();
			$('.JSinfo-popup .JSpopup-inner').html("<p class='p-info'>"+trans('Prihvatite uslove isporuke')+"</p>")
		}
	});
	
	$('.JScheck-term-cart').on('change', function() {
		if(!$('.JScheck-term-cart').find('input').prop('checked')) {
			sessionStorage.removeItem('checkedTerm')
			$('#checkout-btn').addClass('disabled-btn');
			$('.JScall-alert').show();
		} else {
			sessionStorage.setItem('checkedTerm', '1');
			$('#checkout-btn').removeClass('disabled-btn');
			$('.JScall-alert').hide();
			$('.JScheck-term-cart').removeClass('JSterm-blink');
		}
	});

	if(sessionStorage.getItem('checkedTerm')) {
		$('.JScheck-term-cart').find('input').prop('checked', 'checked');
		$('.JScall-alert').hide();
		$('#checkout-btn').removeClass('disabled-btn');
	}

	if(!('.JScheck-term-cart').length) {
		sessionStorage.removeItem('checkedTerm');
	}

	// ---------------------------

	$('.JScall-alert').on('click', function() {
		$('.JSinfo-popup').fadeIn().delay(500).fadeOut();
		$('.JSinfo-popup .JSpopup-inner').html("<p class='p-info'>"+trans('Prihvatite uslove isporuke')+"</p>")
		$('.JScheck-term').addClass('JSterm-blink');
	});

	$('#to-checkout').addClass("disabled-btn");

	$('#to-checkout').on('click', function(e) {
		// Check if the checkbox is not checked
		if (!$('.JScheck-term input').prop('checked')) {
			e.preventDefault();
			$('#to-checkout').attr('disabled', false);
			$('.JSinfo-popup').fadeIn().delay(500).fadeOut();
			$('.JSinfo-popup .JSpopup-inner').html("<p class='p-info'>"+trans('Prihvatite uslove isporuke')+"</p>")
		}
	});
	
	$('.JScheck-term').on('change', function() {
		if(!$('.JScheck-term').find('input').prop('checked')) {
			sessionStorage.removeItem('checkedTerm')
			$('#to-checkout').addClass('disabled-btn');
			$('.JScall-alert').show();
		} else {
			sessionStorage.setItem('checkedTerm', '1');
			$('#to-checkout').removeClass('disabled-btn');
			$('.JScall-alert').hide();
			$('.JScheck-term').removeClass('JSterm-blink');
		}
	});

	if(sessionStorage.getItem('checkedTerm')) {
		$('.JScheck-term').find('input').prop('checked', 'checked');
		$('.JScall-alert').hide();
		$('#to-checkout').removeClass('disabled-btn');
	}

	if(!('.JScheck-term').length) {
		sessionStorage.removeItem('checkedTerm');
	}

	if($(window).width() < 768) {
		$('.without-registration').append($('.JScheck-term').parent()).addClass('text-center');
	}

	$('.btn-filter').on('click', function() {
		$('.filter-drop').slideToggle();
		$(this).find('i').toggleClass('fa-times')
	});
	
	$('.ft-section-title').each(function() {
		var footerColsDropButton = $(this).find('i');
		var footerDrop = $(this).next('.footer-drop');

		footerColsDropButton.on('click', function() {
			$('.footer-drop').not(footerDrop).slideUp();
			footerDrop.slideToggle();

			$('.ft-section-title i').not(footerColsDropButton).removeClass('fa-minus').addClass('fa-plus');
			footerColsDropButton.toggleClass('fa-plus fa-minus');
		})
	});


	$('#recently-button-clear').on('click', function() {
		$.post(base_url + 'clear-recently-viewed', {}, function (response){
			$('.recently-products-content').html('')
			$('.no-recently-viewed-products').show().removeClass('hidden')
			$('#recently-button-clear').hide();
		});
	})

	$('.num_seen').text(Math.floor(Math.random() * 20) + 1);
	$('.people_seen').text(Math.floor(Math.random() * 10));


	if($(window).width() > 991) {
		$('.menu-background .JSlevel-1 > li').on('mouseover', function() {
			let offsetTop = $(this).offset().top - $(window).scrollTop(),
			offsetLeft = $(this).offset().left + $(this).width(),
			menuHeight = $('.JSlevel-1').height(),
			menuOffset = $('.menu-background').offset().top + 85;
			level2 = $(this).find('.JSlevel-2');

			if(!$('body').is('#start-page')) {
				menuOffset = $('.menu-background').offset().top + 55;
				level2.css('top',menuOffset);
			}

			level2.css('top',menuOffset);
			level2.css('left', offsetLeft);
			level2.show();
			$(this).siblings().find('.JSlevel-2').hide();

			if (offsetTop > 300) {
				level2.css('min-height', menuHeight + 100 + 'px');
			}
			else {
				level2.css('min-height', menuHeight + 100 + 'px');
			}
		}); 

		$('.d-content').on('mouseout', function(e) {
			if ( ! $('.menu-background .JSlevel-1, .JSlevel-2').is(e.target)) {
				$(".JSlevel-2").hide();
			}
		})
	}


 
 // SOCIAL ICONS
	$('.social-icons .facebook').append('<i class="fab fa-facebook-f"></i>');
	$('.social-icons .twitter').append('<i class="fab fa-twitter"></i>');
	$('.social-icons .google-plus').append('<i class="fab fa-google-plus-g"></i>');
	$('.social-icons .skype').append('<i class="fab fa-skype"></i>');
	$('.social-icons .instagram').append('<i class="fab fa-instagram"></i>');
 	$('.social-icons .linkedin').append('<i class="fab fa-linkedin"></i>');
	$('.social-icons .youtube').append('<i class="fab fa-youtube"></i>'); 

// PRODUCT PREVIEW IMAGE
    if ($(".JSproduct-preview-image")[0]){
		jQuery(".JSzoom_03").elevateZoom({
			gallery:'gallery_01', 
			cursor: 'pointer',  
			imageCrossfade: true,  
			zoomType: 'lens',
			lensShape: 'round', 
			lensSize: 200, 
			borderSize: 1, 
			containLensZoom: true
		}); 
		jQuery(".JSzoom_03").bind("click", function(e) { var ez = $('.JSzoom_03').data('elevateZoom');	$.fancybox(ez.getGalleryList()); return false; });
    } 

    $('.JS_product_video').on('click', function(){
    	var vid = $(this).find('video');
    	$('.JSmain_vid').show().attr({ src: vid.attr('src'), alt: vid.attr('alt') });
    	$('.JSmain_img').hide();
    });

    $('.JS_product_image').on('click', function(){
    	$('.JSmain_vid').hide();
    	$('.JSmain_img').show();
    });

	$(".header-cart").on('click', function () {
		$(".JSheader-cart-content").addClass('active');
		$(".mini-cart-overlay").addClass('active');
		$('body').addClass('overflow-hdn');
	})
	$(".mini-cart-header img, .no-items button").on('click', function () {
		$(".JSheader-cart-content").removeClass('active');
		$(".mini-cart-overlay").removeClass('active');
		$('body').removeClass('overflow-hdn');
	})
	$('.mini-cart-overlay').on('click', function() {
		$('.JSheader-cart-content').removeClass('active');
		$(this).removeClass('active');
		$('body').removeClass('overflow-hdn');
	})

// ======== SEARCH ============
 	var searchTimeVest;
	$('#JSsearchVest').on("keyup", function(event) {
		if(event.keyCode == 13 && $('#JSsearchVest').val() != ''){
        	$(".JSsearch-button2").trigger("click");
    	}
    	if($('#elasticsearch').val() == 1){
			timer2();
    	}else{	
			clearTimeout(searchTimeVest);
			searchTimeVest = setTimeout(timer2, 300);
    	}
	});
	
	var searchTime;
	$('#JSsearch2').on("keyup", function(event) {
		if(event.keyCode == 13 && $('#JSsearch2').val() != ''){
        	$(".JSsearch-button2").trigger("click");
    	}
    	if($('#elasticsearch').val() == 1){
			timer2();
    	}else{	
			clearTimeout(searchTime);
			searchTime = setTimeout(timer2, 300);
    	}
	});
 
	$('.JSSearchGroup2').change(function(){	timer2(); });

	function timer2(){
		var search_length = 2;
		if($('#elasticsearch').val() == 1){
			search_length = 0;
		}
		var search = $('#JSsearch2').val();
		if (search != '' && search.length > search_length) {
			$.post(base_url + 'livesearch', {search:search, grupa_pr_id: $('.JSSearchGroup2').val()}, function (response){
				if(response != ''){
					$('.JSsearch_list').remove();
					$('.JSsearchContent2').append(response);
					$('.search-darken').show();

					if(!$('.JSsearch_list').children().length) {
		    			$('.JSsearch_result').remove();
		    		}
				}
			});
		} 
		else {
			// REMOVE FOR EMPTY STRING
			$('.JSsearch_result').remove();
			$('.search-darken').hide();
			$('body').removeClass('overflow-hdn');
		}
	}

	$(document).on('click', function(e){
		var target = $(e.target);
		if(!target.is($('.JSsearch_result, .JSsearch_result *, #JSsearch2, #JSsearch2 *'))) { 
			$('.JSsearch_result').remove();
		} 
    }); 

	$('html :not(.JSsearch_list)').on("click", function() {
		$('.JSsearch_list, .search-darken').remove();
	});
	
		 
// RATING STARS
	$('.JSrev-star i').each(function(i, el){
		var val = 0;
		$(el).on('click', function(){    
			$(el).addClass('fas').removeClass('far'); 
		    $(el).prevAll('i').addClass('fas').removeClass('far'); 
			$(el).nextAll('i').removeClass('fas').addClass('far'); 
		 	if (i == 0) { val = 1; }
		 	else if(i == 1){ val = 2; }
		 	else if(i == 2){ val = 3; }
		 	else if(i == 3){ val = 4; }
		 	else if(i == 4){ val = 5; } 
		 	$('#JSreview-number').val(val); 
		}); 
	}); 
 
  
	$('.JStoggle-btn').on('click', function(){
		$(this).closest('div').find('.JStoggle-content').toggleClass('hidden-small'); 
	});
	 
 // RESPONSIVE NAVIGATON
 	$(".resp-nav-btn").on("click", function(){  
		$("#responsive-nav").toggleClass("openMe");	
		$('.responsive-overlay').show();
	});
	$(".JSclose-nav").on("click", function(){  
		$("#responsive-nav").removeClass("openMe");		 
	});

	// Sidebar Categories
	$('#sidebar-left .JSlevel-1 li').click(function () {
    	$('#responsive-nav #JScategories .JSlevel-1, #JScat_hor .JSlevel-1').slideToggle();
	});

	$('.JScat_arrow').click(function () {
	    $(this).siblings('ul').slideToggle();
		$(this).parent().siblings().children('ul').slideUp();
	});


	if($(window).width() < 991) {
		$('.JScategories').on('click', function() {
			$(this).find('.JSlevel-1').slideDown();
		})
	}
 
// SCROLL TO TOP 
 	$(window).scroll(function () {
		var winScroll = $(window).scrollTop();
        var height = $(document).height() - $(window).height();
        var scrolled = (winScroll / height) * 100;
		var scrollIndicator = $(".scroll-indicator");

        if ($(this).scrollTop() > 150) {
            $('.JSscroll-top').css('right', '20px');
			scrollIndicator.css('height', scrolled + '%')
        } else {
            $('.JSscroll-top').css('right', '-70px');
        }
    });
 
    $('.JSscroll-top').click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 600);
        return false;
    }); 
    
// filters slide down
	$(".JSfilters-slide-toggle").click(function(){
	    $(this).next(".JSfilters-slide-toggle-content").slideToggle('fast');
		$(this).find('i').toggleClass('fa-plus')
	});

	if ($('.selected-filters li').length) {
		$('.JShidden-if-no-filters').show();
	} else {
		$('.JShidden-if-no-filters').parent().remove();
	}
 
 // SELECT ARROW - FIREFOX FIX
	// $('select').wrap('<span class="select-wrapper"></span>');
	
 
 /*======= MAIN CONTENT HEIGHT ========*/
	if ($(window).width() > 1024 ) {
		$('.JScategories').on('mouseover', function(){
			if ($('.d-content').height() < $('.JSlevel-1').height()) { 
				$('.d-content').height($('.JSlevel-1').height());
			}
		}).on('mouseleave', function(){
			$('.d-content').height('');
		});
	} 

	// Sticky Categories
	$('#JSsticky_menu .JScategories').on('click', function() {
		if($('body').is('#start-page')) {
			if ($(window).scrollTop() > 0) {
				$('.JSlevel-1').toggleClass('active-sticky-category');
				$('.stickyOverlay').toggleClass('active');
			}
		} else {
			$('.JSlevel-1').toggleClass('active-sticky-category');
			$('.stickyOverlay').toggleClass('active');
		}
	});

	if(!$('body').is('#start-page')) {
		$('.JSlevel-2').css('border-radius', 0)
	}
 

// POPUP BANNER
	if($('body').is('#start-page') && !(sessionStorage.getItem('popup')) ){   
	 	setTimeout(function(){ 
		 	$('.JSfirst-popup').animate({ top: '50%' }, 700);
		}, 1500);   

		 $(document).on('click', function(e){
		 	var target = $(e.target);
		 	if(!target.is($('.first-popup-inner img'))) { 
		 		$('.JSfirst-popup').hide();
		 	} 
		 }); 
		 if ($('.JSfirst-popup').length) {
	    	sessionStorage.setItem('popup','1');
		 }
	}


	if ($(window).width() > 991 ) {
 
// CATEGORIES WRAP
		if($('body').is('#start-page')) {
			$('.JSlevel-1 li').each(function () {	
				var subitems = $(this).find(".JSlevel-2 > li");
				for(var i = 0; i < subitems.length; i+=2) {
					subitems.slice(i, i+2).wrapAll("<div class='clearfix col-md-7'></div>");
				}
			});
		}

	// FOOTER COLS CLEARFIX
	  	(function(){
	  		var cols = $('.JSfooter-cols > div');
	  		for(var i = 0; i < cols.length; i+=4){
	  			cols.slice(i, i+4).wrapAll('<div class="clearfix JSfooter-secs"></div>');
	  		}
  		}()); 
  
	}

 // LEFT & RIGHT BODY LINK
	if ($(window).width() > 1024 ) {  
		var right_link = $('.JSright-body-link'),
			left_link = $('.JSleft-body-link'),
			main = $('.JSmain'); 

		setTimeout(function(){ 
			main.before((left_link).css('top', main.position().top + 'px'));
			main.after((right_link).css('top', main.position().top + 'px'));
			left_link.width(($(window).outerWidth() - $('.container').outerWidth())/2).height(main.height()); 
			right_link.width(($(window).outerWidth() - $('.container').outerWidth())/2).height(main.height()); 
		}, 1000); 
  	} 

	  // Enquiry
 	$('.JSenquiry').click(function(){
		var obj = $(this);
		var roba_id = obj.data('roba_id');
		var roba_id = obj.data('roba-id');
	
		$('#JSStateArtID').val(roba_id);
		$('#checkState').modal('show');
	});

	if($('#checkState').find('.red-dot-error').text()) {
		$('#checkState').modal();
	}

	$('.checkout-slide').on('click', function() {
		$('.checkout-content').slideToggle('fast');
		$(this).find('i').toggleClass('fa-chevron-up')
	})
 

// SEARCH BY GROUP TEXT
	function selectTxt(){    
		var txt = $('.JSSearchGroup2 option:first').text(trans('Sve')); 
		// if ($(window).width() > 1024 ){ 
		// 	txt.text('Pretraga po kategorijama');  
		// }else{
		// 	txt.text('po kategorijama');  
		// }
	}
	selectTxt();


	// GENERIC CAR TITLE
	$('.generic_car li').each(function(i, li){
		if ($(li).text().trim().length > 20) {
			$(li).attr('title', $(li).text().trim());
		} 
	});

	//order poll modal
	$('.JSOrderPoll').click(function() {
		var order_id = $(this).data('id');
		
		$.ajax({
			type: "POST",
			url: base_url + 'poll-content',
			data: { order_id: order_id },
			success: function(response) {
				$('#JSOrderPollContent').html(response);
				$('#JSOrderPollModal').modal('show');
			}
		});
	});

	// Popup Search
	var header = $('header'),
		offset_top = $('header').offset().top,
		offset_bottom = offset_top + header.outerHeight(true);

	$('#JSsearch2').on('click', function() {
		$('.stickyOverlay').toggleClass('active');
		$('.popup-search').css('top', offset_bottom);
		$('.popup-search').fadeToggle('500');
		$('body').toggleClass('overflow-hdn');
	});
	$('.stickyOverlay').on('click', function() {
		$(this).removeClass('active');
		$('.popup-search').fadeOut('fast');
	})

	$('#JSsearch2').on('input', function() {
		$('.stickyOverlay').removeClass('active');
		$('.popup-search').fadeOut('fast');
	})
 	
 	// COOKIES
 	$(function(){
		function getCookie(cookieParamName){
			if(document.cookie.split('; ').find(row => row.startsWith(cookieParamName))) {
				return document.cookie
				  .split('; ')
				  .find(row => row.startsWith(cookieParamName))
				  .split('=')[1];
			}else{
				return undefined;
			}
		}
		function setCookie(cookieParamName,cookieParamValue,expireDays){
			var now = new Date();
			var time = now.getTime();
			var expireTime = time + 1000*86400*expireDays;
			now.setTime(expireTime);
			document.cookie = cookieParamName+"="+cookieParamValue+";expires="+now.toUTCString()+"; path=/;";
		}

		$("#alertCookiePolicy").hide()
		if (getCookie('acceptedCookiesPolicy') === undefined){
			//console.log('accepted policy', chk);
			$("#alertCookiePolicy").show(); 
		}
		$('#btnAcceptCookiePolicy').on('click',function(){
			//console.log('btn: accept');
			setCookie('acceptedCookiesPolicy','yes',30);
		});
		$('#btnDeclineCookiePolicy').on('click',function(){
			//console.log('btn: decline');
			setCookie('acceptedCookiesPolicy','no',30);
		});
 	});


	//  if($(window).width() > 991) {
	// 	$('.menu-background .JScategories').on('click', function(e) {
	// 		$(this).find('.JSlevel-1').stop().delay(2000).stop().fadeIn(300);
	// 	 })
	
	// 	 $('.menu-background .JScategories').on('mouseleave', function(e) {
	// 		$(this).find('.JSlevel-1').stop().delay(1500).fadeOut(300);
	// 	})	

	// 	$('.menu-background .JSlevel-1 li').on('click', function(e) {
	// 		$(this).find('.JSlevel-2').stop().delay(2000).stop().fadeToggle(300);
	// 		e.preventDefault()
	// 	})

	// 	$('.menu-background .JSlevel-1 > li > a').each(function(){
	// 		if($(this).siblings('ul').length){ 
	// 		$(this).click(function(e) {
	//        		e.preventDefault(); 

	// 			$(this).parent().addClass('show-level-2').siblings().removeClass('show-level-2');
    //      		$(this).parent().addClass('show-level-2').closest('.menu-background .JSlevel-1 li').siblings().children().removeClass('show-level-2');
	// 	    });
	// 			$(this).dblclick(function(e) {
	// 				location.href = $(this).attr('href');
	// 			});
	// 		}
	// 	});

		

	// 	$('.menu-background .JSlevel-1 li').on('mouseleave', function(e) {
	// 		$(this).find('.JSlevel-2').stop().delay(2000).stop().fadeOut(900);
	// 	})

	// 	$(document).on("click", function(event){
	// 		var $trigger = $(".menu-background .JScategories");
	// 		if($trigger !== event.target && !$trigger.has(event.target).length){
	// 			// $('.menu-background .JSlevel-1').hide();
	// 				$(this).find('.menu-background .JSlevel-1').stop().delay(1500).fadeOut(300);

	// 		}            
	// 	});
	
	//  }

	if($(window).width() < 991) {
		// Append to Mobile Menu
		var menuPagesMobile = $('.menu-pages-mobile'),
			loggedIn = $('.login_b2c'),
			notLoggedIn = $('.not_logged_in'),
			loggedIn = $('.login_b2c'),
			hasCustomer = $('.has_customer'),
			wishList = $('#login-icon'),
			wishLoginMenu = $('.wish-login-pages li'),
			noCustomer = $('.no_customer'),
			menuMobileMenu = $("#menu-mobile");
			
		menuPagesMobile.append(menuMobileMenu);
		wishLoginMenu.append(loggedIn)
		wishLoginMenu.append(notLoggedIn)
		wishLoginMenu.append(wishList)
		wishLoginMenu.append(hasCustomer)
		wishLoginMenu.append(loggedIn)
		wishList.removeClass('hidden-sm')
		noCustomer.removeClass('hidden-sm')

		 // MENU FOR CATEG
		$('.close-cat-nav').on('click', function() {
			$("#responsive-nav").removeClass("openMe");	
			$('.responsive-overlay').hide();
		})

		$('.responsive-overlay').on('click', function() {
			$("#responsive-nav").removeClass("openMe");	
			$(this).hide();
			$('#responsive-nav .JSlevel-2').removeClass('left-side-cat-active')
			$('#responsive-nav .drop-2').removeClass('left-side-cat-active')
		})
	}
	
	$('.share-btn').on('click', function() {
		$('.share-modal').show();
		$('header').css('z-index', '0');
		$('.ask-question-overlay').addClass('active');
		$('body').addClass('overflow-hdn');
		$('header').addClass('z-auto');
	})
	$('.share-modal-header button').on('click', function() {
		$('.share-modal').hide();
		$('header').css('z-index', '55');
		$('.ask-question-overlay').removeClass('active');
		$('body').removeClass('overflow-hdn');
		$('header').removeClass('z-auto');
	})

	$('.ask-question-btn').on('click', function() {
		$('.ask-question-modal').show();
		$('header').css('z-index', '0');
		$('.ask-question-overlay').addClass('active');
		$('body').addClass('overflow-hdn');
		$('header').addClass('z-auto');
	})
	$('.ask-question-modal-header button').on('click', function() {
		$('.ask-question-modal').hide();
		$('header').css('z-index', '55');
		$('.ask-question-overlay').removeClass('active');
		$('body').removeClass('overflow-hdn');
		$('header').removeClass('z-auto');
	})

	// Copy text
	function copyToClipboard() {
		var copyInput = $('.share-modal-main input');

		copyInput.select();
		copyInput[0].setSelectionRange(0, 99999);

		document.execCommand("copy");

		copyInput[0].setSelectionRange(0, 0);

		showCheckIcon();
	}

	function showCheckIcon() {
		var copyIcon = $('#copyIcon'),
			checkIcon = $('#checkIcon');

		copyIcon.addClass('hidden');
		checkIcon.removeClass('hidden');

		setTimeout(function() {
			copyIcon.removeClass('hidden');
			checkIcon.addClass('hidden');
		}, 2000)
	}
	$('.copy-btn').on('click', function() {
		copyToClipboard();
	})
	
 	// COOKIES TEXT
 	(function(){
	 	var cookiesBtn = $('.JScookiesInfo_btn');
		cookiesBtn.text('Prikaži detalje');
		cookiesBtn.on('click', function(){
			cookiesBtn.next().slideToggle('fast');
			cookiesBtn.text(function(i, text){
		   		return text == 'Prikaži detalje' ? 'Sakrij detalje' : 'Prikaži detalje'
			}); 
		});
	}());

 	// SHOW COOKIES
	setTimeout(function(){ 
	 	$('.JScookies-part').animate({ bottom: '0' }, 700);
	}, 1500);   

}); // DOCUMENT READY END
 

// ZOOMER 
document.onreadystatechange = function(){
    if (document.readyState === "complete") { 

		if($('body').is('#artical-page')){	
			$('.zoomContainer').css({
				width: $('.zoomWrapper').width(),
				height: $('.zoomWrapper').height()
			}); 
				
			// var art_img =  $('#art-img').attr('src');
			// if (art_img.toLowerCase().indexOf('no-image') >= 0){
			// 	$('.zoomContainer').hide();
			// } 
		// ARTICLE ZOOMER		
			$('.zoomContainer').each(function(i, el){
				if ( i != 0) {
					$(el).remove();
				}
			});
		}  

		$('.product-image').each(function(i, img){
			if(!$(img).is("video")) {
				if ($(img).attr('src') != 'quick_view_loader.gif') {
					$(img).attr('src', '/images/no-image.jpg');
				}
			}
		});  

		// Quick View
		$(document).on('click', '.JSQuickViewButton', function() {
			$('#JSQuickView').modal('show');
			$('#JSQuickViewContent').html('<img class="gif-loader" src="'+base_url+'images/quick_view_loader.gif" alt="gif loader">');	
			$.post(base_url + 'quick-view', { roba_id: $(this).data('roba_id') }, function(response) {
				$('#JSQuickViewContent').html(response);
			});	
		});
		$(document).on('click', '#JSQuickViewCloseButton', function() { 
			$('#JSQuickView').modal('hide');
		});

// OVERLAPING CATEGORIES
		// (function(){
		//   	var categ = $('.JSlevel-1'),
		//   		categ_offset_bottom = categ.offset().top + categ.outerHeight(true); 
 
	 //  		$('.JSproducts_slick').each(function(i, el){
	 //  			if (categ_offset_bottom >= $(el).offset().top) {
	 //  				$(el).closest('.JSaction_offset').css('marginLeft', categ.width() + 'px');
	 //  			}  
	 //  		});

		// }());
 
// SECTION OFFSET
		if($('body').is('#start-page') && $(window).width() > 991){		
			$('.three-banners-wrapper').parent().parent().removeClass('JSd_content');

			if($('.JSd_content').length) {
				$('.JSd_content').each(function(i, el){  
		 			var lvl_1 = $('.JSlevel-1'),
		 				main_slider = $('.JSmain-slider'),
		 				parent_cont = $('.parent-container');

		 			if ($(el).offset().top <= lvl_1.offset().top + lvl_1.outerHeight()) { 

		 				if((lvl_1).children().length){
							$(el).css('margin-left', lvl_1.outerWidth()); 
						}	 

						if ($(this).closest(parent_cont).hasClass('container-fluid')) {

							if (!$(el).find(main_slider).length) {
					 			$(this).closest(parent_cont).removeClass('container-fluid no-padding').addClass('container');

					 		} else {
					 			$(el).css('margin-left', ''); 	 
					 		}

				 		}
					}  

	 
					$(el).css('width', 'auto');  
				 
				}); 
					$('.JSloader').hide();
			} 
			else {
				$('.JSloader').hide();
			}
		}
 
	} 
}
 
 
