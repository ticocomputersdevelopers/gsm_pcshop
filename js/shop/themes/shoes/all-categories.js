$(document).ready(function () { 


    var header = $('header'),
        admin_menu = $('#admin-menu').length ? $('#admin-menu').outerHeight() : 0;
  

    $(window).on('scroll', function () { 
 
        if (header.hasClass('sticky_head')) {
            $('.sticky-element').css('top', header.outerHeight() + admin_menu );
        } else {
            $('.sticky-element').css('top', admin_menu);
        }

    });

  
    $('a[href^="#"]').on('click', function(event) { 
        var target = $( $(this).attr('href') ); 
        if( target.length ) {
            event.preventDefault();
            $('html, body').animate({
                scrollTop: target.offset().top  
            }, 500);
        } 
    }); 

    // category sidebar toggler 
    $('.JScategory-sidebar__list__toggler').on('click', function(){
        $(this).parent().toggleClass('category-sidebar__list--open');
    }); 

});



 