@extends('b2b.layouts.b2bpage')
@section('content')
    <section class="clearfix">
        @if($page==B2bCommon::get_kontakt())
            <h2><span class="heading-background">Kontaktirajte nas</span></h2>

            <div id="map_canvas" class="map"></div>

            <section class="contact-info medium-4 columns">

                <h3>Kontakt informacije</h3>
 
            <ul>
                <li class="medium-4 small-5 columns">Firma:</li><li class="medium-8  small-7 columns"> {{ B2bOptions::company_name() }}</li>
                <li class="medium-4 small-5 columns">Adresa:</li><li class="medium-8 small-7 columns"> {{ B2bOptions::company_adress() }}</li>
                <li class="medium-4 small-5 columns">Grad:</li><li class="medium-8 small-7 columns"> {{ B2bOptions::company_city() }}</li>
                <li class="medium-4 small-5 columns">Telefon:</li><li class="medium-8 small-7 columns"> {{ B2bOptions::company_phone() }}</li>
                <li class="medium-4 small-5 columns">Fax:</li><li class="medium-8 small-7 columns"> {{ B2bOptions::company_fax() }}</li>
                <li class="medium-4 small-5 columns">PIB:</li><li class="medium-8 small-7 columns"> {{ B2bOptions::company_pib() }}</li>
                <li class="medium-4 small-5 columns">E-mail:</li><li class="medium-8 small-7 columns">
                <a class="mailto" href="mailto:{{ Options::company_email() }}">{{ B2bOptions::company_email() }}</a></li>
            </ul>

            </section>

            <section class="contact-form medium-8 columns">

                <h3>Pošaljite poruku</h3>

                <label id="label_name">Vaše ime *</label>
                <input class="contact-name" id="kontakt-name" type="text" onchange="check_fileds('kontakt-name')" >

                <label id="label_email">Vaša e-mail adresa *</label>
                <input class="contact-email" id="kontakt-email" onchange="check_fileds('kontakt-email')" type="text" >

                <label id="label_message">Vaša poruka </label>
                <textarea class="contact-message" id="message" ></textarea>

                <button class="submit" onclick="meil_send()" >Pošalji</button>

            </section>
        @else
        <section class="clearfix about-us-page">
            {{ $web_content}}
        </section>
        @endif
    </section>
@endsection
@section('footer')

    <input type="hidden" id="base_url" value="{{B2bOptions::base_url()}}" />
    <script src="{{ B2bOptions::base_url()}}js/jquery-1.11.2.min.js" type="text/javascript" ></script>
    <script src="{{ B2bOptions::base_url()}}js/b2b/main_function.js" type="text/javascript" ></script>
    <script src="{{ B2bOptions::base_url()}}js/b2b/b2b_main.js" type="text/javascript" ></script>
    <script src="{{ B2bOptions::base_url()}}js/jquery.fancybox.pack.js" type="text/javascript" ></script>
    @if($page == B2bCommon::get_kontakt())
		<script src="https://maps.googleapis.com/maps/api/js"></script>
		<script>

			function initialize() {

				var map_canvas = document.getElementById('map_canvas');
				var map_options = {
					center: {lat: 43.315991, lng:  21.855720},
					zoom: 17,
					mapTypeId: google.maps.MapTypeId.ROADMAP
				}

				var map = new google.maps.Map(map_canvas, map_options);

				var marker = new google.maps.Marker({
					position: {lat: 43.315991, lng:  21.855720},
					map: map,
					title: 'Nasa lokacija'
				});
				
			} // end initialize()

			google.maps.event.addDomListener(window, 'load', initialize);

</script>
    @endif
@endsection