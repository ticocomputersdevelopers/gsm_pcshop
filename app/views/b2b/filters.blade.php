@section('filters')
    <aside id="sidebar-left" class="medium-3 columns product-list-page">
        <!-- CATEGORIES -->
            @if(B2bOptions::category_view()==1)
            @include('b2b.categories')
            @endif
<!--         <ul class="breadcrumbs clearfix">
             {{ B2bUrl::b2bBreadcrumbs()}}
        </ul> -->

    <div class="filters test">
        <!-- CHECKED FILTERS -->
        <div class="row">

	    <span class="selected-filters-wrapper medium-12 columns">

		    <span class="selected-filters-title">Izabrani filteri:</span>

		</span>

        </div>

        <div class="btn-container">

            <a class="reset-filters-button" href="{{ Request::url()}}">Poništi filtere</a>

        </div>
        <!-- END CHECKED -->

        <div class="filter-field">

                <form class="filter-form" method="get" action="">
                <div class="row">
                    <ul>

                        <!-- FILTER ITEM -->

                        <li class="columns filter-box">
                            <a href="javascript:void(0)" class="label-field">Proizvođač 
                                <i class="fa fa-angle-down"></i>
                            </a>

                            <div class="select-fields clearfix">


                                <div class="multiselect multiselect-proizvodjaci">

                                    @foreach(B2b::getManufacturers($grupa_pr_id,$filters) as $row)
                                        @if(isset($filters['proizvodjac']))
                                            @if(in_array($row->proizvodjac_id,$filters['proizvodjac']))
                                                <label><input type="checkbox" data-name="proizvodjac" data-type="remove" data-old-value="{{implode('-',$filters['proizvodjac'])}}"  class="filter-item" value="{{ $row->proizvodjac_id }}" checked> {{$row->naziv}} ( {{$row->products}} )</label>

                                            @else
                                                @if( $filters['proizvodjac'][0]!=null )
                                                    <label><input type="checkbox"  data-name="proizvodjac" data-type="add" data-old-value="{{implode('-',$filters['proizvodjac'])}}" class="filter-item" value="{{ implode('-',$filters['proizvodjac']+[''=>$row->proizvodjac_id]) }}"> {{$row->naziv}} ( {{$row->products}} )</label>
                                                @else
                                                    <label><input type="checkbox"  data-name="proizvodjac" data-type="add" data-old-value="{{implode('-',$filters['proizvodjac'])}}"  class="filter-item" value="{{ $row->proizvodjac_id }}"> {{$row->naziv}} ( {{$row->products}} )</label>
                                                @endif

                                            @endif
                                        @else
                                            <label><input type="checkbox"  data-name="proizvodjac" data-type="add" data-old-value=""  class="filter-item" value="{{ $row->proizvodjac_id }}"> {{$row->naziv}} ( {{$row->products}} )</label>
                                        @endif
                                     @endforeach
                                </div>

                            </div>


                        </li>
                        @foreach($filtersItems as $filter)

                        <li class="columns filter-box">
                            <a href="javascript:void(0)" class="label-field">{{$filter['grupa_naziv']}}
                                <i class="fa fa-angle-down"></i>
                            </a>

                            <div class="select-fields clearfix">


                                <div class="multiselect multiselect-vrednosti">
                                    @foreach($filter['grupa_pr_vrednost'] as $row)
                                        @if(isset($filters[$filter['grupa_naziv_var']]))
                                            @if(in_array($row['vrednost_id'],$filters[$filter['grupa_naziv_var']]))
                                                <label><input type="checkbox" data-name="{{$filter['grupa_naziv_var']}}" data-type="remove" data-old-value="{{implode('-',$filters[$filter['grupa_naziv_var']])}}"  class="filter-item" value="{{ $row['vrednost_id'] }}" checked> {{$row['vrednost_naziv']}} ( {{$row['broj_proizvoda']}} )</label>

                                            @else
                                                @if( $filters[$filter['grupa_naziv_var']][0]!=null )
                                                    <label><input type="checkbox"  data-name="{{$filter['grupa_naziv_var']}}" data-type="add" data-old-value="{{implode('-',$filters[$filter['grupa_naziv_var']])}}"  class="filter-item" value="{{ implode('-',$filters[$filter['grupa_naziv_var']]+[''=>$row['vrednost_id']]) }}"> {{$row['vrednost_naziv']}} ( {{$row['broj_proizvoda']}} )</label>
                                                @else
                                                    <label><input type="checkbox"  data-name="{{$filter['grupa_naziv_var']}}" data-type="add" data-old-value="{{implode('-',$filters[$filter['grupa_naziv_var']])}}"  class="filter-item" value="{{ $row['vrednost_id'] }}"> {{$row['vrednost_naziv']}} ( {{$row['broj_proizvoda']}} )</label>
                                                @endif

                                            @endif
                                        @else
                                            <label><input type="checkbox"  data-name="{{$filter['grupa_naziv_var']}}" data-type="add" data-old-value=""  class="filter-item" value="{{ $row['vrednost_id'] }}"> {{$row['vrednost_naziv']}} ( {{$row['broj_proizvoda']}} )</label>
                                        @endif
                                    @endforeach

                                </div>

                            </div>


                        </li>
                        @endforeach
                    </ul>

                </div>

                </form>
        </div>
    </div>
    </aside>
@endsection