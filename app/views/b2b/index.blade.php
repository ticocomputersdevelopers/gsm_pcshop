@extends('b2b.layouts.b2bpage')
@section('head')
    <link href="{{ B2bOptions::base_url()}}css/slick.css" rel="stylesheet" type="text/css" />
    
@endsection
@section('content')

        <!-- MAIN SLIDER -->
    <section id="main-slider">
        <?php foreach(DB::table('baneri')->where('tip_prikaza',2)->orderBy('redni_broj','asc')->get() as $row){ ?>
        <div><a href="<?php echo $row->link; ?>"><img src="{{B2bOptions::base_url()}}<?php echo $row->img; ?>" /></a></div>

        <?php } ?>
    </section>
        <!-- AKCIJA -->

        <h2><span class="heading-background">Na Akciji</span></h2>
        <section class="sale-products product-slider">

            @foreach(B2bCommon::akcija() as $row)

                <div class="product">

                    <div class="product-content clearfix">

                        <a href="{{B2bOptions::base_url()}}b2b/artikal/{{B2bUrl::url_convert(B2bArticle::seo_title($row->roba_id))}}" class="product-image-wrapper">
                            <img class="product-image" src="{{B2bOptions::base_url()}}{{ B2bArticle::web_slika($row->roba_id) }}" alt="{{ B2bArticle::seo_title($row->roba_id) }}" />
                        </a>
                        <a class="product-title" href="{{B2bOptions::base_url()}}b2b/artikal/{{B2bUrl::url_convert(B2bArticle::seo_title($row->roba_id))}}">{{ B2bArticle::short_title($row->roba_id) }}</a>
                        <section class="action-holder clearfix">
                            <span class="product-price">{{ B2bBasket::cena(B2bArticle::b2bRabatCene($row->roba_id)->ukupna_cena) }}</span>
                            @if(B2bArticle::getStatusArticle($row->roba_id) == 1)
                                <?php
                                    $cartAvailable = B2bArticle::quantityB2b($row->roba_id) - B2bBasket::getB2bQuantityItem($row->roba_id);
                                ?>
                                @if($cartAvailable>0 )
                               <div class="btn-container"> 
                                    <button data-product-id="{{$row->roba_id}}"  data-max-quantity="{{$cartAvailable}}" class="add-to-cart addCart">Dodaj u korpu
                                     
                                   </button>
                               </div>
                                @else
                                 <div class="btn-container"> 
                                    <button class="dodavnje not-available">Nije dostupno</button>
                                </div>
                                @endif
                            @else
                            <button class="dodavanje not-available">{{ B2bArticle::find_flag_cene(B2bArticle::getStatusArticle($row->roba_id),'naziv') }}</button>
                           
                            @endif
                        </section>
                    </div>

                </div>

            @endforeach

        </section>
        <?php $strana=B2bCommon::get_page_start();?>

            <!-- IZDVAJAMO -->


@endsection
@section('footer')

    <section class="popup info-confirm-popup info-popup">

        <div class="popup-wrapper">

            <section class="popup-inner">



            </section>

        </div>

    </section>
    <script src="{{ B2bOptions::base_url()}}js/jquery-1.11.2.min.js" type="text/javascript" ></script>
    <script src="{{ B2bOptions::base_url()}}js/slick.min.js" type="text/javascript" ></script>
    <script src="{{ B2bOptions::base_url()}}js/b2b/b2b_main.js" type="text/javascript" ></script>
    <script src="{{ B2bOptions::base_url()}}js/jquery.fancybox.pack.js" type="text/javascript" ></script>

    <script>


    </script>
@endsection