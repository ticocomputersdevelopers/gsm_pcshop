@extends('b2b.layouts.b2bpage')
@section('content')
    <div class="cart-page">
        <section class="cart">

            <h2><span class="heading-background">Vaša Korpa</span></h2>

                @if(Session::has('b2b_cart') and B2bBasket::b2bCountItems() > 0)

                <ul class="cart-labels clearfix">
                   
                    <li class="medium-4 columns">Proizvod</li>

                    <div class="medium-8 zero-padding columns end">
                        <li class="medium-2 columns">Cena</li>
                        <li class="medium-1 columns">Rabat</li>
                        <li class="medium-2 zero-padding columns">Cena sa rab.</li>
                        <li class="medium-2 columns">PDV %</li>
                        <li class="medium-2 columns">Količina </li> 
                        <li class="medium-2 columns end">Ukupno</li>
                        <li class="medium-1 columns">Ukloni</li>
                    </div>
               
                </ul>
                    @foreach($items as $row)
                    <ul class="cart-item clearfix">
                        
                        <div class="medium-4 columns small-12 zero-padding">
                            <li class="cart-image medium-4 small-3 columns">
                                <span> 
                                    <img src="{{B2bOptions::base_url()}}{{ B2bArticle::web_slika($row->roba_id) }}">
                                </span>
                            </li>
                            <li class="cart-name medium-8 small-9 columns">
                                <span> 
                                    <a href="{{B2bOptions::base_url()}}b2b/artikal/{{B2bUrl::url_convert(B2bArticle::seo_title($row->roba_id))}}">
                                        {{B2bArticle::short_title($row->roba_id)}}
                                    </a>
                                </span>
                            </li>
                         </div>

                        <div class="medium-8 columns zero-padding end">

                            <?php
                                $rabatCene = B2bArticle::b2bRabatCene($row->roba_id);
                                $totalItem = $rabatCene->ukupna_cena*$row->kolicina;
                            ?>
                            <li class="cart-price medium-2 small-12 columns"><span>{{B2bBasket::cena($rabatCene->osnovna_cena)}}</span></li>
                            <li class="cart-price medium-1 small-12 columns"><span>{{number_format($rabatCene->ukupan_rabat,2)}}%</span></li>
                            <li class="cart-price medium-2 zero-padding small-12 columns"><span>{{B2bBasket::cena($rabatCene->cena_sa_rabatom)}}</span></li>
                            <li class="cart-price medium-2 small-12 columns"><span>{{number_format($rabatCene->porez,2)}}%</span></li>
                            <li class="medium-2 small-12 columns">
                                <span> 
                                    <a class="cart-less" data-roba-id="{{ $row->roba_id }}"  href="#"><</a>
                                    <input class="cart-amount" id="quantity-{{ $row->roba_id }}" data-roba-id="{{ $row->roba_id }}" data-old-kol="{{ (int)$row->kolicina }}" id="{{ $row->web_b2b_korpa_stavka_id }}" data-max-kol="{{ B2bArticle::quantityB2b($row->roba_id) }}" type="text" value="{{(int)$row->kolicina}}">
                                    <a class="cart-more" data-roba-id="{{ $row->roba_id }}" data-max-kol="{{ B2bArticle::quantityB2b($row->roba_id) }}" href="#">></a> 
                                </span>
                            </li>
                           <li class="medium-2 columns small-12 end"> <span> {{B2bBasket::cena($totalItem)}}</span></li>
                            <li class="cart-remove medium-1 small-12 columns">
                             <span> 
                                 <a href="javascript:deleteCartItem({{ $row->web_b2b_korpa_stavka_id }})" title="Ukloni artikal" class="close"><span>x</span></a>
                            </span>
                        </li>
                        </div>                  
                    </ul>
                    @endforeach

                        <section class="below-cart row">
                             <div  class="cart-remove-all-wrapper medium-3 small-6 columns">
                                 <button class="cart-remove-all">Isprazni korpu</button>
                            </div>
                        </section>

                       <section class="below-cart cart-flex row">
                            <div class="cart-summary-wrapper medium-4 small-12 columns">

                                <section class="cart-summary">

                                    <?php
                                    $cartTotal = B2bBasket::total();
                                    ?>
                                    <span class="header-cart-summary">Osnovica: <span>{{ B2bBasket::cena($cartTotal->cena_sa_rabatom) }}</span></span>
                                    <span class="header-cart-summary">PDV: <span>{{ B2bBasket::cena($cartTotal->porez_cena) }}</span></span>
                                    <span class="header-cart-summary">Ukupno: <span>{{ B2bBasket::cena($cartTotal->ukupna_cena) }}</span></span>

                                </section>

                            </div>

                        </section>

                <!-- PAYMENT METHOD -->
                <form class="payment-method row" action="{{ route('b2b.check_out') }}" method="POST">

                    <span class="cart-more-icon"></span>

                    <div class="medium-6 columns">

                        <label>Način isporuke:</label>

                        <select id="nacin_isporuke" name="nacin_isporuke">

                            {{B2bBasket::nacin_isporuke()}}


                        </select>

                    </div>

                    <div class="medium-6 columns">

                        <label>Način plaćanja:</label>

                        <select id="nacin_placanja" name="nacin_placanja">

                            {{B2bBasket::nacin_placanja()}}

                        </select>

                    </div>

                    <div class="medium-12 columns">

                        <label>Napomena:</label>

                        <textarea class="" name="napomena" id="napomena"></textarea>

                    </div>
                    <div class="row text-right"> 
                        <button class="submit-order-button">Prosledi porudžbinu</button>
                    </div>
                </form>
    </div>

                @else
                    <p class="empty-page">Trenutno nemate artikle u korpi.</p>
                @endif


        </section>


@endsection

@section('footer')
    <section class="popup info-confirm-popup info-popup">

        <div class="popup-wrapper">

            <section class="popup-inner">



            </section>

        </div>

    </section>
    <script src="{{ B2bOptions::base_url()}}js/jquery-1.11.2.min.js" type="text/javascript" ></script>
    <script src="{{ B2bOptions::base_url()}}js/b2b/b2b_main.js" type="text/javascript" ></script>
    <script src="{{ B2bOptions::base_url()}}js/jquery.fancybox.pack.js" type="text/javascript" ></script>
    <script>
        $(document).ready(function(){
            $('.cart-less').click(function(){
               var roba_id = $(this).data('roba-id');
               var quantity = $('#quantity-'+roba_id);
                if(quantity.val()==1){
                    $('.info-popup').fadeIn("fast").delay(2000).fadeOut("fast");

                    $('.info-popup .popup-inner').html("<p class='p-info'>Minimalna količina je 1 kom.</p>");
                    quantity.val(quantity.data('old-kol'));
                }
                else {
                    $.ajax({

                        type: "POST",
                        url:'{{route('b2b.cart_add')}}',
                        cache: false,
                        data:{roba_id:roba_id, status:3, quantity:1},
                        success:function(res){

                            location.reload();

                        }

                    });
                }
            });

            $('.cart-more').click(function(){
                var roba_id = $(this).data('roba-id');
                var max = $(this).data('max-kol');
                var quantity = $('#quantity-'+roba_id);
                if(quantity.val()==max){
                    $('.info-popup').fadeIn("fast").delay(2000).fadeOut("fast");

                    $('.info-popup .popup-inner').html("<p class='p-info'>Maksimalna količina je "+max+" kom.</p>");
                    quantity.val(quantity.data('old-kol'));
                }
                else {
                    $.ajax({

                        type: "POST",
                        url:'{{route('b2b.cart_add')}}',
                        cache: false,
                        data:{roba_id:roba_id, status:2, quantity:1},
                        success:function(res){

                            location.reload();

                        }

                    });
                }
            });

            $('.cart-amount').change(function(){
               var roba_id = $(this).data('roba-id');
               var old_kol = $(this).data('old-kol');
               var max = $(this).data('max-kol');
               var quantity = $(this).val();
                if(quantity < 1){
                    $('.info-popup').fadeIn("fast").delay(2000).fadeOut("fast");

                    $('.info-popup .popup-inner').html("<p class='p-info'>Minimalna količina je 1 kom.</p>");
                    $(this).val(old_kol);
                }
                else if(quantity > max){
                    $('.info-popup').fadeIn("fast").delay(2000).fadeOut("fast");

                    $('.info-popup .popup-inner').html("<p class='p-info'>Maksimalna količina je "+max+" kom.</p>");
                    $(this).val(old_kol);
                }
                else {
                    $.ajax({

                        type: "POST",
                        url:'{{route('b2b.cart_add')}}',
                        cache: false,
                        data:{roba_id:roba_id, status:1, quantity:quantity},
                        success:function(res){

                            location.reload();

                        }

                    });
                }

            });

            $('.cart-remove-all').click(function() {
                $.ajax({

                    type: "POST",
                    url:'{{route('b2b.cart_delete_all')}}',
                    cache: false,
                    success:function(){

                        location.reload();


                    }

                });
            });

        });
        function deleteCartItem(cart_id){
            $.ajax({

                type: "POST",
                url:'{{route('b2b.cart_delete')}}',
                cache: false,
                data:{cart_id:cart_id},
                success:function(res){

                    location.reload();


                }

            });
        }
    </script>
@endsection