<!DOCTYPE html>

<html>

<head>

    <title>{{ B2bOptions::company_name()}} Admin</title>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="msapplication-tap-highlight" content="no"/>

    <link rel="icon" type="image/png" href="{{ B2bOptions::base_url()}}favicon.ico">

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,400,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <link href="{{ B2bOptions::base_url()}}css/foundation.min.css" rel="stylesheet" type="text/css" />
    <link href="{{ B2bOptions::base_url()}}css/admin.css" rel="stylesheet" type="text/css" />



</head>

<body class="body-login">
    <div class="color-overlay-image"></div>
    <div class="color-overlay"></div>

<!-- LOGIN FORM -->

<div class="login-form-wrapper">



    <section class="login-form">

        <a class="logo" href="{{ B2bOptions::base_url()}}" title="{{ B2bOptions::company_name()}}"><img src="{{ B2bOptions::base_url()}}{{B2bOptions::company_logo()}}" alt=""></a>
        <span class="logo-text">B2B</span>
        <form action="{{route('b2b.forgot_password_post')}}" method="post" >
            <div class="field-group">
                <label for="email">Email</label>
                <input id="mail" name="mail" type="email" class="login-form__input" required>
            </div>
            <label>{{Session::get('message')}}</label>
            
            <div class="btn-container center">
                <button class="submit admin-login btn btn-primary btn-round btn-large">Potvrdi</button>
            </div>
            <a class="submit btn btn-secondary btn-round btn-small became-partner" href="login">Otkaži</a>
            <a class="submit btn btn-secondary btn-round btn-small became-partner" href="{{route('b2b.registration')}}" >Postanite partner</a>

        </form>


    </section>

</div>




</body>

</html>