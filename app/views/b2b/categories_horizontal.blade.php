<div id="cat_hor">

    <nav id="categories">
    
        <?php $query_category_first=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>0,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc')->get(); ?>
        <h3 class="categories-titile">Kategorije </h3>
        <span class="toggler"><i class="fa fa-bars"></i></span>
        
        <!-- CATEGORIES LEVEL 1 -->
    
        <ul class="level-1">
        @if(B2bOptions::category_type()==1)
        @foreach ($query_category_first as $row1)
        
                @if(B2bCommon::broj_cerki($row1->grupa_pr_id) >0)
                <li>
                    <a href="{{ B2bOptions::base_url()}}b2b/artikli/{{ B2bUrl::url_convert($row1->grupa)  }}">{{ $row1->grupa  }}</a>

                <?php $query_category_second=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row1->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('grupa_pr_id','asc') ?>
                <ul class="level-2 clearfix">
                
                    @foreach ($query_category_second->get() as $row2)
                         <li><a href="{{ B2bOptions::base_url()}}b2b/artikli/{{ B2bUrl::url_convert($row1->grupa) }}/{{ B2bUrl::url_convert($row2->grupa) }}">{{ $row2->grupa }}</a>
                         @if(B2bCommon::broj_cerki($row2->grupa_pr_id) >0)
                            <?php $query_category_third=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row2->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('grupa_pr_id','asc')->get(); ?>
                            
                            
                                <ul class="level-3 clearfix">
                                    @foreach($query_category_third as $row3)
                                     <li><a href="{{ B2bOptions::base_url()}}b2b/artikli/{{ B2bUrl::url_convert($row1->grupa) }}/{{ B2bUrl::url_convert($row2->grupa) }}/{{ B2bUrl::url_convert($row3->grupa) }}">{{ $row3->grupa }}</a>
                                    @endforeach
                                </ul>
                                @endif
                         </li>
                    @endforeach
                </ul>
                @else
                <li><a href="{{ B2bOptions::base_url()}}b2b/artikli/{{ B2bUrl::url_convert($row1->grupa) }}">{{ $row1->grupa  }}</a>
                
                @endif
                
                </li>
        @endforeach
        @else
        @foreach ($query_category_first as $row1)
        
                @if(B2bCommon::broj_cerki($row1->grupa_pr_id) >0)
                <li><a href="{{ B2bOptions::base_url()}}b2b/artikli/{{ B2bUrl::url_convert($row1->grupa)  }}">{{ $row1->grupa  }}</a>
                <?php $query_category_second=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row1->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('grupa_pr_id','asc') ?>
                <ul class="level-2-category clearfix">
                
                    @foreach ($query_category_second->get() as $row2)
                         <li><a href="{{ B2bOptions::base_url()}}b2b/artikli/{{ B2bUrl::url_convert($row1->grupa) }}/{{ B2bUrl::url_convert($row2->grupa) }}">{{ $row2->grupa }}</a>
                         @if(B2bCommon::broj_cerki($row2->grupa_pr_id) >0)
                            <?php $query_category_third=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row2->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('grupa_pr_id','asc')->get(); ?>
                            
                            
                                <ul class="level-3 clearfix">
                                    @foreach($query_category_third as $row3)
                                     <li><a href="{{ B2bOptions::base_url()}}b2b/artikli/{{ B2bUrl::url_convert($row1->grupa) }}/{{ B2bUrl::url_convert($row2->grupa) }}/{{ B2bUrl::url_convert($row3->grupa) }}">{{ $row3->grupa }}</a>
                                    @endforeach
                                </ul>
                                @endif
                         </li>
                    @endforeach
                </ul>
                @else
                <li><a href="{{ B2bOptions::base_url()}}b2b/artikli/{{ B2bUrl::url_convert($row1->grupa) }}">{{ $row1->grupa  }}</a>
                
                @endif
                
                </li>
        @endforeach             
        @endif

        @if(B2bOptions::all_category()==1)
        <li class="category-item-accent">
            <a class="all-category-link" href="/categoty_listing">Sve Kategorije</a>
        </li>
        @endif
        
        </ul> 
    
    </nav>
</div>