<section id="preheader" class="row">
    <!-- MAIN MENU -->
    <nav class="medium-8 small-12 columns">
        <ul id="main-menu" class="clearfix">
            @foreach(B2bCommon::menu_top_items() as $row)
            <li><a href="{{ B2bOptions::base_url().'b2b/'.$row->naziv_stranice }}">{{ $row->title }}</a></li>
            @endforeach
            <li><a href="{{B2bOptions::base_url()}}b2b/user/card">Kartica</a></li> 
        </ul>
    </nav>
    <!-- B2B PROGRAM -->
    <nav class="medium-4 columns text-right small-only-text-center"> 
        <a id="b2b-login-icon" href="/b2b/logout" class="btn confirm global-bradius"><span>Odjavi se</span></a>
      <!-- LOGIN & REGISTRATION ICONS -->
        @if(Session::has('b2b_user_'.B2bOptions::server()))
        <a id="user_edit" href="{{route('b2b.user_edit')}}"><span>{{ B2bPartner::getPartnerName() }}</span></a>
        @endif
    </nav>
    <span class="menu-close"><i class="fa fa-times" aria-hidden="true"></i></span>
</section>

 