@extends('adminb2b.defaultlayout')
@section('content')

<section class="" id="main-content">
	
	<div class="row art-row">
		<div class="row">
			<section class="medium-5 columns">
				<div class="flat-box">
					@include('adminb2b.pages.nivo_pristupa_proizvodjaci')
				</div>
			</section>
		</div>
	</div>
</section>
@endsection