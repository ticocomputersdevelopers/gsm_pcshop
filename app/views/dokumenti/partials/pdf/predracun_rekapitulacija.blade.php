        <div class="row"> 
        	<br>
			<h4>{{ AdminLanguage::transAdmin('Rekapitulacija') }}</h4>
		</div>

		<div class="row"> 
			<div class="col-7"> 
				<table class="rekapitulacija">
					<thead>
						<tr>
							<td></td>
							<td>{{ AdminLanguage::transAdmin('Osnovica') }}</td>
							<td>{{ AdminLanguage::transAdmin('PDV') }}</td>
						</tr>
					</thead>

					<tbody>
						<tr>
							<td>{{ AdminLanguage::transAdmin('Oslobodjeno poreza') }}</td>
							<td>0,00</td>
							<td>0,00</td>
						</tr>
						<tr>
							<td>{{ AdminLanguage::transAdmin('Po posebnoj stopi') }}</td>
							<td>0,00</td>
							<td>0,00</td>
						</tr>
						<tr>
							<td>{{ AdminLanguage::transAdmin('Po opstoj stopi') }}</td>
							<td>{{AdminCommon::cena($predracunCene->ukupno)}}</td>
							<td>{{AdminCommon::cena($predracunCene->pdv)}}</td>
						</tr>
						<tr>
							<td><strong>{{ AdminLanguage::transAdmin('Ukupno') }}</strong></td>
							<td>{{AdminCommon::cena($predracunCene->ukupno)}}</td>
							<td>{{AdminCommon::cena($predracunCene->pdv)}}</td>
						</tr>
					</tbody>
				</table>
		 	</div>

		 	<div class="col-4"> 
		        <table class="summary">
		        	<tr class="text-right">
		                <td class="summary text-right"><span class="sum-span">{{ AdminLanguage::transAdmin('Iznos osnovice') }}:</span>
		                 {{AdminCommon::cena($predracunCene->ukupno)}}</td>
		            </tr>
		            <tr class="text-right">
		                <td class="summary text-right"><span class="sum-span">{{ AdminLanguage::transAdmin('Iznos PDV-a') }}:</span>
		                 {{AdminCommon::cena($predracunCene->pdv)}}</td>
		            </tr>
		            <tr class="text-right">
		                <td class="summary text-right"><span class="sum-span">{{ AdminLanguage::transAdmin('Ukupno') }}:</span> 
		                {{AdminCommon::cena($predracunCene->ukupno_pdv)}}</td>
		            </tr>
		        </table>
	        </div>
		</div>
