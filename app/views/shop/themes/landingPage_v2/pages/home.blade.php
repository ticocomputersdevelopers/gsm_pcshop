@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('baners_sliders')
@include('shop/themes/'.Support::theme_path().'partials/baners_sliders')
@endsection 

@section('page')

<div class="container bw">     

	<!-- BANERS --> 
	@if($firstSlider = Slider::getFirstSlider('Baner') AND count($slajderStavke = Slider::slajderStavke($firstSlider->slajder_id)) > 0)
	<div class="row banners"> 
		
		@foreach($slajderStavke as $slajderStavka)
		<div class="col-md-4 col-sm-4 col-xs-12"> 
			<div class="text-center banner"> 

				<a class="slider-link" href="{{ $slajderStavka->link }}"></a>

				<div class="bg-img relative" style="background-image: url('{{ Options::domain() }}{{ $slajderStavka->image_path }}'); ">
					
					@if($slajderStavka->naslov_dugme != '') 
					<span class="slider-btn-link inline-block JSInlineShort" data-target='{"action":"slide_button","id":"{{$slajderStavka->slajder_stavka_id}}"}'>{{ $slajderStavka->naslov_dugme }}
						<i class="fas fa-chevron-right arrow pull-right"></i>
					</span> 
					@endif 
				</div> 
				 
				<div class="bannersText">
					  
					@if($slajderStavka->naslov != '')
					<h2 class="JSInlineShort" data-target='{"action":"slide_title","id":"{{$slajderStavka->slajder_stavka_id}}"}'>
						{{ $slajderStavka->naslov }}
					</h2> 
				 	@endif

					@if($slajderStavka->sadrzaj != '') 
					<div class="JSInlineFull" data-target='{"action":"slide_content","id":"{{$slajderStavka->slajder_stavka_id}}"}'>
						{{ $slajderStavka->sadrzaj }}
					</div> 
					@endif  
				</div> 
				 
			</div> 
		</div>
		
		@endforeach
	</div> 
	@endif 
</div>
 
 
<div class="container bw"> 
	<div class="row testimonial text-center JStestimonial"> 

		<div class="col-md-4 col-sm-4 col-xs-12">
			<img src="/images/presentationImages/tes_1.jpg" class="img-responsive margin-auto" alt="image">
			<h3>27 Ways To Improve selling</h3>
			<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
		</div>

		<div class="col-md-4 col-sm-4 col-xs-12">
			<img src="/images/presentationImages/tes_2.jpg" class="img-responsive margin-auto" alt="image">
			<h3>Learn Exactly How We Made Our Business</h3>
			<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
		</div>

		<div class="col-md-4 col-sm-4 col-xs-12">
			<img src="/images/presentationImages/tes_3.jpg" class="img-responsive margin-auto" alt="image">
			<h3>10 Warning Signs Of You Failure</h3>
			<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
		</div>

		<div class="col-md-4 col-sm-4 col-xs-12">
			<img src="/images/presentationImages/tes_4.jpg" class="img-responsive margin-auto" alt="image">
			<h3>Everything You Wanted to Know About Selling Strategy</h3>
			<p>The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters</p>
		</div>
	</div>
</div> 


<div class="container bw"> 
	<div class="main-desc text-center">
		{{ Support::front_admin_content(5) }}
	</div>
 
	@include('shop/themes/'.Support::theme_path().'partials/section-news')
</div>
  

<!-- NEWSLATTER -->
@if(Options::newsletter()==1)
<div class="newsletter bw"> 
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-sm-6 xol-xs-6">
				<h5 class="">{{ Language::trans('Prijavite se za naš newsletter') }}</h5> 
				<p>{{ Language::trans('Za najnovije informacije o našem sajtu prijavite se na našu e-mail listu') }}.</p>
			</div>

			<div class="col-md-6 col-sm-6 xol-xs-6">
				<div class="relative">              
					<input type="text" placeholder="{{Language::trans('Unesite Vašu e-mail adresu')}}" id="newsletter" /> 
					<button onclick="newsletter()" class="text-uppercase text-white">{{ Language::trans('Prijavi se') }}</button>
				</div>
			</div>
		</div> 
	</div>
</div>
@endif

@endsection