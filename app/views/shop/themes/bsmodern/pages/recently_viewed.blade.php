@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')

<div class="recently-viewed-navigation text-center">
    <h1 class="mb-1">Nedavno pregledani proizvodi</h1>
    <div class="flex items-center gap-1 justify-center">
        <a href="{{ Options::base_url() }}">Početna</a> /
        <span>Nedavno pregledani proizvodi</span>
    </div>
</div>

@if(Session::has('viewed'))
    <div class="recently-viewed-products text-center">
        <button class="button radius-50 transition-sm" id="recently-button-clear">Obriši sve</button>

        <?php $recently_viewed = Session::get('viewed');  ?>
        <div class="recently-products-content text-left">
            @foreach($recently_viewed as $roba_id)
            <?php $row = DB::table('roba')->where('roba_id',$roba_id)->first(); ?>
            <div class="recently-viewed-content">
                @include('shop/themes/'.Support::theme_path().'partials/products/product_on_grid')
            </div>
            @endforeach
        </div>
    </div>
@endif

<p class="no-recently-viewed-products text-center lead {{ Session::has('viewed') ? 'hidden' : '' }}">Nemate nedavno pregledane proizvode.
    <a class="underline" href="/">Nazad u kupovinu</a>
</p>

@endsection 
