@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page') 

<!-- NEW.blade -->
<div class="row">
	<div class="col-md-9">
		<div class="col-xs-12">
		 <div class="single-news">

			<h1 class="news-title">{{ $naslov }}</h1>


			@if(in_array(Support::fileExtension($slika),array('jpg','png','jpeg','gif')))

				<img class="max-width" src="{{ $slika }}" alt="{{ $naslov }}">

			@else

				<iframe src="{{ $slika }}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>                   

			@endif
			

			<h2 class="news-title">{{ All::getNewsCategory($id) }}</h2>
			<div><i class="far fa-clock"></i> {{ date('d-m-y', strtotime($datum)) }}</div>

			{{ $sadrzaj }} 

			</div>  

		 </div>
	</div>
	<div class="col-md-3 col-sm-3 col-xs-12 no-padding"> 

	   <h3 class="more-text flex">Još tekstova...</h3>

		<div class="main-category">
			<div class="category-content">
				<h2>Oblasti</h2>
				 <div class="category-link">
					@foreach(All::getNewsCategories() as $row)
						<button class="button inline-block">
							<a href="{{ Options::base_url() }}blog/{{ Url_mod::slugify($row->naziv) }}">
								{{ $row->naziv }}
							</a>
						</button>
					@endforeach
				 </div>
			</div>	
		</div>

        <div class="newsBlogs">
        	<h2>{{ Language::trans('Blog') }}</h2>
			<div class="news-content">
	       		 {{All::getNewsifiedPages()}}
			</div>
        </div>

        <div class="LatestNews">
        	<h2 class="main-title">{{ Language::trans('Najnovije vesti') }}</h2>
	        @foreach(All::getNews() as $row)
				<div class="news relative JSRightArticles">
					<div class="row side-blog">
						<div class="col-xs-6 no-padding">			
							<div class="side-blog-image-wrap">
								<a class="overlink" href="{{ Options::base_url() }}blog/{{ Url_mod::blog_kategorija_link($row->web_vest_b2c_id) }}/{{ Url_mod::blog_link($row->web_vest_b2c_id) }}"></a>
									@if(in_array(Support::fileExtension($row->slika),array('jpg','png','jpeg','gif')))						
										<div class="bg-img" href="{{ Options::base_url() }}{{ Url_mod::slug_trans('blog') }}/{{ Url_mod::blog_link($row->web_vest_b2c_id) }}" style="background-image: url('{{ $row->slika }}');"></div>
									@else
										<iframe width="560" height="315" src="{{ $row->slika }}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
									@endif
							</div>
						</div>

						<div class="col-xs-6 no-padding">
							<h2 class="news-title overflow-hdn JSArticleHeading">{{ Url_mod::blog_naslov($row->web_vest_b2c_id) }} <br> {{ Url_mod::blog_kategorija($row->web_vest_b2c_id) }}</h2>
							<div class="calendar">
								<span>{{ Support::date_convert($row->datum) }}</span>
							</div>
							
						</div>	
					</div>
				</div>
			@endforeach
		</div>
	</div>
</div>


<!-- NEW.blade END -->

@endsection