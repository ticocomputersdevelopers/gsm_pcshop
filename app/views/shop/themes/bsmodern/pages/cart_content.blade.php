@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')

<!-- CART_CONTENT.blade -->

@if(Session::has('b2c_korpa') and Cart::broj_cart() >= 1 )

<br class="hidden-sm hidden-xs">

<div class="cart-navigation text-center">
	<h2 class="text-center cart-heading"><span class="section-title">{{ Language::trans('Vaša Korpa') }}</span></h2>
	<span><a href="/">Početna</a></span> /
	<span>Vaša korpa</span>
</div>
<br>

@if(Session::has('failure-message'))
<h4>{{ Session::get('failure-message') }}</h4>
@endif

<div class="cart-content-wrapper">
	<div class="row">
		<div class="col-md-9 col-sm-9 col-xs-12 mb-2">
			<!-- Table -->
			<table class="table table-bordered cart-table">
				<thead class="hidden-sm hidden-xs">
					<tr>
						<th colspan="1" scope="col">{{ Language::trans('Proizvod') }}</th>
						<th colspan="1"></th>
						<th colspan="1" scope="col">{{ Language::trans('Količina') }}</th>
						<th colspan="1" scope="col">{{ Language::trans('Ukupno') }}</th>
						<th colspan="2" scope="col"></th>
					</tr>
				</thead>
				<tbody>
					@foreach(DB::table('web_b2c_korpa_stavka')->where('web_b2c_korpa_id',Cart::korpa_id())->orderBy('web_b2c_korpa_stavka_id','asc')->get() as $row) 
						<tr>
							<td class="cart-table-article">
								<div class="row">
									<div class="col-xs-12">
										@if(!empty(Product::design_id($row->roba_id)) AND Options::pitchprint_aktiv() == 1)  
											@if(!empty($row->project_id))
												<img src="https://s3-eu-west-1.amazonaws.com/pitchprint.io/previews/{{$row->project_id}}_1.jpg" alt="{{ Product::short_title($row->roba_id) }}" class="cart-image img-responsive" />
												@else
													<img src="https://s3-eu-west-1.amazonaws.com/pitchprint.io/previews/{{Product::design_id($row->roba_id)}}_1.jpg" alt="{{ Product::short_title($row->roba_id) }}" class="cart-image img-responsive" />
												@endif
											@else
											<img src="{{ Options::domain() }}{{Product::web_slika($row->roba_id) }}" alt="{{ Product::short_title($row->roba_id) }}" class="cart-image img-responsive" />
										@endif
									</div>
								</div>
							</td>

							<td>
								<div class="col-xs-12 sm-no-padd">
									<div> 
										<a href="{{ Options::base_url() }}{{Url_mod::slug_trans('artikal')}}/{{ Url_mod::slugify(Product::seo_title($row->roba_id)) }}" class="text-bold">
											{{ Product::short_title($row->roba_id) }}
										</a>
										{{Product::getOsobineStr($row->roba_id,$row->osobina_vrednost_ids)}}   
									</div>
									<div class="table-currency">{{ Cart::cena($row->jm_cena) }}</div>
								</div>
							</td>

							<td>
								<div class="cart-table-gray flex items-center justify-between radius-5">
									@if(Options::web_options(320)==0)
										<a data-stavka_id="{{ $row->web_b2c_korpa_stavka_id }}" class="JScart-less" href="javascript:void(0)" rel="nofollow">
											<i class="fas fa-minus"></i>
										</a>

										
										<input type="text" class="JScart-amount" value="{{ round($row->kolicina) }}" onkeypress="validate(event)" data-stavka_id="{{ $row->web_b2c_korpa_stavka_id }}">
										
										<a data-stavka_id="{{ $row->web_b2c_korpa_stavka_id }}" class="JScart-more" href="javascript:void(0)" rel="nofollow">
											<i class="fas fa-plus"></i>
										</a>
									@else
										@if(Product::jedinica_mere($row->roba_id)->jedinica_mere_id == 3 AND Product::pakovanje($row->roba_id))
										<a data-stavka_id="{{ $row->web_b2c_korpa_stavka_id }}" class="JScart-less-gram" href="javascript:void(0)" rel="nofollow"><</a>
										
										<input type="text" class="JScart-amount" value="{{ $row->kolicina }}" onkeypress="validate(event)" data-stavka_id="{{ $row->web_b2c_korpa_stavka_id }}">
										
										<a data-stavka_id="{{ $row->web_b2c_korpa_stavka_id }}" class="JScart-more-gram" href="javascript:void(0)" rel="nofollow">></a>
										@else
										<a data-stavka_id="{{ $row->web_b2c_korpa_stavka_id }}" class="JScart-less" href="javascript:void(0)" rel="nofollow"><</a>
										
										<input type="text" class="JScart-amount" value="{{ round($row->kolicina) }}" onkeypress="validate(event)" data-stavka_id="{{ $row->web_b2c_korpa_stavka_id }}">
										
										<a data-stavka_id="{{ $row->web_b2c_korpa_stavka_id }}" class="JScart-more" href="javascript:void(0)" rel="nofollow">></a>
										@endif

									@endif
								</div>
							</td>

							<td colspan="1">
								<span class="JScart-item-price" data-stavka_id="{{ $row->web_b2c_korpa_stavka_id }}">{{ Cart::cena($row->jm_cena*$row->kolicina) }}</span>
							</td>

							<td class="text-center">
								<a href="javascript:void(0)" data-stavka_id="{{ $row->web_b2c_korpa_stavka_id }}" class="remove-item JSdelete_cart_item" rel="nofollow"><span class="fas fa-trash"></span></a>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
			<!-- End of table -->

			<br>

			<button class="button" id="JSDeleteCart">{{ Language::trans('Isprazni korpu') }}</button>

			<br>

		</div>

		<!-- Right Side -->
		<div class="col-md-3 col-sm-3 col-xs-12 cart-right-side">
			<div class="cart-content-right">
				<div>
					<div class="cart-summ no-padding text-right">
						<?php $troskovi = Cart::troskovi(); ?>
						<div class="JSdelivery_total_amountParent" {{(Cart::troskovi()>0) ? '' : 'hidden'}}>
							<span class="sum-label">{{ Language::trans('Cena artikala') }}: </span>
							<span class="JSdelivery_total_amount sum-amount">{{Cart::cena(Cart::cart_ukupno())}}</span>
						</div>

						<div class="JSDelivery" {{(Cart::troskovi()>0) ? '' : 'hidden'}}>
							<span class="sum-label">{{ Language::trans('Troškovi isporuke') }}: </span>
							<span class="JSexpenses">{{ Cart::cena($troskovi) }}</span>
						</div>

						<div class="flex justify-between items-center">
							<span class="sum-label text-bold fs-20">{{ Language::trans('Ukupno') }}: </span>
							<span class="JStotal_amount JStotal_amount_weight text-bold fs-20">{{Cart::cena(Cart::cart_ukupno()+$troskovi)}}</span>
						</div>
					</div> 
				</div>

				<div class="row">
					<span class="col-xs-12 JScheck-term-cart">
						<input type="checkbox" name=""> {{ Language::trans('Slažem se sa') }} <a href="/uslovi-isporuke" target="_blank">  {{ Language::trans('uslovima isporuke') }} </a>
					</span>
				</div>

				<div class="to-checkout">
					<a class="d-block text-center w-full button fs-base radius-50" id="checkout-btn" href="{{ Options::base_url() }}checkout" rel="nofollow">{{Language::trans('Plaćanje')}}</a>
				</div>
			</div>
		</div>


	</div>

</div>


@else

<br>

<div class="cart-navigation text-center">
	<h2 class="text-center cart-heading"><span class="section-title">{{ Language::trans('Vaša korpa je prazna') }}</span></h2>
	<span><a href="/">Početna</a></span> /
	<span>Vaša korpa</span>
</div>

<div class="no-articles text-center">
	<a href="/" class="back-to-shop button btn radius-50 p-1">Vratite se u prodavnicu</a>
</div>

@endif 

<br>

<!-- CART_CONTENT.blade END -->

@endsection