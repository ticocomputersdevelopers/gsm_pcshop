@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page') 

<div class="row my-2">

	<div class="col-md-3 col-sm-3 col-xs-12 no-padding">
		<div class="JSsearchContent2">  
            <div class="relative"> 
                <form autocomplete="off">
                    <input type="text" id="JSsearchVest" placeholder="{{ Language::trans('Pretraga') }}" />
                </form>      
                <button onclick="search_vest()" class="JSsearch-button2"> <i class="fas fa-search"></i> </button>
            </div>
        </div>  

		<div class="main-category">
			<div class="category-content">
				<h2>Oblasti</h2>
				 <div class="category-link">
					@foreach(All::getNewsCategories() as $row)
						<button class="button inline-block">
							<a href="{{ Options::base_url() }}blog/{{ Url_mod::slugify($row->naziv) }}">
								{{ $row->naziv }}
							</a>
						</button>
					@endforeach
				 </div>
			</div>	
		</div>

        <div class="newsBlogs">
        	<h2>{{ Language::trans('Blog') }}</h2>
			<div class="news-content">
	       		 {{All::getNewsifiedPages()}}
			</div>
        </div>

        <div class="LatestNews">
        	<h2 class="main-title">{{ Language::trans('Najnovije vesti') }}</h2>
	        @foreach(All::getNews() as $row)
				<div class="news relative JSRightArticles">
					<div class="row side-blog">
						<!-- <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">			 -->
						<div class="col-xs-6 no-padding">			
							<div class="side-blog-image-wrap">
								<a class="overlink" href="{{ Options::base_url() }}blog/{{ Url_mod::blog_kategorija_link($row->web_vest_b2c_id) }}/{{ Url_mod::blog_link($row->web_vest_b2c_id) }}"></a>
									@if(in_array(Support::fileExtension($row->slika),array('jpg','png','jpeg','gif')))						
										<div class="bg-img" href="{{ Options::base_url() }}{{ Url_mod::slug_trans('blog') }}/{{ Url_mod::blog_link($row->web_vest_b2c_id) }}" style="background-image: url('{{ $row->slika }}');"></div>
									@else
										<iframe width="560" height="315" src="{{ $row->slika }}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
									@endif
							</div>
						</div>

						<!-- <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding sm-padding"> -->
						<div class="col-xs-6 no-padding">
							<h2 class="news-title overflow-hdn JSArticleHeading">{{ Url_mod::blog_naslov($row->web_vest_b2c_id) }} <br> {{ Url_mod::blog_kategorija($row->web_vest_b2c_id) }}</h2>
							<div class="calendar">
								<span>{{ Support::date_convert($row->datum) }}</span>
							</div>
							
							<!-- <div class="news-desc overflow-hdn JSArticleText">{{ All::shortNewDesc($row->tekst) }}</div> -->

							<!-- <div>
								<a class="news_btn" href="{{ Options::base_url() }}blog/{{ Url_mod::blog_kategorija_link($row->web_vest_b2c_id) }}/{{ Url_mod::blog_link($row->web_vest_b2c_id) }}" class="button inline-block relative z-index-1">{{ Language::trans('Pročitaj članak') }} <i class="fas fa-caret-right"></i></a>
							</div>  -->
						</div>	
					</div>
				</div>
			@endforeach
		</div>
	</div>

	<div class="col-md-9 col-sm-9 col-xs-12">
		<div class="row">
			@foreach(All::getNews() as $row) 

			<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 no-padding" id="news-no-border">

				<div class="news relative">
				 	
				 	<a class="overlink" href="{{ Options::base_url() }}blog/{{ Url_mod::blog_kategorija_link($row->web_vest_b2c_id) }}/{{ Url_mod::blog_link($row->web_vest_b2c_id) }}"></a>


		            @if(in_array(Support::fileExtension(Url_mod::blog_slike($row->web_vest_b2c_id)),array('jpg','png','jpeg','gif')))
		              
		            	<div class="bg-img" href="{{ Options::base_url() }}{{ Url_mod::slug_trans('blog') }}/{{ Url_mod::blog_link($row->web_vest_b2c_id) }}" style="background-image: url('{{ $row->slika }}');"></div>
		            
		            @else
		            
		            	<iframe width="560" height="315" src="{{ $row->slika }}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>                   
		            
		            @endif

				   <div class="news-text">

				   
					  <h2 class="news-title overflow-hdn">{{ Url_mod::blog_naslov($row->web_vest_b2c_id) }} <br> {{ Url_mod::blog_kategorija($row->web_vest_b2c_id) }}</h2>
					  <!-- <div><i class="far fa-clock"></i> {{Url_mod::blog_datum($row->web_vest_b2c_id)}}</div> -->

					  <div class="news-desc overflow-hdn">{{ All::shortNewDesc(Url_mod::blog_sadrzaj($row->web_vest_b2c_id)) }}</div>
				    </div>

					<!-- <div class="text-center">
						<a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('blog') }}/{{ Url_mod::blog_link($row->web_vest_b2c_id) }}" class="button inline-block relative z-index-1">{{ Language::trans('Pročitaj članak') }}</a>
					</div>  -->

				</div>
			</div>

			<!-- 	<div>
					{{ All::getNews()->links() }}
				</div>   -->

			@endforeach
		</div>
	</div>

</div> 

@endsection