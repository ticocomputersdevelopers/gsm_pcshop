@extends('shop/themes/'.Support::theme_path().'templates/article')

@section('article_details')

<!-- ARTICLE_DETAILS.blade -->

<div id="fb-root"></div> 
<script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.12';
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

@if(Session::has('success_add_to_cart'))
    $(document).ready(function(){     
     
        bootboxDialog({ message: "<p>" + trans('Artikal je dodat u korpu') + ".</p>" }, 2200); 

    });
@endif

@if(Session::get('message'))

    $(document).ready(function(){     
 
        bootboxDialog({ message: "<p>{{ Session::get('message') }}</p>" }); 

    });

@endif

@if(Session::has('success_comment_message'))
    $(document).ready(function(){     
     
        bootboxDialog({ message: "<p>" + trans('Vaš komentar je poslat') + ".</p>" }, 2200); 

    });
@endif
</script> 

<main class="d-content JSmain relative"> 
    <div class="stickyOverlay"></div>
    <div class="mini-cart-overlay"></div>

    <div class="container">
     
        <ul class="breadcrumb bg-transparent px-0-lg"> 
            {{ Product::product_bredacrumps(DB::table('roba')->where('roba_id',$roba_id)->pluck('grupa_pr_id')) }}
        </ul>
     
        <div class="row article-details-wrapper"> 
            
            <div class="JSproduct-preview-image col-md-7 col-sm-12 col-xs-12">
                <div class="bg-white radius-15 preview-image h-full">
                    <button class="p-1 fullscreen-btn relative">
                        <img 
                            src="{{ Options::base_url() }}images/fullscreen.svg" 
                            alt="fullscreen"
                            width="15"
                            height="15"   
                        />
                        <span class="tooltiptext">Uvećaj (Fullscreen)</span>
                    </button>
    
                    <div class="row sm-col-reverse" style="align-items: unset;"> 
                        <div class="col-md-2 col-sm-12 col-xs-12">
                            <div class="text-center sm-no-padd slider-for JSproduct-gallery">
                                @foreach($slike as $image)
                                <div>
                                    @if(Product::get_file_extension($image->putanja) == 'webm') 
                                        <a href="javascript:void(0)" class="img-gallery elevatezoom-gallery JS_product_video JSimg-gallery"> 
                                            <video autoplay muted loop playsinline class="img-responsive " alt="{{ Options::domain().$image->putanja }}" id="{{ Options::domain().$image->web_slika_id }}" src="{{ Options::domain().$image->putanja }}"/></video>
                                        </a>
                                    @else 
                                        <a href="javascript:void(0)" class="img-gallery elevatezoom-gallery JS_product_image JSimg-gallery"> 
                                            <img alt="{{ Options::domain().$image->putanja }}" id="{{ Options::domain().$image->web_slika_id }}" src="{{ Options::domain().$image->putanja }}" decoding="async" loading="lazy" />
                                        </a>
                                    @endif
                                </div>
                                @endforeach
                            </div>
                        </div>

                        <div class="disableZoomer relative col-md-10 col-sm-12 col-xs-12">  

                            <!-- @if(Product::get_file_extension($slika_big) == 'webm') 
                                <img class="JSmain_img img-responsive" src="" alt="" />  
                                <video autoplay loop muted playsinline class="JSmain_vid img-responsive" src="{{ Options::domain() }}{{ $slika_big }}" alt="{{ Product::seo_title($roba_id)}}" decoding="async" loading="lazy" /></video>
                            @else
                                <img class="JSmain_img img-responsive" src="{{ Options::domain() }}{{ $slika_big }}" alt="{{ Product::seo_title($roba_id)}}" decoding="async" loading="lazy" />  
                                <video autoplay loop muted playsinline class="JSmain_vid JSmain_vid_empty img-responsive" src="" alt="" /></video>
                            @endif -->

                            <div class="text-center sm-no-padd slider-nav JSproduct-main-slick">
                                @foreach($slike as $image)
                                <div>
                                    @if(Product::get_file_extension($image->putanja) == 'webm') 
                                        <a href="javascript:void(0)"> 
                                            <video autoplay muted loop playsinline class="img-responsive " alt="{{ Options::domain().$image->putanja }}" id="{{ Options::domain().$image->web_slika_id }}" src="{{ Options::domain().$image->putanja }}"/></video>
                                        </a>
                                    @else 
                                        <a href="javascript:void(0)"> 
                                            <img alt="{{ Options::domain().$image->putanja }}" id="JSmain-img" src="{{ Options::domain().$image->putanja }}" class="img-gallery elevatezoom-gallery flex JS_product_image JSimg-gallery" decoding="async" loading="lazy" />
                                        </a>
                                    @endif
                                </div>
                                @endforeach
                            </div>
    
                            <!-- STICKER -->
                            <div class="product-sticker flex">
                                @if(B2bArticle::stiker_levo($roba_id) != null )
                                    <span class="article-sticker-img">
                                        <img class="img-responsive" src="{{ Options::domain() }}{{B2bArticle::stiker_levo($roba_id) }}" alt="sticker-left" />
                                    </span>
                                @endif 
                                
                                @if(B2bArticle::stiker_desno($roba_id) != null )
                                    <span class="article-sticker-img clearfix">
                                        <img class="img-responsive pull-right" src="{{ Options::domain() }}{{B2bArticle::stiker_desno($roba_id) }}" alt="sticker-right" />
                                    </span>
                                @endif   
                            </div>
    
                            <!-- CUSTOM MODAL -->
                            <div class="JSmodal articleModal">
                                <div class="flex full-screen relative"> 
                                
                                    <div class="modal-cont relative text-center"> 
                                
                                        <div class="inline-block relative"> 
                                            <span class="JSclose-modal">
                                                <img 
                                                    src="{{ Options::base_url() }}images/cursor-close.png" 
                                                    alt="close mini cart"
                                                    width="22"
                                                    height="22"
                                                    class="pointer"
                                                />
                                            </span>
                                    
                                            <img src="" class="img-responsive" id="JSmodal_img" alt="modal image" decoding="async" loading="lazy" />
                                        </div>
    
                                        <button class="btn JSleft_btn"><i class="fas fa-chevron-left"></i></button>
                                        <button class="btn JSright_btn"><i class="fas fa-chevron-right"></i></button>
                                    </div>
                            
                                </div>
                            </div>
                        </div> 
                    </div>

                </div>
            </div>

            <div class="product-preview-info col-md-5 col-sm-12 col-xs-12 bg-white radius-15">
                <!-- FOR SEO -->
                @if(!empty(Product::sifra_is($roba_id)))
                <h1 class="article-heading">{{ Product::seo_title($roba_id) }}</h1>
                @else
                <h1 class="article-heading">{{ Product::seo_title($roba_id) }}</h1>
                @endif

                <!-- Review -->
                <div class="flex gap-2 items-center">
                    <div class="review">{{ Product::getRating($roba_id) }}</div>
                    <span class="flex gap-1">
                        <img 
                            src="{{ Options::domain() }}images/fire.svg" 
                            alt="fire"
                            width="15"
                            height="15"
                        />
                        <p class="no-margin text-primary">
                            <span class="num_seen"></span> 
                            prodato u poslednjih 30 dana
                        </p>
                    </span>
                </div>

                <div class="row">
                    <div class="col-xs-12 no-padding">
                        <!-- <div class="col-xs-6 no-padding">
                            @if($proizvodjac_id != -1)
                                @if( Product::slikabrenda($roba_id) != null )
                            
                                <a class="article-brand-img inline-block" href="{{Options::base_url()}}{{Url_mod::slug_trans('proizvodjac')}}/{{Url_mod::slug_trans(Product::get_proizvodjac($roba_id)) }}">
                                    <img src="{{ Options::domain() }}{{product::slikabrenda($roba_id) }}" alt="{{ product::get_proizvodjac($roba_id) }}" />
                                </a>

                                @else

                                <a href="{{Options::base_url()}}{{Url_mod::slug_trans('proizvodjac')}}/{{Url_mod::slug_trans(Product::get_proizvodjac($roba_id)) }}" class="artical-brand-text inline-block">{{ product::get_proizvodjac($roba_id) }}</a>

                                @endif                                   
                            @endif     
                        </div> -->

                        <!-- <div class="col-xs-6 text-right text-bold" style="float: right;"> -->
                            <!-- ARTICLE PASSWORD -->
                            <!-- @if(AdminOptions::sifra_view_web()==1)
                            <div>{{Language::trans('Šifra') }}: {{Product::sifra($roba_id)}}</div>
                            @elseif(AdminOptions::sifra_view_web()==4)                       
                            <div>{{Language::trans('Šifra') }}: {{Product::sifra_d($roba_id)}}</div>
                            @elseif(AdminOptions::sifra_view_web()==3)                       
                            <div>{{Language::trans('Šifra') }}: {{Product::sku($roba_id)}}</div>
                            @elseif(AdminOptions::sifra_view_web()==2)                       
                            <div>{{Language::trans('Šifra') }}: {{Product::sifra_is($roba_id)}}</div>
                            @endif   
                            
                            <div class="review hidden">{{ Product::getRating($roba_id) }}</div>
                        </div> -->
                    </div>
                </div>
                    


                <ul>
                    <!-- @if($proizvodjac_id != -1) 
                    <li class="hidden">{{Language::trans('Proizvođač')}}: 

                        @if(Support::checkBrand($roba_id))
                        <a class="text-bold" style="color: #666" href="{{ Options::base_url() }}{{ Url_mod::slug_trans('proizvodjac')}}/{{ Url_mod::slug_trans(Product::get_proizvodjac($roba_id)) }}">{{Product::get_proizvodjac($roba_id)}}</a>
                        @else
                        <span>{{Product::get_proizvodjac($roba_id)}}</span>
                        @endif
                    </li>
                    @endif  -->

                    @if(Options::checkTezina() == 1 AND Product::tezina_proizvoda($roba_id)>0)
                    <li>{{Language::trans('Težina artikla')}}: {{Product::tezina_proizvoda($roba_id)/1000}} kg</li>
                    @endif 
                </ul>

                <!-- ENERGY CLASS -->
                @if(AdminOptions::energy_class()==1)
                <div class="row flex JSenergyClassToggle" energy-data="{{ Product::get_energetska_klasa_tip($roba_id) }}">
                    <div class="col-md-3 col-sm-3 col-xs-3 no-padding">
                        {{ Language::trans('Energetska klasa') }}: 
                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-9 no-padding flex">
                        @if(Product::get_energetska_klasa_old_or_new($roba_id) == 0)
                        <div class="oldEnergyClassMark flex">A <i class="fas fa-arrow-up"></i> G</div>
                        @endif
                        <div class="energyClassWrap">
                            <a href="{{ Product::get_energetska_klasa_link($roba_id) }}" target="_blank">
                                <span class="energyClass flex" energy-class="{{ Product::get_energetska_klasa_tip($roba_id) }}"><span>{{ Product::get_energetska_klasa($roba_id) }}</span></span>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 no-padding energyLinkWrap">
                        @if(Product::get_energetska_klasa_old_or_new($roba_id) == 1)
                            <a href="{{ Options::base_url() }}novi-standardi-za-klase-energetske-efikasnosti" target="_blank">
                                <div>{{ Language::trans('Napomena: Po prethodnoj klasifikaciji') }}</div>
                            </a>
                        @endif
                            <a href="{{Options::base_url()}}product-deklaracija/{{ $roba_id }}" target="_blank">
                                <span class="energyLink">{{ Language::trans('Lista sa podacima aparata') }}</span>
                            </a>
                    </div>
                </div>
                @endif

                <!-- PRICE -->
                <div class="product-preview-price-wrapper">
                    <div class="product-preview-price">
                        @if(Product::getStatusArticlePrice($roba_id) == 1)
    
                            <!--@if(Product::pakovanje($roba_id)) 
                            <div> 
                                <span class="price-label">{{ Language::trans('Pakovanje') }}:</span>
                                <span class="price-num">{{ Product::ambalaza($roba_id) }}</span> 
                            </div>
                            @endif  -->                                
    
                            @if(All::provera_akcija($roba_id))    
    
                            <!-- @if(Product::get_mpcena($roba_id) != 0) 
                            <div>
                                <span class="price-label">{{ Language::trans('Maloprodajna cena') }}:</span>
                                <span class="price-num mp-price">{{ Cart::cena(Product::get_mpcena($roba_id)) }}</span> 
                            </div>
                            @endif   -->
    
                            <div class="flex flex-col-reverse align-items-start"> 
                                <!-- <span class="price-label action-title">{{ Language::trans('Akcijska cena')}}:</span>  -->
                                <span class="JSaction_price price-num main-price text-black" data-action_price="{{Product::get_price($roba_id)}}">
                                    {{ Cart::cena(Product::get_price($roba_id,true,false,(!is_null(Input::old('kamata')) ? Input::old('kamata') : 0 ))) }}
                                </span>
                                <span class="price-num mp-price action-price">MP cena: {{ Cart::cena(Product::get_mpcena($roba_id)) }}</span> 
    
                            </div>
    
                            @if(All::action_start($roba_id) || All::action_deadline($roba_id))
                                <span class="action_text_info">
                                    {{ Language::trans('Akcije traje od') }}: {{ All::action_start($roba_id) }} {{ Language::trans('do') }} {{ All::action_deadline($roba_id) }} {{ Language::trans('ili do isteka zaliha') }}
                                </span>
                            @endif
    
                            @if(Product::getPopust_akc($roba_id)>0)
                            <div> 
                                <!-- <span class="price-label">{{ Language::trans('Popust') }}: </span> -->
                                <span class="price-num discount text-uppercase">Ušteda: {{ Cart::cena(Product::getPopust_akc($roba_id)) }}</span>
                            </div>
                            @endif
    
                            @else
    
                            @if(Product::get_mpcena($roba_id) != 0) 
                            <div> 
                                <!-- <span class="price-label">{{ Language::trans('Maloprodajna cena') }}:</span> -->
                                <span class="price-num mp-price">MP cena: {{ Cart::cena(Product::get_mpcena($roba_id)) }}</span> 
                            </div>
                            @endif
    
                            <div> 
                                <!-- <span class="price-label">{{ Language::trans('WebCena') }}:</span> -->
                                <!-- <span class="price-label">{{ Language::trans('Cena') }}:</span> -->
                                <span class="JSweb_price price-num main-price" data-cena="{{Product::get_price($roba_id)}}">
                                   {{ Cart::cena(Product::get_price($roba_id,true,false,(!is_null(Input::old('kamata')) ? Input::old('kamata') : 0 ))) }}
                               </span>
                           </div>
    
                            <!-- @if(Product::getPopust($roba_id)>0)
                               @if(AdminOptions::web_options(132)==1)
                               <div> 
                                <span class="price-label">{{ Language::trans('Popust') }}:</span>
                                <span class="price-num discount">{{ Cart::cena(Product::getPopust($roba_id)) }}</span>
                                </div>
                                @endif
                            @endif -->
                            <hr>
                        @endif
                    @endif 

                </div>
                <div class="installments">
                    
                </div>
            </div>

            <!-- Watching -->
            <div class="currently_watching my-3">
                <span class="flex gap-1">
                    <img 
                        src="{{ Options::domain() }}images/watching.svg" 
                        alt="watching"
                        width="35"
                        height="35"
                    />
                    <p class="no-margin">
                        <span class="people_seen"></span> 
                        osoba trenutno gleda ovo
                    </p>
                </span>
            </div>
            <hr>

            <!-- Ask Question - Share -->
            <div class="ask-question-share my-2">
                <div class="ask-question-overlay"></div>
                <div class="ask-question-share-content my-3">
                    <button class="ask-question-btn">
                        <img 
                            src="{{ Options::domain() }}images/question.svg" 
                            alt="question"
                            width="20"
                            height="20"
                        />
                        Postavi pitanje
                    </button>

                    <button class="share-btn">
                        <img 
                            src="{{ Options::domain() }}images/share.svg" 
                            alt="share"
                            width="20"
                            height="20"
                        />
                        Podeli
                    </button>
                </div>

                <!-- Share Modal -->
                <div class="share-modal radius-5 p-3">
                    <div class="share-modal-header text-right">
                        <button type="button" data-dismiss="modal">
                            <img 
                                src="{{ Options::domain() }}images/cursor-close.png"
                                alt="close share modal" 
                                width="16" 
                                height="16" 
                                class="pointer" 
                                style="filter: invert(1)"
                            />
                        </button>
                    </div>
                    <div class="share-modal-main">
                        <span class="text-bold fs-20">Copy Link</span>
                        <div class="flex gap-1 my-1">
                            <input type="text" value="{{ Options::base_url() }}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($roba_id))}}" class="flex-1" />
                            <button class="copy-btn radius-50">
                                <img 
                                    id="copyIcon"
                                    src="{{ Options::domain() }}images/copy.svg"
                                    alt="copy link" 
                                    width="14" 
                                    height="14" 
                                    class="pointer" 
                                />
                                <img 
                                    id="checkIcon"
                                    src="{{ Options::domain() }}images/check-copy.svg"
                                    alt="check copy" 
                                    width="16" 
                                    height="16" 
                                    class="pointer hidden" 
                                />
                            </button>
                        </div>

                        <div class="share-options">
                            <span class="text-bold my-1 inline-block">Podeli</span>
                            <div class="flex gap-1">
                                <div class="social social-icons social-icons-share flex gap-1">
                                    {{ Options::social_icon() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Ask Question Modal -->
                <div class="ask-question-modal radius-5">
                    <div class="ask-question-modal-header text-right flex items-center p-2 border-b-gray">
                        <h3 class="fs-20 text-center flex-1 text-bold">Postavite pitanje</h3>
                        <button type="button" data-dismiss="modal">
                            <img 
                                src="{{ Options::domain() }}images/cursor-close.png"
                                alt="close share modal" 
                                width="16" 
                                height="16" 
                                class="pointer" 
                                style="filter: invert(1)"
                            />
                        </button>
                    </div>

                    <div class="ask-question-modal-main p-3">
                        <form method="POST" action="{{ Options::base_url() }}contact-message-send-article">
                            <div class="flex justify-between gap-1 mb-1">
                                <div class="flex-1">
                                    <input class="contact-name px-2" name="contact-name-article" id="JScontact_name_article" type="text" value="{{ Input::old('contact-name') }}" placeholder="Ime" />
                                    <div class="error red-dot-error">{{ $errors->first('contact-name-article') ? $errors->first('contact-name-article') : "" }}</div>
                                </div> 

                                <div class="flex-1">
                                    <input class="contact-email px-2" name="contact-email-article" id="JScontact_email_article" type="text" value="{{ Input::old('contact-email') }}" placeholder="Email" />
                                    <div class="error red-dot-error">{{ $errors->first('contact-email-article') ? $errors->first('contact-email-article') : "" }}</div>
                                </div>
                            </div>

                            <div>	
                                <textarea class="contact-message radius-15 px-2" name="contact-message-article" rows="5" id="message_article" placeholder="Vaša poruka">{{ Input::old('contact-message') }}</textarea>
                                <div class="error red-dot-error">{{ $errors->first('contact-message-article') ? $errors->first('contact-message-article') : "" }}</div>
                            </div> 

                            <div> 
                                <button type="submit" class="button w-full radius-50 transition-sm">{{ Language::trans('Pošalji') }}</button>
                            </div>
                        </form>
                    </div>
                </div>

            </div>

            <hr>

            <div class="add-to-cart-area clearfix">    

             @if(Product::getStatusArticle($roba_id) == 1)
                 @if(Cart::check_avaliable($roba_id) > 0)
                 <form method="POST" action="{{ Options::base_url() }}product-cart-add" id="JSAddCartForm"> 
                  
                    <!-- PITCHPRINT -->
                    @if(!empty(Product::design_id($roba_id) AND !empty(Options::pitchprint() AND Options::pitchprint_aktiv() == 1)))
                        @include('shop/themes/'.Support::theme_path().'partials/pitchprint')
                    @endif
                    <!-- PITCHPRINT END -->

                @if(Product::check_osobine($roba_id))  
                    @foreach(Product::osobine_nazivi($roba_id) as $osobina_naziv_id)

                    <div class="attributes">

                        <p>{{ Product::find_osobina_naziv($osobina_naziv_id,'naziv') }}</p>

                        @foreach(Product::osobine_vrednosti($roba_id,$osobina_naziv_id) as $osobina_vrednost_id)
                        <label class="relative no-margin" style="background-color: {{ Product::find_osobina_vrednost($osobina_vrednost_id, 'boja_css') }}">
                            <span class="tooltip-char">{{ Product::find_osobina_vrednost($osobina_vrednost_id, 'vrednost') }}</span>

                            <input type="radio" name="osobine{{ $osobina_naziv_id }}" value="{{ $osobina_vrednost_id }}" {{ Product::check_osobina_vrednost($roba_id,$osobina_naziv_id,$osobina_vrednost_id,Input::old('osobine'.$osobina_naziv_id)) }}>

                            <span class="inline-block">{{ Product::find_osobina_vrednost($osobina_vrednost_id, 'vrednost') }}</span>
                            
                        </label>
                        @endforeach

                    </div>

                    @endforeach 
                @endif

                <div class="cart-inputs-wrapper flex items-center gap-1">
                    <div class="cart-inputs-left flex-1">
                        <div class="qty-cart-input radius-50">
                            <div class="cart-add-amount flex items-center justify-between mx-1">
                                <div>
                                    <button onClick="var result = document.getElementById('productAmountCart'); var productAmountCart = result.value; if( !isNaN( productAmountCart ) &amp;&amp; productAmountCart &gt; 0 ) result.value--;return false;" class="product-count" aria-label="product amount" type="button"><i class="fa fa-minus"></i></button>
                                </div>
        
                                <div>
                                    <input id="productAmountCart" type="text" name="kolicina" class="cart-amount" value="{{ Input::old('kolicina') ? Input::old('kolicina') : '1' }}" aria-labelledby="productAmountCart">
                                </div>
        
                                <div>
                                    <button onClick="var result = document.getElementById('productAmountCart'); var productAmountCart = result.value; if( !isNaN( productAmountCart )) result.value++;return false;" class="product-count" aria-label="product amount" type="button"><i class="fa fa-plus"></i></button>            
                                </div>  
                            </div>
                        </div>
                    </div>    

                    <div class="cart-inputs-right flex sm-justify-center">
                        @if(AdminOptions::web_options(313)==1) 
                        <div class="num-rates"> 
                            <div> 
                                <div class="inline-block lorem-1">{{ Language::trans('Broj rata') }}</div>
                            </div>
                            <select class="JSinterest" name="kamata">
                                {{ Product::broj_rata(Input::old('kamata')) }}
                            </select>
                        </div>
                        @endif
        
                        @if(Cart::kupac_id() > 0)
                        <button class="like-it JSadd-to-wish" type="button" data-roba_id="{{$roba_id}}" title="{{ Language::trans('Dodaj na listu želja') }}">
                            <img 
                                src="{{ Options::base_url() }}images/wish.svg" 
                                alt="wish list"
                                width="16"
                                height="16"
                            />
                            <span class="tooltiptext"> {{ Language::trans('Dodaj u listu želja') }} </span>
                        </button>
                        @else
                        <button class="like-it JSnot_logged" type="button" data-roba_id="{{$roba_id}}" title="{{ Language::trans('Dodavanje artikla na listu želja moguće je samo registrovanim korisnicima') }}">
                            <img 
                                src="{{ Options::base_url() }}images/wish.svg" 
                                alt="wish list"
                                width="16"
                                height="16"
                            />
                            <span class="tooltiptext"> {{ Language::trans('Dodaj u listu želja') }} </span>
                        </button> 
                        @endif
        
                        <input type="hidden" name="roba_id" value="{{ $roba_id }}">
                        @if(Options::web_options(320) == 1 AND (Product::jedinica_mere($roba_id)->jedinica_mere_id) == 3 AND Product::pakovanje($roba_id) ) 
                            <input type="number" name="kolicina" class="cart-amount weight" min="0" step="0.1" value="{{ Input::old('kolicina') ? Input::old('kolicina') : '1' }}"><span>&nbsp;{{Language::trans(' kg')}}&nbsp;</span>
                        @else
                        <!-- <input type="text" name="kolicina" class="cart-amount" value="{{ Input::old('kolicina') ? Input::old('kolicina') : '1' }}"> -->
                        
                        @endif
                        <button type="submit" id="JSAddCartSubmit" class="add-to-cart button">{{Language::trans('Dodaj u korpu')}}</button>
                        <input type="hidden" name="projectId" value=""> 
        
                        
                        <button class="JScompare like-it {{ All::check_compare($roba_id) ? 'active' : '' }}" data-id="{{$roba_id}}" title="{{ Language::trans('Uporedi') }}">
                            <img 
                            src="{{ Options::base_url() }}images/compare.svg" 
                            alt="compare"
                            width="16"
                            height="16"
                            />
                            <span class="tooltiptext"> {{ Language::trans('Uporedi') }} </span>
                        </button>
                    </div>
                    <div class="red-dot-error">{{ $errors->first('kolicina') ? $errors->first('kolicina') : '' }}</div>  
                </form>

                @else
    
                <button class="not-available button">{{Language::trans('Nije dostupno')}}</button>       
                @endif
    
                @else
                <!-- <button class="button" data-roba-id="{{$roba_id}}">
                    {{ Product::find_flag_cene(Product::getStatusArticle($roba_id),'naziv') }}
                </button> -->
    
                <!-- <button class="not-available button"><span class="inline-block cart-me"></span>  {{Language::trans('Nije dostupno')}}</button>        -->
    
                @if(Product::find_flag_cene(Product::getStatusArticle($roba_id),'naziv') == 'Na upit')
                    <button class="button JSenquiry" data-roba-id="{{$roba_id}}">{{Product::find_flag_cene(Product::getStatusArticle($roba_id),'naziv')}}</button> 
                    @elseif(Product::find_flag_cene(Product::getStatusArticle($roba_id), 'naziv') == 'Proveri dostupnost')
                     <a href="tel:064/825-00-00" class="call-button inline-block button checkAvailability" style="margin: 5px;"><span class="inline-block fas fa-phone"></span>  {{ Language::trans('Proveri dostupnost') }}</a>
                    @else
                    <button class="buy-button button soon-on-stock-btn" data-roba-id="{{$roba_id}}">
                        {{ Product::find_flag_cene(Product::getStatusArticle($roba_id),'naziv') }}
                    </button>
                    <!-- <a href="tel:064/825-00-00" class="call-button inline-block button"><span class="inline-block fas fa-phone"></span>  {{ Language::trans('Pozovite') }}</a> -->
                @endif
    
                @endif
            </div>

            <div class="delivery-info my-2 flex gap-1">
                <div class="flex gap-1 no-wrap">
                    <img 
                        src="{{ Options::base_url() }}images/boat.svg" 
                        alt="boat"
                        width="16"
                        height="16"
                    />
                    <span>Estimate delivery times: <b>3-6 days</b> (International)</span>
                </div>

                <div class="flex gap-1 no-wrap">
                    <img 
                        src="{{ Options::base_url() }}images/return.svg" 
                        alt="return"
                        width="16"
                        height="16"
                    />
                    <span>Return within <b>45 days</b> of purchase. Duties & taxes are non-refundable.</span>
                </div>
            </div>

            <hr><br>

            <div class="article-info-below">
                <div class="row">
                    <div class="col-xs-12 no-padding">
                        <ul class="flex gap-1">
                            @if(AdminOptions::sifra_view_web()==1)
                                <li class="w-full">
                                    <div><span class="inline-block" style="min-width: 14rem;">{{Language::trans('Šifra') }}: </span> <span>{{Product::sifra($roba_id)}}</span></div>
                                    @elseif(AdminOptions::sifra_view_web()==4)                       
                                    <div><span class="inline-block" style="min-width: 14rem;">{{Language::trans('Šifra') }}:</span> <span>{{Product::sifra_d($roba_id)}}</span></div>
                                    @elseif(AdminOptions::sifra_view_web()==3)                       
                                    <div><span class="inline-block" style="min-width: 14rem;">{{Language::trans('Šifra') }}:</span> <span>{{Product::sku($roba_id)}}</span></div>
                                    @elseif(AdminOptions::sifra_view_web()==2)                       
                                    <div><span class="inline-block" style="min-width: 14rem;">{{Language::trans('Šifra') }}:</span> <span>{{Product::sifra_is($roba_id)}}</span></div>
                                </li>
                            @endif   
                            
                            @if(Options::vodjenje_lagera() == 1)
                            <li class="w-full">
                                <span class="inline-block" style="min-width: 14rem;">Stanje proizvoda: </span>
                                
                                @if(Cart::check_avaliable($roba_id,'true')>0)
                                    <span class="available-green text-bold">{{Language::trans('Na stanju')}}</span>
                                @else
                                    <span class="not-available-red text-bold">{{Language::trans('Nema na stanju')}}</span>
                                @endif
                            </li>
                            @endif

                            <li class="w-full">
                                <span class="inline-block" style="min-width: 14rem;">Kategorija:</span>
                                <span>{{ Product::get_grupa($roba_id) }}</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            
         </div>

        <hr>

        <br>
        @if(!empty(Product::get_labela($roba_id)))
        <div class="custom-label inline-block relative">
            <i class="fa fa-info-circle"></i>
            {{Product::get_labela($roba_id)}} 
        </div>
        @endif
       

        <!-- ADMIN BUTTON-->
        @if(Session::has('b2c_admin'.Options::server()) AND Admin_model::check_admin(array('ARTIKLI')) AND Admin_model::check_admin(array('ARTIKLI_PREGLED')))
        <div class="admin-article"> 
            @if(Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
            <a class="JSFAProductModalCall article-level-edit-btn" data-roba_id="{{$roba_id}}" href="javascript:void(0)" rel="nofollow">{{ Language::trans('IZMENI ARTIKAL') }}</a> 
            @endif
            <span class="supplier"> {{ Product::get_dobavljac($roba_id) }}</span> 
            <span class="supplier">{{ Language::trans('NCP') }}: {{ Product::get_ncena($roba_id) }}</span>
        </div>
        @endif
    </div>
    </div>

    <div class="row">
        <div class="col-md-7 col-sm-12 col-xs-12">
            <!-- TABS -->
            <div class="tabs-wrap p-4 bg-white radius-15">
                <ul class="nav nav-tabs tab-titles">
                    <li class="{{ !Session::has('contactError') ? 'active' : '' }}"><a data-toggle="tab" href="#service_desc-tab">{{Language::trans('Opis')}}</a></li>
                    <li><a data-toggle="tab" href="#description-tab">{{Language::trans('Specifikacije')}}</a></li>
                    <li><a data-toggle="tab" href="#decleration-tab">{{Language::trans('Deklaracija')}}</a></li>
                    <li class="{{ Session::has('contactError') ? 'active' : '' }}"><a data-toggle="tab" href="#the-comments">{{Language::trans('Komentari')}}</a></li>
                </ul>

                <div class="tab-content"> 

                    <div data-nosnippet id="service_desc-tab" class="tab-pane fade{{ !Session::has('contactError') ? ' in active' : '' }}">
                        {{ Product::get_opis($roba_id) }} 
                        
                        @if(Product::get_proizvodjac_name($roba_id)=='Bosch' OR Product::get_proizvodjac_name($roba_id)=='BOSCH')
                        <script type="text/javascript"></script>

                        <div class="loadbeeTabContent" data-loadbee-apikey="{{Options::loadbee()}}" data-loadbee-gtin="{{Product::get_barkod($roba_id)}}" data-loadbee-locale="sr_RS"></div>

                        <script src="https://cdn.loadbee.com/js/loadbee_integration.js" async=""></script>
                        @endif
                        <div id="flix-minisite"></div>
                        <div id="flix-inpage"></div>
                        <!-- fixmedia -->
                        <script type="text/javascript" src="//media.flixfacts.com/js/loader.js" data-flix-distributor="{{Options::flixmedia()}}" data-flix-language="rs" data-flix-brand="{{Product::get_proizvodjac_name($roba_id)}}" data-flix-mpn="" data-flix-ean="{{Product::get_barkod($roba_id)}}" data-flix-sku="" data-flix-button="flix-minisite" data-flix-inpage="flix-inpage" data-flix-button-image="" data-flix-price="" data-flix-fallback-language="en" async>                                    
                        </script>
                    </div>

                    <div id="description-tab" class="tab-pane fade">
                        {{ Product::get_karakteristike($roba_id) }}
                    </div>

                    <div id="decleration-tab" class="tab-pane fade">
                        <ul class="features-list flex">
                            
                            <li>{{ Language::trans('Naziv artikla:') }} </li>
                            <li>{{ Language::trans(Product::seo_title($roba_id)) }} </li>
                            <li>{{ Language::trans('Naziv i vrsta robe:') }} </li>
                            <li>{{ Product::get_grupa($roba_id) }} </li>
                            <li>{{ Language::trans('Uvoznik:') }} </li>
                            <li>{{ 'Dostupno na ambalaži proizvoda' }} </li>
                            <li>{{ Language::trans('Zemlja porekla:') }}</li>
                            <li>{{ 'Dostupno na ambalaži proizvoda' }}</li>
                            <li>{{ Language::trans('Prava potrošača:') }}</li>
                            <li>{{'Zagarantovana sva prava kupaca po osnovu zakona o zaštiti potrošača'}} </li>
                        </ul>
                    </div>
            
                    <div id="the-comments" class="tab-pane fade{{ Session::has('contactError') ? ' in active' : '' }}">
                        <div class="row"> 
                            <?php $query_komentary=DB::table('web_b2c_komentari')->where(array('roba_id'=>$roba_id,'komentar_odobren'=>1));
                            if($query_komentary->count() > 0){?>
                                <div class="col-md-12 col-sm-12 col-xs-12"> 
                                    <ul class="comments">
                                        <?php foreach($query_komentary->orderBy('web_b2c_komentar_id', 'DESC')->get() as $row)
                                        { ?>
                                            <li class="comment">
                                                <ul class="comment-content relative">
                                                    <li class="comment-name">{{$row->ime_osobe}}</li>
                                                    <li class="comment-date">{{$row->datum}}</li>
                                                    <li class="comment-rating">{{Product::getRatingStars($row->ocena)}}</li>
                                                    <li class="comment-text">{{ $row->pitanje }}</li>
                                                </ul>
                                                <!-- REPLIES -->
                                                @if($row->odgovoreno == 1)
                                                <ul class="replies">
                                                    <li class="comment">
                                                        <ul class="comment-content relative">
                                                            <li class="comment-name">{{ Options::company_name() }}</li>
                                                            <li class="comment-text">{{ $row->odgovor }}</li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                                @endif
                                            </li>
                                        <?php }?>
                                    </ul>
                                </div>
                            <?php } ?>
                            <div class="col-md-12 col-sm-12 col-xs-12"> 
                                <form method="POST" action="{{ Options::base_url() }}comment-add">
                                    <label>{{Language::trans('Vaše ime')}}</label>
                                    <input name="comment-name" type="text" value="{{ Input::old('comment-name') }}" {{ $errors->first('comment-name') ? 'style="border: 1px solid red;"' : '' }} />
                            
                                    <label for="JScomment_message">{{Language::trans('Komentar')}}</label>
                                    <textarea class="comment-message" name="comment-message" rows="5"  {{ $errors->first('comment-message') ? 'style="border: 1px solid red;"' : '' }}>{{ Input::old('comment-message') }}</textarea>
                                    <input type="hidden" value="{{ $roba_id }}" name="comment-roba_id" />
                                    <span class="review JSrev-star">
                                        <span>{{Language::trans('Ocena')}}:</span>
                                        <i id="JSstar1" class="far fa-star review-star" aria-hidden="true"></i>
                                        <i id="JSstar2" class="far fa-star review-star" aria-hidden="true"></i>
                                        <i id="JSstar3" class="far fa-star review-star" aria-hidden="true"></i>
                                        <i id="JSstar4" class="far fa-star review-star" aria-hidden="true"></i>
                                        <i id="JSstar5" class="far fa-star review-star" aria-hidden="true"></i>
                                        <input name="comment-review" id="JSreview-number" value="0" type="hidden"/>
                                    </span>
                                    <div class="capcha text-center"> 
                                        {{ Captcha::img(5, 160, 50) }}<br>
                                        <span>{{ Language::trans('Unesite kod sa slike') }}</span>
                                        <input type="text" name="captcha-string" tabindex="10" autocomplete="off" {{ $errors->first('captcha') ? 'style="border: 1px solid red;"' : '' }}>
                                    </div>
                                    <button class="pull-right button radius-50">{{Language::trans('Pošalji')}}</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
          </div>
        </div>
    </div>

    @if(count($srodni_artikli) > 0)
        <div class="related-custom JSproducts_slick row"> 
        @foreach($srodni_artikli as $srodni_artikl)
        <div class="JSproduct col-md-4 col-sm-4 col-xs-12">  
            <div class="card flex row">  
                <div class="col-xs-3 no-padding">
                    <div class="img-wrap"> 
                        <a href="{{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($srodni_artikl->srodni_roba_id))}}">
                            <img class="img-responsive" src="{{ Options::domain() }}{{ Product::web_slika_big($srodni_artikl->srodni_roba_id) }}" alt="{{ Product::seo_title($srodni_artikl->srodni_roba_id)}}" />
                        </a>
                    </div>
                </div>

                <div class="col-xs-9">
                    <a href="{{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($srodni_artikl->srodni_roba_id))}}">
                        <h2 class="title">{{ Product::seo_title($srodni_artikl->srodni_roba_id) }}</h2>
                    </a>

                    <div>{{ Product::get_karakteristika_srodni($srodni_artikl->grupa_pr_vrednost_id) }}</div>

                    <div class="price"> 
                        <span>{{ Cart::cena(Product::get_price($srodni_artikl->srodni_roba_id)) }}</span>
                    </div>  
                </div>   
            </div>
        </div>
        @endforeach 
        </div>
    @endif

    @if(Options::checkTags() == 1)
        @if(Product::tags($roba_id) != '')
        <div class="product-tags">  
            <div>{{ Language::trans('Tagovi') }}:</div>

            <h6 class="text-white inline-block">
                {{ Product::tags($roba_id) }} 
            </h6>
        </div>       
        @endif
    @endif    

    <!-- PRODUCT PREVIEW TABS-->
    <div id="product_preview_tabs" class="product-preview-tabs row container mx-auto padding-lg-x-0">
        <div class="col-xs-12 padding-lg-x-0""> 
                @if(Options::web_options(118))
                 <br>
                     @if(count($vezani_artikli))
                     <h2><span class="section-title">{{Language::trans('Vezani artikli')}}</span></h2>
                     
                     <div class="JSproducts_slick row">

                        @foreach($vezani_artikli as $vezani_artikl)
                        @if(Product::checkView($vezani_artikl->roba_id))
                        <div class="JSproduct col-md-3 col-sm-4 col-xs-12 no-padding">
                            <div class="shop-product-card relative related-product-card"> 
                                <!-- PRODUCT IMAGE -->
                                <div class="product-image-wrapper flex relative">
                                    <aside class="product-options-side">
                                        <div class="relative">
                                            @if(Cart::kupac_id() > 0)
                                            <button class="JSadd-to-wish" data-roba_id="{{$roba_id}}" title="{{ Language::trans('Dodaj na listu želja') }}">
                                                <img 
                                                    src="{{ Options::base_url() }}images/wish.svg" 
                                                    alt="wish list"
                                                />
                                                <span class="tooltiptext"> {{ Language::trans('Dodaj u listu želja') }} </span>
                                            </button> 
                                            @else
                                            <button class="JSnot_logged" data-roba_id="{{$roba_id}}" title="{{ Language::trans('Dodavanje artikla na listu želja moguće je samo registrovanim korisnicima') }}">
                                                <img 
                                                    src="{{ Options::base_url() }}images/wish.svg" 
                                                    alt="wish list"
                                                />
                                                <span class="tooltiptext"> {{ Language::trans('Dodaj u listu želja') }} </span>
                                            </button> 
                                            @endif	
                                        </div>

                                        <!-- Compare -->
                                        <div class="relative">
                                            <button class="JScompare {{ All::check_compare($roba_id) ? 'active' : '' }}" data-id="{{$vezani_artikl->vezani_roba_id}}" title="{{ Language::trans('Uporedi') }}">
                                                <img 
                                                    src="{{ Options::base_url() }}images/compare.svg" 
                                                    alt="compare"
                                                />
                                                <span class="tooltiptext"> {{ Language::trans('Uporedi') }} </span>
                                            </button>
                                        </div>

                                    </aside>

                                    <a class="margin-auto" href="{{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($vezani_artikl->vezani_roba_id))}}">
                                        <img 
                                            class="product-image img-responsive JSlazy_load firstImgProduct" 
                                            src="{{Options::domain()}}images/quick_view_loader.gif" 
                                            data-src="{{ Options::domain() }}{{ Product::web_slika($vezani_artikl->vezani_roba_id) }}" 
                                            alt="{{ Product::seo_title($vezani_artikl->vezani_roba_id) }}" 
                                            decoding="async"
                                        />
                                        <!-- Second Image -->
                                        <img 
                                            class="product-image img-responsive JSlazy_load secondImgHover" 
                                            src="{{Options::domain()}}images/quick_view_loader.gif" 
                                            data-src="{{ Options::domain() }}{{ Product::web_slika_second($vezani_artikl->vezani_roba_id) }}" 
                                            alt="{{ Product::seo_title($vezani_artikl->vezani_roba_id) }}" 
                                            decoding="async"
                                        />
                                    </a>

                                    <!-- <a href="{{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($vezani_artikl->vezani_roba_id))}}" class="article-details hidden-xs">
                                        <i class="fas fa-search-plus"></i> {{Language::trans('Detaljnije')}}</a> -->
                                    </div>

                                    <div class="product-meta">
                            			<span class="text-uppercase">{{ Product::get_grupa($vezani_artikl->vezani_roba_id) }}</span>

                                        <!-- PRODUCT PRICE -->
                                        <div class="price-holder">
                                            {{ Cart::cena(Product::get_price_vezani($vezani_artikl->roba_id,$vezani_artikl->vezani_roba_id)) }}
                                        </div>   

                                     <h2 class="product-name">
                                        <a href="{{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($vezani_artikl->vezani_roba_id))}}">  {{ Product::short_title($vezani_artikl->vezani_roba_id) }}</a>
                                    </h2>

                                    @if($vezani_artikl->flag_cena == 1)
                                    <div class="add-to-cart-container text-center">
                                        <!-- WISH LIST  --> 
                                        <!-- @if(Cart::kupac_id() > 0)
                                        <button class="like-it JSadd-to-wish" data-roba_id="{{$vezani_artikl->roba_id}}" title="{{Language::trans('Dodaj na listu želja')}}"><i class="far fa-heart"></i></button>
                                        @else
                                        <button class="like-it JSnot_logged" data-roba_id="{{$vezani_artikl->roba_id}}" title="{{Language::trans('Dodavanje artikla na listu želja moguće je samo registrovanim korisnicima') }}"><i class="far fa-heart"></i></button> 
                                        @endif    -->

                                        @if(Product::getStatusArticle($vezani_artikl->roba_id) == 1)
                                            @if(Cart::check_avaliable($vezani_artikl->vezani_roba_id) > 0)
                                                <button class="buy-btn button JSadd-to-cart-similar" data-vezani_roba_id="{{ $vezani_artikl->vezani_roba_id }}" data-roba_id="{{ $vezani_artikl->roba_id }}">
                                                    <img 
                                                        src="{{ Options::base_url() }}images/cart.svg" 
                                                        alt="add to cart"
                                                        width="20"
                                                        height="20"
                                                        style="filter: invert(1);"
                                                    />
                                                </button>
                                            <!-- <input type="text" class="JSkolicina linked-articles-input like-it" value="1" onkeypress="validate(event)"> -->
                                           
                                            @else  
                                           
                                            <button class="not-available button">{{Language::trans('Nije dostupno')}}</button>
                                           
                                            @endif
                                           
                                            @else
                                            <button class="buy-btn button">
                                                {{ Product::find_flag_cene(Product::getStatusArticle($vezani_artikl->roba_id),'naziv') }}
                                            </button>
                                        @endif
                                    </div>
                                    @endif
                                </div>
                                <!-- ADMIN BUTTON -->
                                @if(Session::has('b2c_admin'.Options::server()) AND Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
                                <a class="article-edit-btn JSFAProductModalCall" data-roba_id="{{$vezani_artikl->vezani_roba_id}}" href="#">{{Language::trans('IZMENI ARTIKAL')}}</a>
                                @endif
                            </div>
                        </div>
                        @endif
                        @endforeach 
                    </div> 
                    @endif
                @endif

                @if(count(Product::get_related($roba_id)))
                    <!-- RELATED PRODUCTS --> 
                    <br>
                    <h2><span class="section-title">{{Language::trans('Srodni proizvodi')}}</span></h2>
                    <div class="JSproducts_slick row related-products">
                        @foreach(Product::get_related($roba_id) as $row)
                            @include('shop/themes/'.Support::theme_path().'partials/products/product_on_grid')
                        @endforeach
                    </div>
                @endif

            </div>
        </div> 
    </div>  
</main>

<!-- ARTICLE_DETAILS.blade -->

@endsection