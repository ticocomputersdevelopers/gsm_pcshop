@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')
<div>
	<div class="row energyClassPage">
		<div class="col-md-12 col-sm-12 col-xs-12">
			 <h1 class="article-heading">{{ Product::seo_title($roba_id) }}</h1>
		</div>

		<div class="col-md-6 col-sm-6 col-xs-12">
			<img src="{{ Product::get_energetska_klasa_link($roba_id) }}" alt="energetska_klasa" class="img-responsive">
		</div>

		<div class="col-md-6 col-sm-6 col-xs-12">
			{{ Product::get_opis_deklaracija($roba_id) }}
		</div>
	</div>
</div>

@endsection