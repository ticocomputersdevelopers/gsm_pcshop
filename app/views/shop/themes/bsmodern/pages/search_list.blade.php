@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page') 
<div class="row">   
	<br><br>
	<h3>Rezultati pretrage za: <strong>“ {{ $pretraga }} ”</strong> </h3>
	<br>
	<div class="col-md-9 col-sm-9 col-xs-12 no-padding">
		<div class="row">
		@foreach($articles as $row)
			<div class="col-md-3 col-sm-3 col-xs-12 ">
				<div class="news relative">
				 	<a class="overlink" href="{{ Options::base_url() }}{{ Url_mod::slug_trans('blog') }}/{{ Url_mod::blog_link($row->web_vest_b2c_id) }}"></a>

		            @if(in_array(Support::fileExtension(Url_mod::blog_slike($row->web_vest_b2c_id)),array('jpg','png','jpeg','gif')))
		            	<div class="bg-img" href="{{ Options::base_url() }}{{ Url_mod::slug_trans('blog') }}/{{ Url_mod::blog_link($row->web_vest_b2c_id) }}" style="background-image: url('{{ Url_mod::blog_slike($row->web_vest_b2c_id) }}');"></div>
		            @else
		            	<iframe width="560" height="315" src="{{ Url_mod::blog_slike($row->web_vest_b2c_id) }}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>       
		            @endif
				   
					<h2 class="news-title overflow-hdn">{{ Url_mod::blog_naslov($row->web_vest_b2c_id) }} <br> {{ Url_mod::blog_kategorija($row->web_vest_b2c_id) }}</h2>
					<div><i class="far fa-clock"></i> {{Url_mod::blog_datum($row->web_vest_b2c_id)}}</div>

					<div class="news-desc overflow-hdn">{{ All::shortNewDesc(Url_mod::blog_sadrzaj($row->web_vest_b2c_id)) }}</div>

					<div class="text-center">
						<a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('blog') }}/{{ Url_mod::blog_link($row->web_vest_b2c_id) }}" class="button inline-block relative z-index-1">{{ Language::trans('Pročitaj članak') }}</a>
					</div> 
				</div>
			</div>
		@endforeach
		</div>
	</div>

	<!-- SEARCH -->
	<div class="col-md-3 col-sm-3 col-xs-12 articleSearch">
		<h4>Pretraga članaka</h4>
            <div class="row">  
                <div class="relative"> 
                    <form autocomplete="off">
                        <input type="text" id="JSsearchVest" placeholder="{{ Language::trans('Pretraga') }}" />
                    </form>      
                    <button onclick="search_vest()" class="JSsearch-button2"> <i class="fas fa-search"></i> </button>
                </div>
            </div> 
            <small class="text-dark text-top">Poslednji tekstovi</small>
        <div class="row">
			@foreach(All::getNewsAll() as $row)
				<div class="news relative JSRightArticles">

				<div class="row">
					<div class="col-lg-3 col-md-5 col-sm-5 col-xs-12">			
						<a class="overlink" href="{{ Options::base_url() }}{{ Url_mod::slug_trans('blog') }}/{{ Url_mod::blog_link($row->web_vest_b2c_id) }}"></a>
							@if(in_array(Support::fileExtension($row->slika),array('jpg','png','jpeg','gif')))						
								<div class="bg-img" href="{{ Options::base_url() }}{{ Url_mod::slug_trans('blog') }}/{{ Url_mod::blog_link($row->web_vest_b2c_id) }}" style="background-image: url('{{ $row->slika }}');"></div>
							@else
								<iframe width="560" height="315" src="{{ $row->slika }}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
							@endif
					</div>

					<div class="col-lg-6 col-md-6 col-sm-7 col-xs-12 no-padding sm-padding">
						
						<h2 class="news-title overflow-hdn JSArticleHeading">{{ $row->naslov }}</h2>
						<div class="calendar">
							<i class="far fa-calendar"></i> <span>{{$row->datum}}</span>
						</div>
						
						<div>
							<a class="news_btn" href="{{ Options::base_url() }}{{ Url_mod::slug_trans('blog') }}/{{ Url_mod::blog_link($row->web_vest_b2c_id) }}" class="button inline-block relative z-index-1">{{ Language::trans('Pročitaj članak') }} <i class="fas fa-caret-right"></i></a>
						</div> 
					</div>	
				</div>
				</div>
			@endforeach
		</div>
    </div>

	@if($count_products == 0) 
		<h4> <strong> {{ Language::trans('Termin za datu pretragu nije pronađen') }}. </strong> </h4>
	@endif

</div>
	<div class="text-center customPaginator">
		{{ Paginator::make($articles, $count_products, $limit)->links() }}
	</div>
<!-- SEARCH RESULT END -->
@endsection