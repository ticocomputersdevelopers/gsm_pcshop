@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page') 

<div class="row my-2">
	<div class="col-md-3 col-sm-3 col-xs-12 no-padding" id="single-new-left">
		<div class="JSsearchContent2 my-2">  
            <div class="relative"> 
                <form autocomplete="off">
                    <input type="text" id="JSsearchVest" placeholder="{{ Language::trans('Pretraga') }}" />
                </form>      
                <button onclick="search_vest()" class="JSsearch-button2"> <i class="fas fa-search"></i> </button>
            </div>
        </div>  

        @foreach(All::getNews() as $row)
			<div class="news relative JSRightArticles">
				<div class="row side-blog">
					<!-- <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">			 -->
						<div class="col-xs-6 no-padding">
						<div class="side-blog-image-wrap">
							<a class="overlink" href="{{ Options::base_url() }}blog/{{ Url_mod::blog_kategorija_link($row->web_vest_b2c_id) }}/{{ Url_mod::blog_link($row->web_vest_b2c_id) }}"></a>
								@if(in_array(Support::fileExtension($row->slika),array('jpg','png','jpeg','gif')))						
									<div class="bg-img" href="{{ Options::base_url() }}{{ Url_mod::slug_trans('blog') }}/{{ Url_mod::blog_link($row->web_vest_b2c_id) }}" style="background-image: url('{{ $row->slika }}');"></div>
								@else
									<iframe width="560" height="315" src="{{ $row->slika }}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
								@endif
						</div>
					</div>

						<div class="col-xs-6 no-padding">
						 <h2 class="news-title overflow-hdn JSArticleHeading">{{ Url_mod::blog_naslov($row->web_vest_b2c_id) }} <br> {{ Url_mod::blog_kategorija($row->web_vest_b2c_id) }}</h2>
						 <div class="calendar">
							<span>{{ Support::date_convert($row->datum) }}</span>
						 </div>
						
					</div>	
				</div>
			</div>
		@endforeach
	</div>

	<div class="col-md-9 col-sm-9 col-xs-12">
		<div class="row">
			@foreach(All::getNewsCategorized($strana) as $row)

			<div class="col-md-4 col-sm-4 col-xs-12 no-padding" id="single-new-right">

				<div class="news relative">
				 	
				 	<a class="overlink" href="{{ Options::base_url() }}blog/{{ Url_mod::blog_kategorija_link($row->web_vest_b2c_id) }}/{{ Url_mod::blog_link($row->web_vest_b2c_id) }}"></a>


		            @if(in_array(Support::fileExtension(Url_mod::blog_slike($row->web_vest_b2c_id)),array('jpg','png','jpeg','gif')))
		              
		            	<div class="bg-img" href="{{ Options::base_url() }}{{ Url_mod::slug_trans('blog') }}/{{ Url_mod::blog_link($row->web_vest_b2c_id) }}" style="background-image: url('{{ $row->slika }}');"></div>
		            
		            @else
		            
		            	<iframe width="560" height="315" src="{{ $row->slika }}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>                   
		            
		            @endif

				   
					<div class="news-text">

						<h2 class="news-title overflow-hdn text-bold">{{ Url_mod::blog_naslov($row->web_vest_b2c_id) }} <br> {{ Url_mod::blog_kategorija($row->web_vest_b2c_id) }}</h2>
						<!-- <div><i class="far fa-clock"></i> {{Url_mod::blog_datum($row->web_vest_b2c_id)}}</div> -->

						<div class="news-desc overflow-hdn">{{ All::shortNewDesc(Url_mod::blog_sadrzaj($row->web_vest_b2c_id)) }}</div>

					</div>
				</div>
			</div>

			<!-- 	<div>
					{{ All::getNews()->links() }}
				</div>   -->

			@endforeach
		</div>
	</div>

</div> 

@endsection