@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')

<!-- CART_CONTENT.blade -->

@if(Session::has('b2c_korpa') and Cart::broj_cart() >= 1 )

@if(Session::has('failure-message'))
<h4>{{ Session::get('failure-message') }}</h4>
@endif

<nav class="checkout-header relative">
	<div class="checkout-container flex justify-between items-center relative">
		<div class="mx-2">
			<a class="logo v-align inline-block sm-index" href="/" title="{{Options::company_name()}}" rel="nofollow">
				<img src="{{ Options::domain() }}{{Options::company_logo()}}" alt="{{Options::company_name()}}" class="img-responsive"/>
			</a>
		</div>
		<div class="mx-2">
			<a href="{{ Options::base_url() }}{{ Seo::get_korpa() }}" aria-labe="cart link">
				<img 
					src="{{ Options::domain() }}images/checkout-cart.svg" 
					alt="checkout cart"
					width="25"
					height="25"
				/>
			</a>
		</div>
	</div>
</nav>

<div class="checkout-wrapper container no-padding">
	<div class="row sm-col-reverse">
		<div class="col-md-6 col-sm-12 col-xs-12 checkout-left-side">
			<br><br>
			<!-- CART ACTION BUTTONS -->
			<div id="cart_form_scroll">
				@if(!Session::has('b2c_kupac'))
					<!-- @if(Options::neregistrovani_korisnici()==1)
					<button class="button" id="JSRegToggle">{{ Language::trans('Kupi bez registracije') }}</button>
					@endif -->
					<a class="inline-block text-left" href="#" role="button" data-toggle="modal" data-target="#loginModal" rel="nofollow">{{ Language::trans('Nemate nalog? Registrujte se') }}</a>
				@endif
			</div>
			<br>
			<!-- SUBMIT ORDER AREA -->

			<!-- BUY WITHOUT REGISTRATION FORM -->
			<!-- <div class="without-registration" id="JSRegToggleSec" {{ Session::has('b2c_kupac') ? "" : (count(Input::old()) == 0 ? "hidden='hidden'" : "") }}>  -->
			<div class="without-registration" id="JSRegToggleSec"> 
				<form method="POST" action="{{ Options::base_url() }}order-create" id="JSOrderForm" class="without-reg-form">
					<div class="row">
						<div class="flag-customer">
						@if(Options::neregistrovani_korisnici()==1 AND !Session::has('b2c_kupac'))
						@if(Input::old('flag_vrsta_kupca') == 1)
						<div class="col-md-6 col-sm-6 col-xs-6 text-center">
							<div class="JScheck_user_type without-btn personal" data-vrsta="personal">
								{{ Language::trans('Fizičko Lice') }} <i class="fas fa-check"></i> 
							</div>
						</div>

						<div class="col-md-6 col-sm-6 col-xs-6 text-center">
							<div class="JScheck_user_type without-btn active none-personal" data-vrsta="non-personal">
								{{ Language::trans('Pravno Lice') }} <i class="fas fa-check"></i>
							</div>
						</div>
						@else
						<div class="col-md-6 col-sm-6 col-xs-6 text-center">
							<div class="JScheck_user_type without-btn active personal" data-vrsta="personal">
								{{ Language::trans('Fizičko Lice') }} <i class="fas fa-check"></i>
							</div>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-6 text-center">
							<div class="JScheck_user_type without-btn none-personal" data-vrsta="non-personal">
								{{ Language::trans('Pravno Lice') }} <i class="fas fa-check"></i>
							</div>	
						</div>
						@endif
						
						@endif
						<input type="hidden" name="flag_vrsta_kupca" value="{{ Input::old('flag_vrsta_kupca') == 1 ? '1' : '0' }}">
						@if(Options::neregistrovani_korisnici()==1 AND !Session::has('b2c_kupac'))
					

						<div class="col-md-6 col-sm-6 col-xs-12 JSwithout-reg-none-personal {{ Input::old('flag_vrsta_kupca') == 1 ? 'active' : '' }}">
							<label for="without-reg-company"><span class="red-dot-error">*</span> {{ Language::trans('Naziv Firme') }}:</label>
							<input id="without-reg-company" name="naziv" type="text" tabindex="1" value="{{ htmlentities(Input::old('naziv') ? Input::old('naziv') : '') }}" placeholder="Naziv Firme">
							<div class="error red-dot-error">{{ $errors->first('naziv') ? $errors->first('naziv') : "" }}</div>
						</div>

						<div class="col-md-6 col-sm-6 col-xs-12 JSwithout-reg-none-personal {{ Input::old('flag_vrsta_kupca') == 1 ? 'active' : '' }}">
							<label for="without-reg-pib">{{ Language::trans('PIB') }}:</label>
							<input id="without-reg-pib" name="pib" type="text" tabindex="2" value="{{ htmlentities(Input::old('pib') ? Input::old('pib') : '') }}" placeholder="PIB">
							<div class="error red-dot-error">{{ $errors->first('pib') ? $errors->first('pib') : "" }}</div>
						</div>

						<div class="col-md-6 col-sm-6 col-xs-12 JSwithout-reg-none-personal {{ Input::old('flag_vrsta_kupca') == 1 ? 'active' : '' }}">
							<label for="without-reg-maticni_br">{{ Language::trans('Matični broj') }}:</label>
							<input id="without-reg-maticni_br" name="maticni_br" type="text" tabindex="2" value="{{ htmlentities(Input::old('maticni_br') ? Input::old('maticni_br') : '') }}" placeholder="Matični broj">
							<div class="error red-dot-error">{{ $errors->first('maticni_br') ? $errors->first('maticni_br') : "" }}</div>
						</div>

						<div class="col-md-6 col-sm-6 col-xs-12 JSwithout-reg-personal {{ Input::old('flag_vrsta_kupca') == 1 ? '' : 'active' }}">
							<label for="without-reg-name"><span class="red-dot-error">*</span> {{ Language::trans('Ime') }}</label>
							<input id="without-reg-name" name="ime" type="text" tabindex="1" value="{{ htmlentities(Input::old('ime') ? Input::old('ime') : '') }}" placeholder="Ime">
							<div class="error red-dot-error">{{ $errors->first('ime') ? $errors->first('ime') : "" }}</div>
						</div>

						<div class="col-md-6 col-sm-6 col-xs-12 JSwithout-reg-personal {{ Input::old('flag_vrsta_kupca') == 1 ? '' : 'active' }}">
							<label for="without-reg-surname"><span class="red-dot-error">*</span> {{ Language::trans('Prezime') }}</label>
							<input id="without-reg-surname" name="prezime" type="text" tabindex="2" value="{{ htmlentities(Input::old('prezime') ? Input::old('prezime') : '') }}" placeholder="Prezime">
							<div class="error red-dot-error">{{ $errors->first('prezime') ? $errors->first('prezime') : "" }}</div>
						</div>

						<div class="col-md-6 col-sm-6 col-xs-12"> 
							<label for="without-reg-phone"><span class="red-dot-error">*</span> {{ Language::trans('Telefon') }}</label>
							<input id="without-reg-phone" name="telefon" type="text" tabindex="3" value="{{ htmlentities(Input::old('telefon') ? Input::old('telefon') : '') }}" placeholder="Telefon">
							<div class="error red-dot-error">{{ $errors->first('telefon') ? $errors->first('telefon') : "" }}</div>
						</div>

						<div class="col-md-6 col-sm-6 col-xs-12"> 
							<label for="without-reg-e-mail"><span class="red-dot-error">*</span> {{ Language::trans('E-mail') }}</label>
							<input id="JSwithout-reg-email" name="email" type="text" tabindex="4" value="{{ htmlentities(Input::old('email') ? Input::old('email') : '') }}" placeholder="Email">
							<div class="error red-dot-error">{{ $errors->first('email') ? $errors->first('email') : "" }}</div>
						</div>

						<div class="col-md-6 col-sm-6 col-xs-12"> 
							<label for="without-reg-address"><span class="red-dot-error">*</span> {{ Language::trans('Adresa za dostavu') }}</label>
							<input id="without-reg-address" name="adresa" type="text" tabindex="5" value="{{ htmlentities(Input::old('adresa') ? Input::old('adresa') : '') }}" placeholder="Adresa za dostavu">
							<div class="error red-dot-error">{{ $errors->first('adresa') ? $errors->first('adresa') : "" }}</div>
						</div>

						<div class="col-md-6 col-sm-6 col-xs-12"> 
							<label for="without-reg-city"><span class="red-dot-error">*</span> {{ Language::trans('Mesto') }}/{{ Language::trans('Grad') }}</label>				 
							<input type="text" name="mesto" tabindex="6" value="{{ htmlentities(Input::old('mesto') ? Input::old('mesto') : '') }}" placeholder="Mesto / Grad">
							<div class="error red-dot-error">{{ $errors->first('mesto') ? $errors->first('mesto') : "" }}</div>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12"> 
							<label for="without-reg-address"><span class="red-dot-error">*</span> {{ Language::trans('Opština') }}</label>
							<input id="without-reg-address" name="opstina" type="text" tabindex="5" value="{{ htmlentities(Input::old('opstina') ? Input::old('opstina') : '') }}" placeholder="Opština">
							<div class="error red-dot-error">{{ $errors->first('opstina') ? $errors->first('opstina') : "" }}</div>
						</div>

						<div class="col-md-6 col-sm-6 col-xs-12"> 
							<label for="without-reg-address"><span class="red-dot-error">*</span> {{ Language::trans('Poštanski broj') }}</label>
							<input id="without-reg-address" name="postanski_broj" type="text" tabindex="5" value="{{ htmlentities(Input::old('postanski_broj') ? Input::old('postanski_broj') : '') }}" placeholder="Poštanski broj">
							<div class="error red-dot-error">{{ $errors->first('postanski_broj') ? $errors->first('postanski_broj') : "" }}</div>
						</div>
						@endif
						</div>
						<!-- End of flag customer -->

						@if(Options::neregistrovani_korisnici()==1 OR Session::has('b2c_kupac'))
						@if(Session::has('b2c_kupac') AND Options::web_options(314)==1)
						<div class="col-md-6 col-sm-6 col-xs-12">
							<label>{{ Language::trans('Bodovi') }}</label>
							<div>{{ Language::trans('Ovom kupovinom broj bodova koji možete ostvariti je') }} <span id="JSAchievePoints">{{ Cart::bodoviOstvareniBodoviKorpa() }}</span>.</div>
							<div>{{ Language::trans('Broj bodova kiji imate je') }} {{ WebKupac::bodovi() }}.
								@if(!is_null($trajanjeBodova = WebKupac::trajanjeBodova()))
								{{ Language::trans('Rok važenja bodova je') }} {{ $trajanjeBodova }}.
								@endif

							</div>
							<div>{{ Language::trans('Maksimalni broj bodova kiji možete iskoristiti u ovoj kupovini je') }} <span id="JSMaxUsingPoints">{{ Cart::bodoviPopustBodoviKorpa() }}</span>.</div>
						</div>

						<div class="col-md-6 col-sm-6 col-xs-12">
							<label>{{ Language::trans('Unesite bodove koje želite da iskoristite') }}:</label>
							<input type="text" name="bodovi" tabindex="6" value="{{ htmlentities(Input::old('bodovi') ? Input::old('bodovi') : '') }}" placeholder="Bodovi">
							<div class="error red-dot-error">{{ Session::has('bodovi_error') ? Session::get('bodovi_error') : '' }}</div>
						</div>
						@endif

						@if(Session::has('b2c_kupac') AND Options::web_options(318)==1)
						<div class="col-md-6 col-sm-6 col-xs-12">
							<label>{{ Language::trans('Vaučeri') }}</label>
							<div>{{ Language::trans('Maksimalni popust preko vaučera kiji možete iskoristiti u ovoj kupovini je') }} <span id="JSMaxUsingVoucherPrice">{{ Cart::cena(Cart::vauceriPopustCenaKorpa()) }}</span></div>
						</div>

						<div class="col-md-6 col-sm-6 col-xs-12">
							<label>{{ Language::trans('Unesite kod sa vašeg vaučera') }}:</label>
							<input type="text" name="vaucer_code" tabindex="6" value="{{ htmlentities(Input::old('vaucer_code') ? Input::old('vaucer_code') : '') }}">
							<div class="error red-dot-error">{{ Session::has('vaucer_error') ? Session::get('vaucer_error') : '' }}</div>
						</div>	
						@endif

						<div class="col-md-6 col-sm-6 col-xs-12">
							<label>{{ Language::trans('Način isporuke') }}:</label>
							<select name="web_nacin_isporuke_id" class="JSdeliveryInput" tabindex="7" >
								{{Order::nacin_isporuke(Input::old('web_nacin_isporuke_id'))}}
							</select>
						</div>

						<div class="col-md-6 col-sm-6 col-xs-12">
							<label>{{ Language::trans('Način plaćanja') }}:</label>
							<select name="web_nacin_placanja_id" tabindex="8">
								{{Order::nacin_placanja(Input::old('web_nacin_placanja_id'))}}
							</select>
						</div>

						<div class="col-md-12 col-sm-12 col-xs-12"> 
							<label>{{ Language::trans('Napomena') }}:</label>
							@if(Cart::kamata(Cart::korpa_id()) >0 )					
							<textarea rows="5" tabindex="9" name="napomena" placeholder="Napomena">{{ Language::trans('Kupovina na:') }} {{ Cart::kamata(Cart::korpa_id()) }} {{ Language::trans('rata') }}</textarea>
							@else
							<textarea rows="5" tabindex="9" name="napomena" placeholder="Napomena">{{ htmlentities(Input::old('napomena') ? Input::old('napomena') : '') }}</textarea>
							@endif
						</div>

						@endif
					</div>

					<div class="required-fields"><span>*</span> {{ Language::trans('Obavezna polja') }}</div>
					
					@if(Options::neregistrovani_korisnici()==1 OR Session::has('b2c_kupac'))
					<div class="text-center"> 
						<button id="JSOrderSubmit" class="button">{{ Language::trans('Završi kupovinu') }}</button>
					</div>
					@endif
				</form>	
			</div>
			<!-- End of Registration Form -->
		</div>

		<!-- Right Side -->
		<div class="col-md-6 col-sm-12 col-xs-12 checkout-right-side relative">
			<div class="checkout-slide hidden-lg">
				<div class="checkout-slide-header bg-gray p-2 pointer">
					<?php $troskovi = Cart::troskovi(); ?>
					<div class="flex justify-between">
						<p class="no-margin">Prikaži rezime porudžbine <i class="fas fa-chevron-down"></i></p>
						<p class="text-bold no-margin">{{Cart::cena(Cart::cart_ukupno()+$troskovi)}}</p>
					</div>
				</div>
			</div>

			<div class="checkout-content">
				@foreach(DB::table('web_b2c_korpa_stavka')->where('web_b2c_korpa_id',Cart::korpa_id())->orderBy('web_b2c_korpa_stavka_id','asc')->get() as $row) 
				<ul class="checkout-item row">	

					<div class="checkout-item-main">
						<li class="relative">
							<span class="checkout-total-items">{{ round($row->kolicina)  }}</span>
							@if(!empty(Product::design_id($row->roba_id)) AND Options::pitchprint_aktiv() == 1)  
								@if(!empty($row->project_id))
								<img src="https://s3-eu-west-1.amazonaws.com/pitchprint.io/previews/{{$row->project_id}}_1.jpg" alt="{{ Product::short_title($row->roba_id) }}" class="cart-image img-responsive" />
								@else
								<img src="https://s3-eu-west-1.amazonaws.com/pitchprint.io/previews/{{Product::design_id($row->roba_id)}}_1.jpg" alt="{{ Product::short_title($row->roba_id) }}" class="cart-image img-responsive" />
								@endif
							@else
							<img src="{{ Options::domain() }}{{Product::web_slika($row->roba_id) }}" alt="{{ Product::short_title($row->roba_id) }}" class="cart-image img-responsive" />
							@endif
						</li>
		
						<li>
							<span> 
								<a href="{{ Options::base_url() }}{{Url_mod::slug_trans('artikal')}}/{{ Url_mod::slugify(Product::seo_title($row->roba_id)) }}" class="checkout-item-name">
									{{ Product::short_title($row->roba_id) }}
								</a>
								{{Product::getOsobineStr($row->roba_id,$row->osobina_vrednost_ids)}}   
							</span>
						</li>

						<input type="hidden" name="projectid" value="{{ $row->project_id }}" >
						<li><span>{{ Cart::cena($row->jm_cena) }}</span></li>
					</div>		 
				</ul>
				@endforeach


			<div class="row">  
				<br class="hidden-md hidden-lg">

				<div class="cart-summ">
					<div class="JSdelivery_total_amountParent" {{(Cart::troskovi()>0) ? '' : 'hidden'}}>
						<span class="sum-label">{{ Language::trans('Cena artikala') }}: </span>
						<span class="JSdelivery_total_amount sum-amount">{{Cart::cena(Cart::cart_ukupno())}}</span>
					</div>

					<div class="JSDelivery" {{(Cart::troskovi()>0) ? '' : 'hidden'}}>
						<span class="sum-label">{{ Language::trans('Troškovi isporuke') }}: </span>
						<span class="JSexpenses sum-amount" >{{ Cart::cena($troskovi) }}</span>
					</div>

					<div class="checkout-total flex justify-between items-center">
						<span class="sum-label">{{ Language::trans('Ukupno') }}: </span>
						<span class="JStotal_amount JStotal_amount_weight sum-amount ">{{Cart::cena(Cart::cart_ukupno()+$troskovi)}}</span>
					</div>

				</div> 
			</div>
		 </div>
		</div>
		<!-- End of right side -->

	</div>
	<div class="shop-by p-3 border-t-gray text-center">
		<small>{{ Options::company_name() }} &copy; {{ date('Y') }}. {{Language::trans('Sva prava zadržana')}}. - 
			<a href="https://www.selltico.com/" style="color: #111">{{Language::trans('Izrada internet prodavnice')}}</a> - 
			<a href="https://www.selltico.com/"  style="color: #111"> Selltico. </a>
		</small>
	</div>
</div>



@else

<br>

<h2><span class="section-title">{{ Language::trans('Vaša Korpa') }}</span></h2> 

<div class="no-articles">{{ Language::trans('Trenutno nemate artikle u korpi') }}.</div>

@endif 

<!-- CART_CONTENT.blade END -->

@endsection