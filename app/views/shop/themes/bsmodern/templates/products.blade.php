<!DOCTYPE html>
<html lang="sr">
<head>
	<link href="{{Options::domain()}}css/slick.css" rel="stylesheet" type="text/css" />
	<?php $page = Input::get('page', 1); ?>
	@if($page > 1)
		<meta name="googlebot" content="noindex" />
	@endif

	@include('shop/themes/'.Support::theme_path().'partials/head')

	<script type="application/ld+json">
		{
			"@context": "http://schema.org",
			"@type": "WebPage",
			"name": "<?php echo $title; ?>",
			"description": "<?php echo $description; ?>",
			"url" : "<?php echo Options::base_url().$url; ?>",
			"publisher": {
			"@type": "Organization",
			"name": "<?php echo Options::company_name(); ?>"
		}
	}
</script>
</head>
<body id="product-page" 
@if($strana == All::get_page_start()) id="start-page"  @endif 
@if(Support::getBGimg() != null) 
style="background-image: url({{Options::domain()}}{{Support::getBGimg()}}); background-size: cover; background-repeat: no-repeat; background-attachment: fixed; background-position: center;" 
@endif
>


@include('shop/themes/'.Support::theme_path().'partials/menu_top')
 
@include('shop/themes/'.Support::theme_path().'partials/header')

<!-- PRODUCTS.blade -->
<main class="d-content JSmain relative"> 
	<div class="stickyOverlay"></div>
	<div class="mini-cart-overlay"></div>

	<!-- Breadcrumbs -->
	<!-- <div class="container">
		@if($strana!='akcija' and $strana!='tip')
			<ul class="breadcrumb">
				@if($strana!='pretraga' and $strana!='filteri' and $strana!='proizvodjac')
				{{ Url_mod::breadcrumbs2()}}
				@elseif($strana == 'proizvodjac')
				<li>
					<a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('brendovi') }}">{{ Language::trans('Brendovi') }}</a>  
					@if($grupa)
					<a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('proizvodjac') }}/{{ Url_mod::slug_trans($proizvodjac) }}">{{ All::get_manofacture_name($proizvodjac_id) }}</a>
					{{ Groups::get_grupa_title($grupa) }}
					@else
					{{ All::get_manofacture_name($proizvodjac_id) }} 
					@endif
				</li>
				@else					
				<li><a href="{{ Options::base_url() }}">{{ All::get_title_page_start() }}</a></li>
				<li>{{$title}}</li>
				@endif
			</ul>
		@endif  
	</div> -->

	<!-- Category Banner -->
	<div class="container-fluid category-banner-main no-padding">
		@if((!empty($baner_id) AND $banner = Slider::slajder($baner_id) AND count($bannerStavke = Slider::slajderStavke($banner->slajder_id)) > 0) AND !empty($bannerStavka = $bannerStavke[0]))
			<div class="banners row">
				<div class="col-md-12 col-sm-12 col-xs-12 no-padding text-center">
					<a href="{{ $bannerStavka->link }}" class="center-block" aria-label="category banner">
						<img src="{{ Options::domain() }}{{ $bannerStavka->image_path }}" alt="{{ $bannerStavka->alt }}" class="img-responsive margin-auto" loading="lazy" decoding="async" />
						<div class="banner-navigation text-white">
							<span><a href="/" aria-label="početna">Početna</a></span> / 
							<span>{{ Groups::getGrupa($grupa_pr_id) }}</span>
						</div>
					</a>

					@if($bannerStavka->naslov != '')
						<div class="sliderText short-desc JSInlineFull content text-white" data-target='{"action":"slide_title","id":"{{$bannerStavka->slajder_stavka_id}}"}'>
							{{ $bannerStavka->naslov }}
						</div>
					@endif

					
				</div>
			</div>
		@endif
	</div>
	
	<div class="container"> 
		<div class="row">  
 			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 product-page">

				@if($strana!='pretraga' and $strana!='filteri' and $strana!='proizvodjac' and $strana != 'tagovi' and $strana != 'tip' and $strana != 'akcija')

					<!-- FOR SEO -->
					<h1 class="seo"> <span class="group-title">  {{ Groups::getGrupa($grupa_pr_id) }} </span> </h1>

					@if(isset($sub_cats) and !empty($sub_cats))
					<div class="row sub-group JSsubs_slick">
						@foreach($sub_cats as $row)
							<div class="col-lg-2-sub col-md-3 col-sm-3 col-xs-8 no-padding text-center category-box">
								<a href="{{ Options::base_url()}}c/{{ $url }}/{{ Url_mod::slug_trans($row->grupa) }}">
									
									@if(isset($row->putanja_slika))
										<!-- <img src="https://umino-demo-v2.myshopify.com/cdn/shop/files/cate2.jpg?v=1686216539" alt="test image" class="img-responsive" loading="lazy" decoding="async"> -->
										<img src="{{ Options::domain() }}{{ $row->putanja_slika }}" alt="{{ $row->grupa }}" class=" img-responsive" loading="lazy" decoding="async" />
									@endif
									
									<span class="bg-white radius-50">{{ $row->grupa }}</span>
								</a>
							</div>
						@endforeach

					</div>
					@endif
				@else
					<!-- FOR SEO -->
					<h1 class="seo"> 
						<span class="group-title">  
							@if($strana == 'pretraga')
								{{ Language::trans('Rezultati pretraživanja za pojam') }}: 
							@elseif($strana == 'proizvodjac')
								{{ Language::trans('Proizvodjač') }}: 
							@endif
							<strong> {{ Groups::getSearchResults() }} </strong> 
						</span>
					</h1>
				@endif

				@yield('products_list')

			</div>

		</div>
	</div>
</main>
<!-- PRODUCTS.blade END -->


@include('shop/themes/'.Support::theme_path().'partials/footer')
 
@include('shop/themes/'.Support::theme_path().'partials/popups')


<!-- BASE REFACTORING -->
<input type="hidden" id="base_url" value="{{Options::base_url()}}" />
<input type="hidden" id="in_stock" value="{{Options::vodjenje_lagera()}}" />
<input type="hidden" id="elasticsearch" value="{{ Options::gnrl_options(3055) }}" />

<!-- js includes -->
@if(Session::has('b2c_admin'.Options::server()) OR (Options::enable_filters()==1 AND $filter_prikazi))
<script src="{{Options::domain()}}js/slick.min.js"></script>
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
@endif
@if(Session::has('b2c_admin'.Options::server()))
<script src="{{Options::domain()}}js/tinymce_5.1.3/tinymce.min.js"></script> 
<script src="{{ Options::domain()}}js/jquery-ui.min.js"></script>
<script src="{{Options::domain()}}js/shop/front_admin/main.js"></script>
<script src="{{Options::domain()}}js/shop/front_admin/products.js"></script>
<script src="{{Options::domain()}}js/shop/front_admin/product.js"></script>
@endif

<script src="{{Options::domain()}}js/shop/translator.js"></script>
<script src="{{Options::domain()}}js/shop/themes/{{Support::theme_path()}}main.js"></script>
<script src="{{Options::domain()}}js/shop/themes/{{Support::theme_path()}}main_function.js"></script>
<script src="{{Options::domain()}}js/shop/themes/{{Support::theme_path()}}cart.js"></script>

@if(Options::header_type()==1)
<script src="{{Options::domain()}}js/shop/themes/{{Support::theme_path()}}fixed_header.js"></script>
@endif

	<script type="text/javascript">
	    window.omnisend = window.omnisend || [];

	    omnisend.push(["accountID", "636b2e64e67810becfd7645e"]);

	    omnisend.push(["track", "$pageViewed"]);

	    !function(){var e=document.createElement("script");e.type="text/javascript",e.async=!0,e.src="https://omnisnippet1.com/inshop/launcher-v2.js";var t=document.getElementsByTagName("script")[0];t.parentNode.insertBefore(e,t)}();
	</script>
</body>
</html>