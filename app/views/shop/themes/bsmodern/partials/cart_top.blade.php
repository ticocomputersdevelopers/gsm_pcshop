<div class="header-cart-container order-1">  
	<button class="header-cart text-center relative flex items-center gap-2">
		<img 
			src="{{ Options::base_url() }}images/cart.svg"
			alt="cart icon" 
			width="22"
        	height="22"
		/>
		<div>
			<p class="text-left opacity-5">{{ Language::trans('Korpa') }}</p>
			<p class="header-cart-total">rsd. {{ Cart::cena(Cart::cart_ukupno()) }}</p>
		</div>
		<span class="JScart_num badge"> {{ Cart::broj_cart() }} </span> 		
		<input type="hidden" id="h_br_c" value="{{ Cart::broj_cart() }}" />	
	</button>


	<div class="JSheader-cart-content">
		@include('shop/themes/'.Support::theme_path().'partials/mini_cart_list') 
	</div>
</div>
