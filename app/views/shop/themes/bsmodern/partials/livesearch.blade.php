<div class="JSsearch_list my-3">
    <div class="search-darken"></div>
	<div class="first-search-result my-3">
		<h4 class="text-center"> {{Language::trans('Rezultat pretrage')}}: <span class="text-black">"{{ $search }}"</span> </h4>
        <div class="first-search-content text-center">
    		@if(count($groups) > 0)
    		<ul>
    			@foreach(array_slice($groups,0,50) as $group)
    			<li>
    				{{ $search }} u 
    				<span>
    				    <a href="{{ Options::base_url() }}{{ $group['_source']['slug'] }}">{{ $group['_source']['name'] }}</a>
    				</span>
    			</li>
    			@endforeach
    		</ul>
    		@endif
        </div>
	</div>

    <div class="row second-search-result">
         <div class="col-md-12 col-sm-12 col-xs-12 no-padding">
            <h4 class="text-center"> {{Language::trans('Preporučeni proizvodi')}} </h4>
            <div class="second-search-content">
                @if(count($products) > 0)
                <ul>
                	@foreach($products as $product)
                    <li class="row flex">
                        <div class="col-xs-12 p-left"> 
                            <a class="search-list-photo" href="#!">
                                <img class="margin-auto center-block img-responsive" src="{{ $product['_source']['image'] }} " alt="{{ $product['_source']['name'] }}" />
                            </a>
                            <a class="search-list-name inline-block my-2" href="{{ Options::base_url() .Url_mod::slug_trans('artikal') }}/{{ $product['_source']['slug'] }}">
                                {{ $product['_source']['name'] }} 
                            </a> 
                        </div>

                        <div class="col-xs-12 no-padding">
                            <div class="row">
                                <div class="col-xs-12 no-padding">
                                    <div class="review my-1">
                                       {{ $product['_source']['rating'] }}
                                    </div>
                                </div>
                                <div class="col-xs-12 no-padding text-left text-bold">
                                    {{ $product['_source']['price'] }}
                                </div>
                            </div>
                        </div>
                    </li>
                    @endforeach
                </ul>
                @endif
            </div>
        </div>

    </div>

</div>