<div class="row flex items-start">    
    <div class="col-md-6 col-sm-6 col-xs-12 no-padding">
        @if(All::provera_akcija($roba_id))
            <div class="sale-label text-white inline-block"> 
                <div class="for-sale-price">- <sub>{{ Product::getSale($roba_id) }} %</sub></div> 
            </div>
		@endif
        
        <div class="main-slider-img JSgallery_slick">
            <div class="JSleft_btn pointer text-center"><i class="fas fa-chevron-left"></i></div>

            <img 
                class="JSmain_quickimg img-responsive mx-auto" 
                src="" 
                alt="modal main"
                width="200"
                height="250"
                decoding="async"
                loading="lazy"
            />

            <div class="JSright_btn pointer text-center"><i class="fas fa-chevron-right"></i></div>
        </div>

        <a href="{{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($roba_id))}}" class="JS-quick-no-image hidden">
            <img src="{{Options::base_url()}}images/no-image.jpg" class="img-responsive margin-auto">
        </a>

        <div class="modal-gallery-img clearfix text-center hidden">
            {{ Product::get_quick_list_images($roba_id) }}
        </div>
     
    </div>

    <div class="col-md-6 col-sm-6 col-xs-12 product-preview-info">

        <div class="my-2 text-uppercase">{{ Product::get_grupa($roba_id) }}</div>

        <a href="{{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($roba_id))}}" class="article-heading-modal">
            {{ Product::seo_title($roba_id) }}
        </a>
        <!-- <h1 class="article-heading-modal">{{ Product::seo_title($roba_id) }}</h1> -->

         <!-- PRICE -->
         <div class="product-preview-price flex items-center gap-1">
            @if(Product::getStatusArticlePrice($roba_id) == 1)
                @if(All::provera_akcija($roba_id))    

                <div class="inline-block"> 
                    <span class="price-label hidden">{{ Language::trans('Akcijska cena')}}:</span> 
                    <span class="JSaction_price price-num main-price" data-action_price="{{Product::get_price($roba_id)}}">
                        {{ Cart::cena(Product::get_price($roba_id,true,false,(!is_null(Input::old('kamata')) ? Input::old('kamata') : 0 ))) }}
                    </span>
                </div>

                @if(Product::get_mpcena($roba_id) != 0) 
                <div class="inline-block">
                    <span class="price-label hidden">{{ Language::trans('Maloprodajna cijena') }}:</span>
                    <s class="price-num mp-price product-old-price">{{ Cart::cena(Product::get_mpcena($roba_id)) }}</s> 
                </div>
                @endif  


                @if(Product::getPopust_akc($roba_id)>0)
                <div class="hidden"> 
                    <span class="price-label hidden">{{ Language::trans('Popust') }}: </span>
                    <span class="price-num discount">{{ Cart::cena(Product::getPopust_akc($roba_id)) }}</span>
                </div>
                @endif

                @else

                @if(Product::get_mpcena($roba_id) != 0) 
                <div class="inline-block hidden"> 
                    <span class="price-label hidden">{{ Language::trans('Maloprodajna cijena') }}:</span>
                    <span class="price-num mp-price">{{ Cart::cena(Product::get_mpcena($roba_id)) }}</span> 
                </div>
                @endif

                <div class="inline-block"> 
                    <span class="price-label hidden">{{ Language::trans('Cijena sa uračunatim popustom za gotovinsko plaćanje') }}:</span>
                    <span class="JSweb_price price-num main-price" data-cena="{{Product::get_price($roba_id)}}">
                       {{ Cart::cena(Product::get_price($roba_id,true,false,(!is_null(Input::old('kamata')) ? Input::old('kamata') : 0 ))) }}
                   </span>
               </div>

               @if(Product::getPopust($roba_id)>0)
                   @if(AdminOptions::web_options(132)==1)
                   <div class="hidden">  
                    <span class="price-label hidden">{{ Language::trans('Popust') }}:</span>
                    <span class="price-num discount">{{ Cart::cena(Product::getPopust($roba_id)) }}</span>
                    </div>
                    @endif
                @endif

            @endif
        @endif 
        </div>

        <!-- Characteristics -->
        @if(Product::check_osobine($roba_id))  
            @foreach(Product::osobine_nazivi($roba_id) as $osobina_naziv_id)

            <p class="char-title">{{ Product::find_osobina_naziv($osobina_naziv_id,'naziv') }}:</p>

            <div class="attributes-modal attributes text-bold">
                @foreach(Product::osobine_vrednosti($roba_id,$osobina_naziv_id) as $osobina_vrednost_id)
                <label class="relative no-margin" style="background-color: {{ Product::find_osobina_vrednost($osobina_vrednost_id, 'boja_css') }}">
                    <span class="tooltip-char">{{ Product::find_osobina_vrednost($osobina_vrednost_id, 'vrednost') }}</span>
                    <input type="radio" name="osobine{{ $osobina_naziv_id }}" value="{{ $osobina_vrednost_id }}" {{ Product::check_osobina_vrednost($roba_id,$osobina_naziv_id,$osobina_vrednost_id,Input::old('osobine'.$osobina_naziv_id)) }}>

                    <span class="inline-block">{{ Product::find_osobina_vrednost($osobina_vrednost_id, 'vrednost') }}</span>
                    
                </label>
                @endforeach

            </div>

            @endforeach 
        @endif
        
        <div class="add-to-cart-area clearfix flex items-center">    
            @if(Product::getStatusArticle($roba_id) == 1)
                 @if(Cart::check_avaliable($roba_id) > 0)
                 <form method="POST" action="{{ Options::base_url() }}product-cart-add" id="JSAddCartForm" class="flex"> 
                  
                <!-- PITCHPRINT -->
                @if(!empty(Product::design_id($roba_id) AND !empty(Options::pitchprint() AND Options::pitchprint_aktiv() == 1)))
                    @include('shop/themes/'.Support::theme_path().'partials/pitchprint')
                @endif
                <!-- PITCHPRINT END -->

                @if(AdminOptions::web_options(313)==1) 
                <div class="num-rates"> 
                    <div> 
                        <div class="inline-block lorem-1">{{ Language::trans('Broj rata') }}</div>
                    </div>
                    <select class="JSinterest" name="kamata">
                        {{ Product::broj_rata(Input::old('kamata')) }}
                    </select>
                </div>
                @endif

                <input type="hidden" name="roba_id" value="{{ $roba_id }}">
                <span class="hidden">&nbsp;{{Language::trans('Količina')}}&nbsp;</span>
            @if(Options::web_options(320) == 1 AND (Product::jedinica_mere($roba_id)->jedinica_mere_id) == 3 AND Product::pakovanje($roba_id) ) 
                <input type="number" name="kolicina" class="cart-amount weight" min="0" step="0.1" value="{{ Input::old('kolicina') ? Input::old('kolicina') : '1' }}"><span>&nbsp;{{Language::trans(' kg')}}&nbsp;</span>
            @else
            
                <div class="add-to-cart-modal">
                    <button onClick="var result = document.getElementById('productAmount'); var productAmount = result.value; if( !isNaN( productAmount ) &amp;&amp; productAmount &gt; 0 ) result.value--;return false;" class="product-count" type="button"><i class="fa fa-minus"></i></button>
                    
                    <input id="productAmount" type="text" name="kolicina" class="cart-amount amount-modal-input" value="{{ Input::old('kolicina') ? Input::old('kolicina') : '1' }}">
                    
                    <button onClick="var result = document.getElementById('productAmount'); var productAmount = result.value; if( !isNaN( productAmount )) result.value++;return false;" class="product-count" type="button"><i class="fa fa-plus"></i></button>
                </div>
            @endif

                <button type="submit" id="JSAddCartSubmit" class="add-to-cart button relative cart-modal text-bold"><span class="sprite sprite-cart"></span>{{Language::trans('Dodaj u Korpu')}}</button>
                <input type="hidden" name="projectId" value=""> 

                <div class="red-dot-error">{{ $errors->first('kolicina') ? $errors->first('kolicina') : '' }}</div>  

                @if(Cart::kupac_id() > 0)
                <button class="like-it JSadd-to-wish cart-wish" type="button" data-roba_id="{{$roba_id}}" title="{{ Language::trans('Dodaj na listu želja') }}">
                    <img 
                        src="{{ Options::base_url() }}images/wish.svg" 
                        alt="wish list"
                        width="16"
                        height="16"
                    />
					<span class="tooltiptext"> {{ Language::trans('Dodaj u listu želja') }} </span>
                </button>
                @else
                <button class="like-it JSnot_logged cart-wish" type="button" data-roba_id="{{$roba_id}}" title="{{ Language::trans('Dodavanje artikla na listu želja moguće je samo registrovanim korisnicima') }}">
                    <img 
                        src="{{ Options::base_url() }}images/wish.svg" 
                        alt="wish list"
                        width="16"
                        height="16"
                    />
					<span class="tooltiptext"> {{ Language::trans('Dodaj u listu želja') }} </span>
                </button> 
                @endif
                
            </form>

            <button class="JScompare like-it {{ All::check_compare($roba_id) ? 'active' : '' }}" data-id="{{$roba_id}}" title="{{ Language::trans('Uporedi') }}">
                <img 
                    src="{{ Options::base_url() }}images/compare.svg" 
                    alt="compare"
                    width="16"
                    height="16"
                />
                <span class="tooltiptext"> {{ Language::trans('Uporedi') }} </span>
            </button>

            @else

            <button class="not-available button">{{Language::trans('Nije dostupno')}}</button>       
            @endif

            @else
            <button class="button soon-on-stock" data-roba-id="{{$roba_id}}">
                {{ Product::find_flag_cene(Product::getStatusArticle($roba_id),'naziv') }}
            </button>
            @endif
        </div>

    </div>
</div>

<script src="{{Options::domain()}}js/shop/themes/{{Support::theme_path()}}gallery.js" type="text/javascript"></script>