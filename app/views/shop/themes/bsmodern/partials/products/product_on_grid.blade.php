<!-- PRODUCT ON GRID -->
<div class="product-card col-md-3 col-sm-4 col-xs-12 no-padding"> 
	<div class="shop-product-card relative"> 
 
		@if(All::provera_akcija($row->roba_id))
		<div class="sale-label text-white"> 
			<div class="for-sale-price">- <sub>{{ Product::getSale($row->roba_id) }} %</sub></div> 
		</div>
		@endif

		<div class="product-image-wrapper flex relative">
			<aside class="product-options-side">
				<!-- Wish -->
				<div class="relative">
					@if(Cart::kupac_id() > 0)
					<button class="JSadd-to-wish" data-roba_id="{{$row->roba_id}}" title="{{ Language::trans('Dodaj na listu želja') }}">
						<img 
							src="{{ Options::base_url() }}images/wish.svg" 
							alt="wish list"
						/>
						<span class="tooltiptext"> {{ Language::trans('Dodaj u listu želja') }} </span>
					</button> 
					@else
					<button class="JSnot_logged" data-roba_id="{{$row->roba_id}}" title="{{ Language::trans('Dodavanje artikla na listu želja moguće je samo registrovanim korisnicima') }}">
						<img 
							src="{{ Options::base_url() }}images/wish.svg" 
							alt="wish list"
						/>
						<span class="tooltiptext"> {{ Language::trans('Dodaj u listu želja') }} </span>
					</button> 
					@endif	
				</div>
				

				<!-- Compare -->
				<div class="relative">
					<button class="JScompare {{ All::check_compare($row->roba_id) ? 'active' : '' }}" data-id="{{$row->roba_id}}" title="{{ Language::trans('Uporedi') }}">
						<img 
							src="{{ Options::base_url() }}images/compare.svg" 
							alt="compare"
						/>
						<span class="tooltiptext"> {{ Language::trans('Uporedi') }} </span>
					</button>
				</div>

				<!-- Quick View -->
				<div class="relative">
					<button class="product-preview-modal quick-view-btn details JSQuickViewButton icon-quick-preview" data-roba_id="{{$row->roba_id}}">
						<img 
						src="{{ Options::base_url() }}images/quick-view.svg" 
						alt="quick view"
						/>
						<span class="tooltiptext"> {{ Language::trans('Brzi pregled') }} </span>
					</button>
				</div>
				
				<!-- Add to Cart -->
				<div class="relative">
					@if(Product::getStatusArticle($row->roba_id) == 1)	
					@if(Cart::check_avaliable($row->roba_id) > 0)

						@if(!Product::check_osobine($row->roba_id)) 

						<button class="JSadd-to-cart" data-roba_id="{{$row->roba_id}}">
							<img 
								src="{{ Options::base_url() }}images/cart.svg" 
								alt="add to cart"
							/>
							<span class="tooltiptext"> {{ Language::trans('Dodaj u korpu') }} </span>
						</button>

						@else

						<button class="JSadd-to-cart" data-roba_id="{{$row->roba_id}}">
							<img 
								src="{{ Options::base_url() }}images/cart.svg" 
								alt="add to cart"
							/>
							<span class="tooltiptext"> {{ Language::trans('Dodaj u korpu') }} </span>
						</button>	 
						
						@endif

						@else  	
						<!-- <button class="not-available button"><span class="inline-block cart-me"></span> {{ Language::trans('Nije dostupno') }}</button>	 -->
						<button>
							<img 
								src="{{ Options::base_url() }}images/cart.svg" 
								alt="add to cart"
							/>
							<span class="tooltiptext"> {{ Language::trans('Nije dostupno') }} </span>
						</button>

					@endif
					@endif
				</div>
			</aside>

			@if(!empty(Product::design_id($row->roba_id)) AND Options::pitchprint_aktiv() == 1)
			<a class="margin-auto" href="{{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($row->roba_id))}}">
				<img class="product-image img-responsive JSlazy_load" src="{{Options::domain()}}images/quick_view_loader.gif" data-src="https://s3-eu-west-1.amazonaws.com/pitchprint.io/previews/{{Product::design_id($row->roba_id)}}_1.jpg?k=0.4491390433183766" alt="{{ Product::seo_title($row->roba_id) }}" />
			</a>
			@else
			<a class="margin-auto" href="{{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($row->roba_id))}}">
				@if(Product::get_file_extension(Product::web_slika($row->roba_id)) == 'webm')
                    <video autoplay muted loop class="product-image img-responsive" src="{{ Options::domain() }}{{ Product::web_slika($row->roba_id) }}" alt="{{ Product::seo_title($row->roba_id) }}"></video>
                @else 
					<img 
						class="product-image img-responsive JSlazy_load firstImgProduct" 
						src="{{Options::domain()}}images/quick_view_loader.gif" 
						data-src="{{ Options::domain() }}{{ Product::web_slika($row->roba_id) }}" 
						alt="{{ Product::seo_title($row->roba_id) }}" 
						decoding="async"
					/>
					
					<!-- Second Image -->
					<img 
						class="product-image img-responsive JSlazy_load secondImgHover" 
						src="{{Options::domain()}}images/quick_view_loader.gif" 
						data-src="{{ Options::domain() }}{{ Product::web_slika_second($row->roba_id) }}" 
						alt="{{ Product::seo_title($row->roba_id) }}" 
						decoding="async"
					/>
				@endif
			</a>
			@endif

			<!-- <a href="{{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($row->roba_id))}}" class="article-details hidden-xs hidden-sm"><i class="fas fa-search-plus"></i> {{ Language::trans('Detaljnije') }}</a> -->

			@if(All::provera_akcija($row->roba_id)) 
				@if(All::provera_datuma_i_tajmera($row->roba_id))
				<input type="hidden" class="JSrok_akcije" value="{{ All::action_deadline($row->roba_id) }}">
				<span class="product-timer">
                    {{ Language::trans('Akcije traje od') }}: {{ All::action_start($row->roba_id) }} {{ Language::trans('do') }} {{ All::action_deadline($row->roba_id) }} {{ Language::trans('ili do isteka zaliha') }}
                </span>
				<!-- <table class="table product-timer">
					<tbody>
						<tr>
							<td class="JSdays"></td>
							<td class="JShours"></td>
							<td class="JSminutes"></td>
							<td class="JSseconds"></td>
						</tr>
					</tbody> 
				</table> -->
				@endif 
			@endif
			
			<div class="product-sticker flex">
			    @if(Product::stiker_levo($row->roba_id) != null )
		            <span class="article-sticker-img">
		                <img class="img-responsive" src="{{ Options::domain() }}{{product::stiker_levo($row->roba_id) }}" alt="sticker left" />
		            </span>
	            @endif 
	            
	            @if(Product::stiker_desno($row->roba_id) != null )
		            <span class="article-sticker-img clearfix">
		                <img class="img-responsive pull-right" src="{{ Options::domain() }}{{product::stiker_desno($row->roba_id) }}" alt="sticker right" />
		            </span>
	            @endif 
        	</div>
		</div>

		<div class="product-meta"> 
			
			<span class="text-uppercase">{{ Product::get_grupa($row->roba_id) }}</span>
			@if(!empty(Product::sifra_is($row->roba_id)))
			<h2 class="product-name">
				@else
				<h2 class="product-name">
					@endif
				<a href="{{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($row->roba_id))}}">
					{{ Product::short_title($row->roba_id) }}  
				</a>
			</h2>

			<span class="review">{{ Product::getRating($row->roba_id) }}</span> 

			@if(Product::getStatusArticlePrice($row->roba_id))
			<div class="price-holder">
				@if(All::provera_akcija($row->roba_id))
				<s class="product-old-price">
				<i class="relative">{{ Cart::cena(Product::old_price($row->roba_id)) }}</i></s>
				<!-- <span class="color_price"><b>Ušteda: {{Product::getSale($row->roba_id)}}%</b> &nbsp; </span> -->
				@endif     
				<div class="product-price {{ All::provera_akcija($row->roba_id) ? 'grid-action-price' : '' }}"> {{ Cart::cena(Product::get_price($row->roba_id)) }} </div>
			</div>	
			@endif 

			@if(AdminOptions::web_options(153)==1)
			<div class="generic_car hidden">
				{{ Product::get_web_roba_karakteristike_short($row->roba_id) }}
			</div>
			@endif

			<!-- <div class="short_desc_grid">{{ Product::get_kratak_opis($row->roba_id) }}</div> -->


			<div class="add-to-cart-container text-center">  
				@if(Product::getStatusArticle($row->roba_id) == 1)	
				@if(Cart::check_avaliable($row->roba_id) > 0)

					@if(!Product::check_osobine($row->roba_id)) 

					<button class="buy-btn button JSadd-to-cart" data-roba_id="{{$row->roba_id}}">
						<img 
							src="{{ Options::base_url() }}images/cart.svg" 
							alt="add to cart"
							width="20"
							height="20"
						/>
						<!-- <span class="inline-block cart-me"></span> {{ Language::trans('Dodaj u korpu') }} -->
					</button>

					@else

					<button class="buy-btn button JSadd-to-cart" data-roba_id="{{$row->roba_id}}">
						<img 
							src="{{ Options::base_url() }}images/cart.svg" 
							alt="add to cart"
							width="20"
							height="20"
						/>
					</button>	 
					
					@endif

					@else  	
					<!-- <button class="not-available button"><span class="inline-block cart-me"></span> {{ Language::trans('Nije dostupno') }}</button>	 -->
					<button class="buy-btn button">
						<img 
							src="{{ Options::base_url() }}images/cart.svg" 
							alt="add to cart"
							width="20"
							height="20"
							style="filter: contrast(0.2)"
						/>
					</button>

				@endif

				@else  
					<!-- <button class="not-available button"><span class="inline-block cart-me"></span>  {{ Language::trans('Nije dostupno') }}</button>	 -->

					@if(Product::find_flag_cene(Product::getStatusArticle($row->roba_id),'naziv') == 'Na upit')
					<button class="button JSenquiry" data-roba-id="{{$row->roba_id}}">
						<!-- {{Product::find_flag_cene(Product::getStatusArticle($row->roba_id),'naziv')}} -->
						<img 
							src="{{ Options::base_url() }}images/cart.svg" 
							alt="add to cart"
							width="20"
							height="20"
							style="filter: contrast(0.2)"
						/>
					</button> 
						@elseif(Product::find_flag_cene(Product::getStatusArticle($row->roba_id), 'naziv') == 'Proveri dostupnost')
							<a href="tel:064/825-00-00" class="call-button inline-block button checkAvailability" style="margin: 5px;"><span class="inline-block fas fa-phone"></span>  {{ Language::trans('Proveri dostupnost') }}</a>
						@else
					
						<!-- <button class="buy-btn button soon-on-stock-btn">{{ Product::find_flag_cene(Product::getStatusArticle($row->roba_id),'naziv') }}</button>	 -->
						<button class="buy-btn button">
							<img 
								src="{{ Options::base_url() }}images/cart.svg" 
								alt="add to cart"
								width="20"
								height="20"
								style="filter: contrast(0.2)"
							/>
						</button>
						
						<!-- <a href="tel:064/825-00-00" class="call-button inline-block button"><span class="inline-block fas fa-phone"></span>  {{ Language::trans('Pozovite') }}</a> -->
					@endif

					

					<!-- <button class="buy-btn button">{{ Product::find_flag_cene(Product::getStatusArticle($row->roba_id),'naziv') }}</button>	 -->
					<!-- 
					@if(Options::compare()==1 AND isset($filter_prikazi) AND $filter_prikazi == 1)
					<button class="like-it JScompare {{ All::check_compare($row->roba_id) ? 'active' : '' }}" data-id="{{$row->roba_id}}" title="{{ Language::trans('Uporedi') }}">
						<img 
							src="{{ Options::base_url() }}images/compare.svg" 
							alt="compare"
						/>
					</button>
					@endif	  -->

				@endif 	
			</div>             
		</div>

		<!-- ADMIN BUTTON -->
		@if(Session::has('b2c_admin'.Options::server()) AND Admin_model::check_admin(array('ARTIKLI_AZURIRANJE')))
		<a class="article-edit-btn JSFAProductModalCall" data-roba_id="{{$row->roba_id}}" href="javascript:void(0)" rel="nofollow">{{ Language::trans('IZMENI ARTIKAL') }}</a>
		@endif
	</div>
</div>



