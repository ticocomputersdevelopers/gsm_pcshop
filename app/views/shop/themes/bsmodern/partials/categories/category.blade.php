<div class="col-md-3-lg col-sm-12 col-xs-12 sm-no-padd padding-lg-l-0">

	<div class="JScategories relative"> 
		<?php $query_category_first=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>0,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc')->get(); ?>
		
		<h4 class="categories-title text-center hidden-sm hidden-xs">
			<img 
				src="{{ Options::base_url() }}images/burger.svg" 
				alt="burger icon"
				width="22"
				height="22"
			/>
			{{ Language::trans('Proizvodi') }} 
			<span class="JSclose-nav hidden-md hidden-lg text-white">&times;</span>
		</h4> 

		<!-- CATEGORIES LEVEL 1 -->
		<div class="cat-nav flex justify-between p-1 hidden-lg hidden-md">
			<ul class="nav nav-tabs tab-titles text-uppercase text-bold hidden-lg hidden-md category-tabs text-center">
				<li class="{{ !Session::has('contactError') ? 'active' : '' }}"><a data-toggle="tab" href="#menu-pages-tab" rel="nofollow">{{Language::trans('Meni')}}</a></li>
				<li class="{{ Session::has('contactError') ? 'active' : '' }}"><a data-toggle="tab" href="#category-tab" rel="nofollow">{{Language::trans('Kategorije')}}</a></li>               
			</ul>
			<button class="close-cat-nav">
				<img 
					src="{{ Options::domain() }}images/cursor-close.png" 
					alt="close"
					width="22"
					height="22"
				/>
			</button>
		</div>

		

		<!-- Pages -->
		<div id="menu-pages-tab" class="tab-pane sm-fade{{ !Session::has('contactError') ? ' in active' : '' }} hidden-lg hidden-md">
			<div class="menu-pages-mobile">
				<ul class="wish-login-pages">
					<li></li>
				</ul>
			</div> 
		</div>
		<!-- End of Pages -->

		<!-- Category -->
		<div id="category-tab" class="tab-pane sm-fade{{ Session::has('contactError') ? ' in active' : '' }}">
		<ul class="JSlevel-1">
			@if(Options::category_type()==1) 
			@foreach ($query_category_first as $row1)
			@if(Groups::broj_cerki($row1->grupa_pr_id) >0)

			<li data-firstlevel="{{ $row1->grupa }}">
				<a href="{{ Options::base_url()}}c/{{ Url_mod::slug_trans($row1->grupa) }}" aria-label="category">
					@if(Groups::check_image_grupa($row1->putanja_slika))
					<span class="lvl-1-img-cont inline-block text-center hidden-sm hidden-xs"> 
						<img src="{{ Options::domain() }}{{$row1->putanja_slika}}" alt="{{ $row1->grupa }}" />
					</span>
					@endif
					{{ Language::trans($row1->grupa)  }} 
				</a>

				<?php $query_category_second=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row1->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc') ?>

				<ul class="JSlevel-2 row">
					<span class="cat-title-mobile hidden-lg hidden-md" id="cat-title-mobile-page">
						<i class="fas fa-chevron-left"></i> 
						<span></span>
					</span>
					<div class="col-md-6 col-sm-12 col-xs-12 no-padding">
					@foreach ($query_category_second->get() as $row2)
					<li class="col-md-6 col-sm-12 col-xs-12">  
 
						<a href="{{ Options::base_url()}}c/{{ Url_mod::slug_trans($row1->grupa) }}/{{ Url_mod::slug_trans($row2->grupa) }}">
							@if(Groups::check_image_grupa($row2->putanja_slika))
							<span class="lvl-2-img-cont inline-block hidden-sm hidden-xs hidden">
								<img src="{{ Options::domain() }}{{$row2->putanja_slika}}" alt="{{ $row2->grupa }}" />
							</span>
							@endif
							 
							{{ Language::trans($row2->grupa) }}
							 
						</a>
		   
						@if(Groups::broj_cerki($row2->grupa_pr_id) >0)
						<?php $query_category_third=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row2->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc')->get(); ?>
						
						<ul class="JSlevel-3">
							@foreach($query_category_third as $row3)
							
							<li>						 
								<a href="{{ Options::base_url()}}c/{{ Url_mod::slug_trans($row1->grupa) }}/{{ Url_mod::slug_trans($row2->grupa) }}/{{ Url_mod::slug_trans($row3->grupa) }}">{{ Language::trans($row3->grupa) }}</a>

								<!-- @if(Groups::broj_cerki($row3->grupa_pr_id) >0)
								<span class="JSCategoryLinkExpend hidden-sm hidden-xs">
									<i class="fas fa-chevron-down" aria-hidden="true"></i>
								</span>
								@endif   -->

								@if(Groups::broj_cerki($row3->grupa_pr_id) >0)
								<?php $query_category_forth=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row3->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc')->get(); ?>

								<ul class="JSlevel-4">
									@foreach($query_category_forth as $row4)
									<li>
										<a href="{{ Options::base_url()}}c/{{ Url_mod::slug_trans($row1->grupa) }}/{{ Url_mod::slug_trans($row2->grupa) }}/{{ Url_mod::slug_trans($row3->grupa) }}/{{ Url_mod::slug_trans($row4->grupa) }}">{{ Language::trans($row4->grupa) }}</a>
									</li>
									@endforeach
								</ul>
								@endif		
							</li>					 	
							@endforeach
						</ul>
						@endif
					</li>
					@endforeach
					</div>

					<!-- Category Banner -->
					<?php $categoryImage = DB::table('slajder_stavka')->where('slajder_id', $row1->baner_id)->orderBy('rbr', 'desc')->first(); ?>
					@if(!is_null(DB::table('grupa_pr')->where('grupa_pr_id',$row1->grupa_pr_id)->pluck('baner_id')) && $categoryImage)
						<div class="col-md-5 hidden-sm hidden-xs banner-category-wrapper">
							<div class="banner-category">
								<div class="banners row padding-v-20">
									<div class="col-md-12 col-sm-12 col-xs-12 no-padding">
										<img src="{{ Options::base_url() }}{{ $categoryImage->image_path }}" alt="category banner" decoding="async" loading="lazy" />
									</div>
								</div>
							</div>
						</div>
					@endif
						
				</ul>
			</li>

			@else

			<li>		 
				<a href="{{ Options::base_url()}}c/{{ Url_mod::slug_trans($row1->grupa) }}">		 
					@if(Groups::check_image_grupa($row1->putanja_slika))
					<span class="lvl-1-img-cont inline-block text-center hidden-sm hidden-xs"> 
						<img src="{{ Options::domain() }}{{$row1->putanja_slika}}" alt="{{ $row1->grupa }}" />
					</span>
					@endif
					{{ Language::trans($row1->grupa)  }}  				
				</a>			 
			</li>
			@endif
			@endforeach
			@else
			@foreach ($query_category_first as $row1)
			 

			@if(Groups::broj_cerki($row1->grupa_pr_id) >0)
			<a href="{{ Options::base_url()}}c/{{ Url_mod::slug_trans($row1->grupa) }}">{{ Language::trans($row1->grupa)  }}</a>
			<?php $query_category_second=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row1->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc') ?>

 
			@foreach ($query_category_second->get() as $row2)
			<li>
				<a href="{{ Options::base_url()}}c/{{ Url_mod::slug_trans($row1->grupa) }}/{{ Url_mod::slug_trans($row2->grupa) }}">
					{{ Language::trans($row2->grupa) }}
				</a>
				@if(Groups::broj_cerki($row2->grupa_pr_id) >0)
				<?php $query_category_third=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row2->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc')->get(); ?>
				<ul>
					@foreach($query_category_third as $row3)
					<a href="{{ Options::base_url()}}c/{{ Url_mod::slug_trans($row1->grupa) }}/{{ Url_mod::slug_trans($row2->grupa) }}/{{ Url_mod::slug_trans($row3->grupa) }}">{{ Language::trans($row3->grupa) }}
					</a>
					@if(Groups::broj_cerki($row3->grupa_pr_id) >0)
					<?php $query_category_forth=DB::table('grupa_pr')->where(array('parrent_grupa_pr_id'=>$row3->grupa_pr_id,'web_b2c_prikazi'=>1))->orderBy('redni_broj','asc')->get(); ?>

					<ul>
						@foreach($query_category_forth as $row4)
						<a href="{{ Options::base_url()}}c/{{ Url_mod::slug_trans($row1->grupa) }}/{{ Url_mod::slug_trans($row2->grupa) }}/{{ Url_mod::slug_trans($row3->grupa) }}/{{ Url_mod::slug_trans($row4->grupa) }}">{{ Language::trans($row4->grupa) }}</a>
						@endforeach
					</ul>
					@endif	
					@endforeach
				</ul>
				@endif
			</li>
			@endforeach

			@else
				<li>
					<a href="{{ Options::base_url()}}c/{{ Url_mod::slug_trans($row1->grupa) }}">{{ Language::trans($row1->grupa) }}</a>
				</li>
			@endif 
			@endforeach				
			@endif

			@if(Options::all_category()==1)
			<li>
				<a href="{{ Options::base_url() }}{{ Url_mod::slug_trans('sve-kategorije') }}">{{ Language::trans('Sve kategorije') }}</a>
			</li>
			@endif

		</ul>

		</div>
		<!-- End of Category -->
	</div>
</div>
