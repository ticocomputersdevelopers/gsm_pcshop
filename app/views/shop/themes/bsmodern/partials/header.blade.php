<!-- HEADER.blade -->
<header class="{{ $strana === 'checkout' ? 'hidden' : '' }}">   
    <div id="JSfixed_header" {{ Options::web_options(321, 'str_data') != '' ? 'style=background-color:' . Options::web_options(321, 'str_data') : '' }}>  
        <div class="container{{(Options::web_options(321)==1) ? '-fluid' : '' }}"> 
            <div class="row flex"> 

                <div class="col-lg-3 col-md-2 col-sm-3 col-xs-6 padding-lg-l-0 sm-text-center"> 
                    <!-- <h1 class="seo">{{ Options::company_name() }}</h1> -->
                    <a class="logo v-align inline-block sm-index" href="/" title="{{Options::company_name()}}" rel="nofollow">
                        <img src="{{ Options::domain() }}{{Options::company_logo()}}" alt="{{Options::company_name()}}" class="img-responsive"/>
                    </a>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 header-search padding-lg-x-0">  
                    <div class="row"> 
                    
                        @if(Options::gnrl_options(3055) == 0)
        
                            <div class="col-md-3 col-sm-3 col-xs-3 no-padding JSselectTxt hidden">  
                                {{ Groups::firstGropusSelect('2') }} 
                            </div>
                    
                        @else  
                    
                            <input type="hidden" class="JSSearchGroup2" value="">
                    
                        @endif 

                         
                        <div class="col-md-12 col-sm-12 col-xs-12 no-padding JSsearchContent2">  
                        <!-- <div class="{{ Options::gnrl_options(3055) == 0 ? 'col-xs-9' : 'col-xs-12' }} no-padding JSsearchContent2">   -->
                            <div class="relative sm-index"> 
                                <form autocomplete="off">
                                    <div class="flex items-center relative search-content-wrapper">
                                        <img 
                                            src="{{ Options::base_url() }}images/search.svg" 
                                            alt="search icon"
                                            width="18"
                                            height="18"
                                        />
                                        <input type="text" id="JSsearch2" placeholder="{{ Language::trans('Pretraga') }}" />
                                    </div>
                                </form>      
                                <button onclick="search2()" class="JSsearch-button2 fs-base"> <span>{{ Language::trans('Pretraži') }}</span> </button>
                                
                                <!-- Popup Search -->
                                <div class="popup-search w-full bg-white">
                                    <div class="container no-padding">
                                        <div class="popup-search-header text-center">
                                            Brza pretraga: <div class="quick-search-categories text-bold"></div>
                                        </div>
                                        <div class="text-left px-2 my-2">
                                            <h4><b>Najpopularniji artikli</b></h4>
                                        </div>
                                        <div class="popup-search-content">
                                            <div class="popup-search-articles">
                                                <?php $search_articles = DB::table('roba')->orderBy('pregledan_puta', 'DESC')->limit(6)->get(); ?> 
                                                @foreach($search_articles as $row)
                                                    @include('shop/themes/'.Support::theme_path().'partials/products/product_on_grid')
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                    </div> 
                </div>

                <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12 header_right_icon padding-lg-x-0">
                    <div class="md-space-between md-space-center sprite-icons {{ Session::has('b2c_kupac') ? 'logged-in-center' : '' }}"> 
                        @if(Session::has('b2c_kupac'))
                            <div class="relative hidden-sm hidden-xs has_customer">
                                <a href="{{Options::base_url()}}{{Url_mod::slug_trans('korisnik')}}/{{Url_mod::slug_trans(WebKupac::get_user_or_company_name())}}">
                                    <img 
                                        src="{{ Options::base_url() }}images/wish.svg" 
                                        alt="wish icon"
                                        width="22"
                                        height="22"
                                    />
                                    <span class="hidden-lg hidden-md">{{ Language::trans('Lista želja') }}</span>
                                </a>
                                <span class="JSbroj_wish text-white badge hidden-sm hidden-xs" title="{{ Language::trans('Omiljeni artikli') }}"><span>{{ Cart::broj_wish() }}</span></span> 
                            </div>
                        @else  
                            <a href="#" id="login-icon" data-toggle="modal" data-target="#loginModal" class="hidden-sm hidden-xs">
                                 <img 
                                    src="{{ Options::base_url() }}images/wish.svg" 
                                    alt="wish icon"
                                    width="22"
                                    height="22"
                                />
                                <span class="hidden-lg hidden-md">{{ Language::trans('Lista želja') }}</span>
                            </a>
                         @endif
                    
                    <!-- CART AREA -->  
                    @include('shop/themes/'.Support::theme_path().'partials/cart_top')

                        @if(Session::has('b2c_kupac'))
                            <div class="login_b2c flex items-center gap-1 order-minus-1">
                                <img 
                                    src="{{ Options::base_url() }}images/login.svg"
                                    alt="" 
                                    width="22"
                                    height="22"
                                />
                                <!-- <div class="login_icon inline-block valign"> </div> -->
    
                                <ul class="inline-block"> 
                                    <li> 
                                    <a id="logged-user" href="{{Options::base_url()}}{{Url_mod::slug_trans('korisnik')}}/{{Url_mod::slug_trans(WebKupac::get_user_name())}}">
                                         <img 
                                            src="{{ Options::base_url() }}images/login.svg"
                                            alt="" 
                                            width="22"
                                            height="22"
                                            class="user-img-logged hidden-lg hidden-md hidden-sm"
                                        />
                                        <span>{{ WebKupac::get_user_name() }}</span>
                                    </a>
                                    </li>
                                    <!-- <li class="JSremove_company_name {{ Session::has('b2c_kupac') ? '' : 'hidden' }}"> 
                                    <a id="logged-user" href="{{Options::base_url()}}{{Url_mod::slug_trans('korisnik')}}/{{Url_mod::slug_trans(WebKupac::get_company_name())}}">
                                        <span>{{ WebKupac::get_company_name() }}</span>
                                    </a>
                                    </li> -->
                                    <li> 
                                    <a id="logout-button" href="{{Options::base_url()}}logout"><span>{{ Language::trans('Odjavi se') }}</span></a> 
                                    </li>
                                </ul>
                            </div>

                        @else  

                            <div class="inline-block order-minus-1 not_logged_in"> 
                              
                            <!-- <div class="login_icon inline-block valign"> </div> -->

                            <ul class="inline-block no_customer hidden-sm hidden-xs"> 
                                <li>
                                    <a href="#" id="login-icon" class="flex items-center gap-1" data-toggle="modal" data-target="#loginModal"> 
                                        <img 
                                            src="{{ Options::base_url() }}images/login.svg"
                                            alt="login icon" 
                                            width="22"
                                            height="22"
                                        />
                                        <div class="hidden-sm hidden-xs">
                                            <p class="opacity-5">{{ Language::trans('Prijava') }}</p>
                                            <p>Nalog</p>
                                        </div>

                                        <span class="hidden-lg hidden-md">Prijava / Registracija</span>
                                    </a>
                                </li>
                               
                                <!-- <li><a id="registration-icon" href="{{Options::base_url()}}{{ Url_mod::slug_trans('registracija') }}">
                                    {{ Language::trans('Registracija') }} </a>
                                </li> -->
                            </ul> 
                            </div>
                        @endif
                    </div>


                <!-- RESPONSIVE BUTTON -->
                <div class="hidden-lg hidden-md col-sm-2 col-xs-3 text-center resp-burger-menu sm-absolute xs-absolute sm-index">
                    <div class="resp-nav-btn pointer">
                        <img 
                            src="{{ Options::base_url() }}images/burger.svg"
                            alt="burger icon" 
                            width="22"
                            height="22"
                        />
                    </div>
                </div>

            </div> 
        </div>  
    </div> 
</header>

<div id="JSsticky_menu" class="menu-background {{ $strana === 'checkout' ? 'hidden' : '' }}">   
    <div class="container"> 
		<div class="responsive-overlay"></div>
        <div id="responsive-nav" class="row">

            @if(Options::category_view()==1)
                @include('shop/themes/'.Support::theme_path().'partials/categories/category')
            @endif               


            <div class="col-md-9-lg col-sm-12 col-xs-12 no-padding">

                <ul class="main-menu text-white" id="menu-mobile">
                    @foreach(All::header_menu_pages() as $row)
                    <li class="header-dropdown">
                        @if(All::broj_cerki($row->web_b2c_seo_id) > 0)  
                        <a href="{{ Options::base_url().Url_mod::page_slug($row->naziv_stranice)->slug }}" data-naziv="{{ Url_mod::page_slug($row->naziv_stranice)->naziv }}">
                            {{Url_mod::page_slug($row->naziv_stranice)->naziv}}
                            @if(All::broj_cerki($row->web_b2c_seo_id) > 0)
                            <span class="hidden-sm hidden-xs">
                                <i style="font-size: 12px; margin-left: 3px; opacity: .5;" class="fas fa-chevron-down" aria-hidden="true"></i>
                            </span>
                            @endif
                        </a>
                        <ul class="drop-2">
            				<span class="cat-title-mobile hidden-lg hidden-md">
                                <i class="fas fa-chevron-left"></i> 
                                <span></span>
                            </span>
                            @foreach(All::header_menu_pages($row->web_b2c_seo_id) as $row2)
                            <li> 
                                <a href="{{ Options::base_url().Url_mod::page_slug($row2->naziv_stranice)->slug }}">{{Url_mod::page_slug($row2->naziv_stranice)->naziv}}</a>
                                <ul class="drop-3">
                                    @foreach(All::header_menu_pages($row2->web_b2c_seo_id) as $row3)
                                    <li> 
                                        <a href="{{ Options::base_url().Url_mod::page_slug($row3->naziv_stranice)->slug }}">{{Url_mod::page_slug($row3->naziv_stranice)->naziv}}</a>
                                    </li>
                                    @endforeach
                                </ul>
                            </li>
                            @endforeach
                        </ul>
                        @else   
                        <a href="{{ Options::base_url().Url_mod::page_slug($row->naziv_stranice)->slug }}">{{Url_mod::page_slug($row->naziv_stranice)->naziv}}</a> 
                        @endif                    
                    </li>                     
                    @endforeach 

                    @if(Options::web_options(121)==1)
                    <?php $konfiguratori = All::getKonfiguratos(); ?>
                    @if(count($konfiguratori) > 0)
                    @if(count($konfiguratori) > 1)
                    <li>
                        
                        <a href="#!" rel="nofollow">{{Language::trans('Konfiguratori')}}</a>

                        <ul class="drop-2">
                            @foreach($konfiguratori as $row)
                            <li>
                                <a href="{{ Options::base_url() }}{{Url_mod::slug_trans('konfigurator')}}/{{ $row->konfigurator_id }}">{{ Language::trans($row->naziv) }}</a>
                            </li>
                            @endforeach
                        </ul>
                    </li>
                    @else
                    <li>
                        <a href="{{ Options::base_url() }}{{Url_mod::slug_trans('konfigurator')}}/{{ $konfiguratori[0]->konfigurator_id }}">
                            {{Language::trans('Konfigurator')}}
                        </a>
                    </li>
                    @endif
                    @endif
                    @endif

                    <li class="lg-pull-right recently-viewed-link">
                        <a href="{{ Options::base_url() }}nedavno-pregledani">
                            <img 
                                src="{{ Options::base_url() }}images/clock.svg" 
                                alt="recently viewed articles" 
                                width="18" 
                                height="18" 
                                class="mr-half"
                            />
                            Nedavno pregledani artikli
                        </a>
                    </li>
                </ul>   
            </div> 
        </div>
    </div>    
</div> 


<div class="modal fade" id="loginModal" role="dialog">
    <div class="modal-dialog">

        <div class="modal-content" >
            <div class="modal-header text-center">
                <button type="button" class="w-full text-right" data-dismiss="modal">
                    <img 
                        src="{{ Options::domain() }}images/cursor-close.png" 
                        alt="close mini cart"
                        width="16"
                        height="16"
                        class="pointer"
                        style="filter: invert(1)"
                    />
                </button>
                <p class="modal-title">{{ Language::trans('Prijava') }}</p>
            </div>

            <div class="modal-body"> 
                <label for="JSemail_login" class="hidden">{{ Language::trans('E-mail') }}</label>
                <input id="JSemail_login" type="text" value="" placeholder="Vaša email adresa *" autocomplete="off">
        
                <label for="JSpassword_login" class="hidden">{{ Language::trans('Lozinka') }}</label>
                <input autocomplete="off" id="JSpassword_login" type="password" value="" placeholder="Vaša lozinka *"> 

                <button type="button" onclick="user_forgot_password()" class="forgot-psw">{{ Language::trans('Zaboravljena lozinka') }}?</button>
            </div>

            <div class="modal-footer text-center">
                <a class="button text-center" href="{{Options::base_url()}}{{ Url_mod::slug_trans('registracija') }}">{{Language::trans('Registruj se')}}</a>
                
                <button type="button" onclick="user_login()" class="button">{{ Language::trans('Prijavi se') }}</button>

                <div class="field-group error-login JShidden-msg" id="JSForgotSuccess"><br>
                    {{ Language::trans('Novu lozinku za logovanje dobili ste na navedenoj e-mail adresi') }}.
                </div> 
            </div>
        </div>   
    </div>
</div>
<!-- HEADER.blade END --> 

@if($strana != "kontakt")
<!-- OPEN MODAL ON CLICK PROVERITE STANJE BUTTON --> 
<div class="modal fade" id="checkState" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content" >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">Zatvori <i class="fas fa-times"></i></button>
            </div>
            <div class="modal-body" id="bodyModal"> 
                <h4 class="modal-title hidden">
                    {{ Language::trans('Pozovite naš call centar na broj') }}

                    @if(Options::company_phone())
                        <a href="tel:{{ Options::company_phone() }}">
                            {{ Options::company_phone() }}
                        </a>
                    @endif

                    {{ Language::trans('ili nam prosledite upit') }}: 

                </h4>

                <form method="POST" action="{{ Options::base_url() }}enquiry-message-send">
                    <input type="hidden" id="JSStateArtID" name="roba_id" value="">
                   
                    <div class="relative my-3">
                        <label id="label_name" class="labelFocus">{{ Language::trans('Vaše ime') }} *</label>
                        <input class="contact-name" name="contact-name" id="JScontact_name"  type="text" value="{{ Input::old('contact-name') }}">
                        <div class="error red-dot-error">{{ $errors->first('contact-name') ? $errors->first('contact-name') : "" }}</div>
                    </div> 

                    <div class="relative my-3">
                        <label id="label_email" class="labelFocus">{{ Language::trans('Vaša e-mail adresa') }} *</label>
                        <input class="contact-email" name="contact-email" id="JScontact_email"  type="text" value="{{ Input::old('contact-email') }}" >
                        <div class="error red-dot-error">{{ $errors->first('contact-email') ? $errors->first('contact-email') : "" }}</div>
                    </div>      
                    <div class="relative my-3">
                        <label id="label_phone" class="labelFocus">{{ Language::trans('Telefon') }} *</label>
                        <input class="contact-phone" name="contact-phone" id="JScontact_phone" type="text" value="{{ Input::old('contact-phone') }}" >
                        <div class="error red-dot-error">{{ $errors->first('contact-phone') ? $errors->first('contact-phone') : "" }}</div>
                    </div>  
                    <div class="relative my-3">   
                        <label id="label_message" class="labelFocus">{{ Language::trans('Vaša poruka') }} </label>
                        <textarea class="contact-message" name="contact-message" rows="5" id="message">{{ Input::old('contact-message') }}</textarea>
                        <div class="error red-dot-error">{{ $errors->first('contact-message') ? $errors->first('contact-message') : "" }}</div>
                    </div> 

                    <div class="text-left"> 
                        <button type="submit" class="button submitBtn">{{ Language::trans('Pošalji') }}</button>
                    </div>

                    <hr style="border-top: 1px solid #ddd">
                </form>

                <div class="createdBy text-center">
                    <p>{{ Options::company_name() }} &copy; {{ date('Y') }}. {{Language::trans('Sva prava zadržana')}}. - 
						<a href="https://www.selltico.com/">{{Language::trans('Izrada internet prodavnice')}}</a> - 
						<a href="https://www.selltico.com/"> Selltico. </a>
					</p>
                </div>
            </div>

        </div>   
    </div>
</div>
@endif

<!-- QUICK VIEW MODAL -->
<div class="modal fade" id="JSQuickView" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
          <div class="modal-header">
            <button type="button" id="JSQuickViewCloseButton" class="pull-right close-me-btn">
              <img 
                 src="{{ Options::base_url() }}images/cursor-close.png" 
                 alt="close modal"
                 width="18"
                 height="18"
              />
            </button>
          </div>
          <div class="modal-body" id="JSQuickViewContent">
            <img alt="loader-image" class="gif-loader" src="{{Options::base_url()}}images/quick_view_loader.gif">
          </div>
      </div>    
    </div>
</div>