<div class="mini-cart-wrapper">
	<div class="mini-cart-header flex items-center justify-between my-2">
		<h4 class="fs-larger">Korpa</h4>
		<img 
			src="{{ Options::base_url() }}images/cursor-close.png" 
			alt="close mini cart"
			width="18"
			height="18"
			class="pointer"
			style="filter: invert(1)"
		/>
	</div>

	<!-- @if(Cart::broj_cart()>0)
	<div class="free-shipping relative">
		<span class="relative"></span>
		<img 
			src="{{ Options::base_url() }}images/shipping.svg" 
			alt="shipping"
			width="32"
			height="32"
		/>	
		<p class="fs-base">Čestitamo! Ostvarili ste besplatnu dostavu!</p>
		<br><br>
	</div>
	@endif -->

@if(Cart::broj_cart()>0)
 
	<div class="miniCartWrapper">
		@foreach(DB::table('web_b2c_korpa_stavka')->where('web_b2c_korpa_id',Cart::korpa_id())->orderBy('web_b2c_korpa_stavka_id','asc')->get() as $row)
		<div class="row mini-cart-item flex"> 

			<div class="col-xs-3"> 
				@if(!empty(Product::design_id($row->roba_id)) AND Options::pitchprint_aktiv() == 1)  
				@if(!empty($row->project_id))			
				<img class="mini-cart-img img-responsive" src="https://s3-eu-west-1.amazonaws.com/pitchprint.io/previews/{{$row->project_id}}_1.jpg" alt="{{Product::short_title($row->roba_id)}}"/>
				@else
				<img class="mini-cart-img img-responsive" src="https://s3-eu-west-1.amazonaws.com/pitchprint.io/previews/{{Product::design_id($row->roba_id)}}_1.jpg" alt="{{Product::short_title($row->roba_id)}}"/>
				@endif
				@else
				<img class="mini-cart-img img-responsive" src="{{ Options::domain() }}{{Product::web_slika($row->roba_id)}}" alt="{{Product::short_title($row->roba_id)}}"/>
				@endif
			</div>

			<div class="col-xs-9 no-padding flex items-baseline" id="mini-cart-info"> 
	
				<div>
					<a class="mini-cart-title line-h fs-base" href="{{Options::base_url()}}{{Url_mod::slug_trans('artikal')}}/{{Url_mod::slugify(Product::seo_title($row->roba_id))}}">{{Product::short_title($row->roba_id)}}</a> 
					@if(AdminOptions::web_options(152)==1)
					<span>{{Cart::cena(ceil(($row->jm_cena)/10)*10 )}}</span>
					@else
					<span class="header-cart-price">{{Cart::cena($row->jm_cena)}}</span>
						@if(All::provera_akcija($row->roba_id))
						<s class="product-old-price" style="font-size: 13px;">
						<i class="relative">{{ Cart::cena(Product::old_price($row->roba_id)) }}</i></s>
						<!-- <span class="color_price"><b>Ušteda: {{Product::getSale($row->roba_id)}}%</b> &nbsp; </span> -->
						@endif 
					@endif

					<div class="add-to-mini-cart flex my-2 radius-5">
						<button 
							class="product-count-minus" 
							type="button" 
							data-roba_id="{{ $row->roba_id }}"
							data-web_b2c_korpa_stavka_id="{{ $row->web_b2c_korpa_stavka_id }}"
							>
							<i class="fa fa-minus"></i>
						</button>
						
						<div> 
							@if(AdminOptions::web_options(320)==1)
								@if(Product::jedinica_mere($row->roba_id)->jedinica_mere_id == 3 AND Product::pakovanje($row->roba_id))
								<span>{{$row->kolicina}}kg</span> x
								@else
								<input type="text" class="round-quantity inline-block" min="1" value="{{ round($row->kolicina) }}" />
								@endif
							@else
								<input type="text" class="round-quantity inline-block" min="1" value="{{ round($row->kolicina) }}" />
							@endif
							
						</div>
						
						<button 
							class="product-count-plus" 
							type="button" 
							data-roba_id="{{ $row->roba_id }}"
							data-web_b2c_korpa_stavka_id="{{ $row->web_b2c_korpa_stavka_id }}"
							>
							<i class="fa fa-plus"></i>
						</button>
					
					</div>
				</div>

				
				<a href="javascript:void(0)" data-stavka_id="{{ $row->web_b2c_korpa_stavka_id }}" title="{{ Language::trans('Ukloni artikal') }}" class="remove-cart-item JSdelete_cart_item fs-15 text-bold" rel=”nofollow”><i class="fa fa-trash" aria-hidden="true"></i></a>

			</div>
		</div>
		@endforeach	
	</div>


	<div class="mini-cart-bottom-wrapper py-1 w-full">
		<div class="mini-cart-bottom-content">
			<ul class="mini-cart-sum"> 
				@if(Options::checkTroskoskovi_isporuke() == 1 AND Cart::cart_ukupno() < Cart::cena_do()) 
		
					<li>{{ Language::trans('Cena artikala') }}: <i>{{Cart::cena(Cart::cart_ukupno())}}</i></li> 
					<li>{{ Language::trans('Troškovi isporuke') }}: <i>{{Cart::cena(Cart::cena_dostave())}}</i></li> 
					<li>{{ Language::trans('Ukupno') }}: <i>{{Cart::cena(Cart::cart_ukupno()+Cart::cena_dostave())}}</i></li> 
		
				@else
					
					 <li>{{ Language::trans('Ukupno') }}: <i>{{ Cart::cena(Cart::cart_ukupno()) }}</i></li> 
		
				@endif  
			</ul>

			<div class="row mini-cart-term fs-15">
				<span class="col-xs-12 JScheck-term">
					<input type="checkbox" name=""> {{ Language::trans('Slažem se sa') }} <a href="/uslovi-isporuke" target="_blank">  {{ Language::trans('uslovima isporuke') }} </a>
				</span>
			</div>
		
			<div class="text-center"> 
				<a class="d-block w-full button radius-50 fs-base my-2 transition-sm" href="{{ Options::base_url() }}{{ Seo::get_korpa() }}" rel="nofollow">{{Language::trans('Završi kupovinu')}}</a>
				<a id="to-checkout" class="d-block w-full button radius-50 fs-base transition-sm" href="{{ Options::base_url() }}checkout" rel="nofollow">{{Language::trans('Plaćanje')}}</a>
			</div>
		</div>
	</div>
@else
	<div class="no-items flex items-center justify-center text-center gap-2 px-1">
		<img 
			src="{{ Options::domain() }}images/no-items.svg" 
			alt="no items"
			width="65"
			height="65"
		/>
		<div class="fs-15">Vaša korpa je prazna.</div>
		<p class="fs-base">Možete pogledati sve dostupne proizvode i kupiti neke u prodavnici.</p>
		<button class="my-2 btn button radius-50 transition-sm" aria-label="close cart">Vratite se u prodavnicu</button>
	</div>
@endif
</div>