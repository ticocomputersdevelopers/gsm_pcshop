

<nav class="manufacturer-categories">
    <ul class="row">
        <li>
            <a  class="" href="{{ Options::base_url()}}{{Url_mod::url_convert('svi-artikli')}}">
                <span class="">{{ Language::trans('Svi artikli') }}</span>
            </a>
        </li>
        @foreach($sub_cats as $row)
            <li>
                <a  class="" href="{{ Options::base_url()}}{{$url=='' ? '' : $url.'/'}}{{ Url_mod::url_convert($row->grupa) }}/0/0/0-0">
                    <span class="">{{ Language::trans($row->grupa) }}</span>
                </a>
            </li>
        @endforeach  
    </ul>
</nav>
