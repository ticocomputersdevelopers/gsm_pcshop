

@if($strana!='akcija' and $strana!='tip')

<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="breadcrumb-div">
			
			<ul>
				@if($strana!='pretraga' and $strana!='filteri' and $strana!='proizvodjac')
					{{ Url_mod::breadcrumbs2()}}
				@elseif($strana == 'proizvodjac')

					<li>
						<a href="{{ Options::base_url() }}{{ Url_mod::convert_url('brendovi') }}">
							Brendovi
						</a>  
					</li>
						@if($grupa)
					<li>
						<a href="{{ Options::base_url() }}{{ Url_mod::convert_url('proizvodjac') }}/{{ Url_mod::url_convert($proizvodjac) }}">
							{{ All::get_manofacture_name($proizvodjac_id) }}
						</a>
					</li>

					<li>
						<a href="">
					    	{{ Groups::get_grupa_title($grupa) }}
					    </a>
					</li>
	
					@else
					<li>
						<a href="">
							{{ All::get_manofacture_name($proizvodjac_id) }} 
					  	</a>
					</li>
					@endif

				@else					
					<li>
						<a href="{{ Options::base_url() }}">
							{{ All::get_title_page_start() }}
						</a>
					</li>

					<li>
						<a href="">
							{{$title}} 
						</a>
					</li>
				@endif
			</ul>
		</div>
	</div>
</div>

@endif  