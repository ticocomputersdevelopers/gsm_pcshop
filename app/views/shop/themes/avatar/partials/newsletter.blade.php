
<?php $newslatter_description = All::newslatter_description(); ?>
@if(Options::newsletter()==1)
	<div class="newsletter">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
				    <h5 class="JSInlineShort" data-target='{"action":"newslatter_label"}'>{{ $newslatter_description->naslov }}</h5>          
				    <div class="newsletter-form">              
				        <input type="text" class="form-control" placeholder="E-mail" id="newsletter" />
				       
				        <button onclick="newsletter()">{{ Language::trans('Prijavi se') }}</button>
				    </div>
				    <h6 class="JSInlineFull" data-target='{"action":"newslatter_content"}'>{{ $newslatter_description->sadrzaj }}</h6>
			    </div>
		    </div>
	    </div>
	</div>
@endif