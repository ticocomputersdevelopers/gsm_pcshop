

@if($filter_prikazi == 0)

	@if($strana == 'proizvodjac')
		@include('shop/themes/'.Support::theme_path().'partials/categories/manufacturer_category')
	@elseif($strana == 'tip')

	@include('shop/themes/'.Support::theme_path().'partials/categories/tip_category')

	@elseif($strana == 'akcija')
		@include('shop/themes/'.Support::theme_path().'partials/categories/akcija_category')
	@endif

@endif

@if(Options::enable_filters()==1 AND $filter_prikazi)
	@include('shop/themes/'.Support::theme_path().'partials/products/filters')
@endif