
@extends('shop/themes/'.Support::theme_path().'templates/main')

@section('page')

<div class="news-category-page">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="page-breadcrumb">
					<ul>
						<li>
							<a href="#!">
								Home /
							</a>
						</li>
						<li>
							<a href="#!">
								Business 
							</a>
						</li>
					</ul>
				</div>

				<h2 class="page-heading">
					Laptop technical details
				</h2>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				@foreach(All::getNews() as $row)
					<div class="single-category-news">
						<a class="image-div" href="{{ Options::base_url() }}{{ Url_mod::convert_url('vesti') }}/{{ $row->web_vest_b2c_id }}">
							<img src="{{ Options::domain() }}{{ $row->slika }}" alt="{{ $row->naslov }}" />
						</a>

						<div class="single-news-content">
							<span class="kind-of">Post</span>	
							
							<a class="title" href="{{ Options::base_url() }}{{ Url_mod::convert_url('vesti') }}/{{ $row->web_vest_b2c_id }}">
								{{ $row->naslov }}
							</a>
							
							<p class="short-description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque fermentum massa vel enim feugiat gravida. Phasellus velit risus, euismod a lacus et.</p>

							<div class="create-news-div">
								<span class="create-news-date">June 06, 2018</span>
								<span style="margin-right: 2px"><a href="#!"> - cmsmasters</a></span>
								In
								<span style="margin-left: 2px"> <a href="#!"> Business</a></span>
							</div>


								<!--	<a href="#!">Business</a>
							 <a href="#!" class="comment"><i class="far fa-comments"></i> 1</a> -->

						<!-- {{ All::shortNewDesc($row->tekst) }} Opis vesti  -->
					
						<!-- <a class="" href="{{ Options::base_url() }}{{ Url_mod::convert_url('vesti') }}/{{ $row->web_vest_b2c_id }}">{{ Language::trans('Pročitaj članak') }}</a>  Procitaj vise - vesti -->
						</div>

					</div>


				@endforeach
			
			</div>
		</div>
	</div>
</div>
@endsection