<html>
	<head>
		<title>{{ $export->naziv }} grupe</title>
		<link href="{{ AdminOptions::base_url()}}css/normalize.css" rel="stylesheet" type="text/css" />
		<link href="{{ AdminOptions::base_url()}}css/foundation.min.css" rel="stylesheet" type="text/css" />
        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/css/select2.min.css" rel="stylesheet" />
        <style>
 
        	.warn {
        		color: red !important;
        		-moz-color: red !important;
        		background: #aaa;
        	}
.error {
	color: red;
	font-size: 12px;
	margin: 5px 0;
}
.btn-custom {
	cursor: pointer;
	padding: 4px 10px;
	text-align: center;
	background-color: #0000ffb3;
	border-radius: 5px;
	display: inline-block;
	color: #fff;
	font-size: 13px;
	margin: 5px 0;
}
.btn-custom:hover {
	opacity: .6;
}
#JSObrisi {
	background-color: #ff3333;
}
     
.padd {
	padding-top: 100px;
}

body {
	background-color: #f2f2f2;
}
* {
	box-sizing: border-box;
}
.row::after {
    content: "";
    clear: both;
    display: table;
}
[class*="col-"] {
    float: left;
}
.col-1 {width: 8.33%;}
.col-2 {width: 16.66%;}
.col-3 {width: 25%;}
.col-4 {width: 33.33%;}
.col-5 {width: 41.66%;}
.col-6 {width: 50%;}
.col-7 {width: 58.33%;}
.col-8 {width: 66.66%;}
.col-9 {width: 75%;}
.col-10 {width: 83.33%;}
.col-11 {width: 91.66%;}
.col-12 {width: 100%;}

.logo{
	display: block;
    padding: 10px;
    text-align: center;
    margin: auto;
}	
header{
	background-color: #4e0e7c;
}
.text-center {text-align: center;}
.button {
    display: inline-block;
    padding: 5px 10px;
    background-color: #f80;
    margin: 0;
    border-radius: 5px;
}
.button:hover{
	background-color: #f60;
	transition: .1s ease-in-out;
}
.input-div input{
	border-radius: 5px;
}
.select-div span ul li input{
    margin: 5px;
    height: auto;
    width: auto !important;
}
.select2-container--default .select2-selection--multiple .select2-selection__choice{
    margin: 4px 2px;
    font-size: 13px;
}
.select2-results__option{
	padding: 0 5px !important;
    font-size: 13px;
}
.go-back-btn{
    display: inline-block;
    background: #ddd;
    padding: 10px 15px;
    color: #000;
    font-size: .9em;
}
.go-back-btn::before{
	content: ' ';
	border: solid black;
	border-width: 0 3px 3px 0;
	display: inline-block;
	padding: 3px;
	margin: 0 5px;
    transform: rotate(135deg);
    -webkit-transform: rotate(135deg);
}
.more-space{
	padding: 0 20px;
}
h3{
	font-size: 1.4em; 
}
.select2-container--default .select2-selection--single .select2-selection__rendered{
	font-size: 14px;
}
        </style>
	</head>
	<body>
		<header> 
			<div class="row"> 
				<h1><img class="logo" src="https://www.kupindo.com/images/Kupindo_deo_limundograda-alpha.png" alt="Kupindo Logo"></h1>
			</div>
		</header>

		<div> 
			<a href="{{ AdminOptions::base_url()}}export/{{ $export->vrednost_kolone }}/{{ $kind }}/{{$key}}" class="go-back-btn">Nazad</a>
		</div>

		<div class=""> 
			<form method="POST" action="{{AdminOptions::base_url()}}export/grupe-povezi/{{ $key }}" id="JSPovezivanjeGrupa">
				<input type="hidden" name="base_url" value="{{ AdminOptions::base_url() }}">
				<input type="hidden" name="export_id" value="{{ $export->export_id }}">
				<input type="hidden" name="kind" value="{{ $kind }}">
				<input type="hidden" name="key" value="{{ $key }}">

			  	 <h3 class="text-center">Povezivanje grupa</h3>
			  	 <div class="row"> 
					 <div class="col-6 more-space"> 
						{{ $export->naziv }} grupe				
						<select name="grupa_id" id="grupa_id" class="m-input-and-button select2shopmania">						
							{{ Export\Support::selectGroups($export->export_id,Input::old('grupa_id')!=null?Input::old('grupa_id'):null) }}
						</select>
						<div class="error" id="JSObrisiError">{{ $errors->first('grupa_id') ? $errors->first('grupa_id') : '' }}</div>
					 </div>

					  <div class="col-6 more-space"> 
							Grupe u prodavnici				
							<select name="grupa_pr_id" class="m-input-and-button select2grupa">						
								{{ Export\Support::selectGroupsSm($export->export_id,$export_grupe,Input::old('grupa_pr_id')!=null?Input::old('grupa_pr_id'):null) }}
							</select>
							<div class="error">{{ $errors->first('grupa_pr_id') ? $errors->first('grupa_pr_id') : '' }}</div>
					 	</div>
				   </div>

  					<div class="row"> 
			  			<div class="col-6 more-space">
							<div class="btn-custom" id="JSObrisi">Razveži</div>
						</div>
						<div class="col-6 more-space">
							<div class="btn-custom" id="JSPovezi">Poveži</div>
						</div>
				  	</div>
			</form>
		</div>
			 
		<div class="row padd">
			<form method="POST" action="{{AdminOptions::base_url()}}export/grupa-file/{{ $key }}" enctype="multipart/form-data" id="JSUcitavanjeFajla">
				<input type="hidden" name="export_id" value="{{ $export->export_id }}">
				<input type="hidden" name="kind" value="{{ $kind }}">
				<input type="hidden" name="key" value="{{ $key }}">	
				<div class="col-12 more-space">
					<h3>Učitavanje fajla za {{ $export->naziv }} grupe</h3>
					<div class="">
						<input type="file" name="imp_file" id="imp_file" value="{{ Input::old('imp_file')!=null?Input::old('imp_file'):null }}">
					</div>
				</div>
			</form>
		</div>


		<script src="{{ AdminOptions::base_url()}}js/jquery-1.11.2.min.js" type="text/javascript"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.min.js"></script>
		<script src="{{ AdminOptions::base_url()}}js/admin/admin_export.js" type="text/javascript"></script>
		<script type="text/javascript">

			$(document).ready(function () {

				$(".select2shopmania").select2({
					placeholder: 'Izaberite grupu'
				});

				$(".select2grupa").select2({
					placeholder: 'Izaberite grupu'
				});

			});

			@if(Session::has('message'))
				alert("{{ Session::get('message') }}");
			@endif			
		</script>
	</body>
</html>