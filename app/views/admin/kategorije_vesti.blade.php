<div id="main-content" >
	@if(Session::has('message'))
	<script>
		alertify.success('{{ Session::get('message') }}');
	</script>
	@endif
	@include('admin/partials/tabs')
	<div class="row">
		<div class="large-3 medium-3 small-12 columns ">
			<div class="flat-box">
				<h3 class="title-med">{{ AdminLanguage::transAdmin('Izaberi kategoriju') }}</h3>
				<div class="manufacturer"> 
					<select class="admin-select JSeditSupport search_select">
						<option value="{{ AdminOptions::base_url() }}admin/kategorije-vesti/0">{{ AdminLanguage::transAdmin('Dodaj novi') }}</option>
						@foreach(AdminSupport::getKategorijeVesti() as $row)
						<option value="{{ AdminOptions::base_url() }}admin/kategorije-vesti/{{ $row->kategorije_vesti_id }}" @if($row->kategorije_vesti_id == $kategorije_vesti_id) {{ 'selected' }} @endif >{{ $row->naziv }}</option>
						@endforeach
					</select>
				</div>
			</div>
		</div>

		<section class="small-12 medium-12 large-5 large-centered columns">
			<div class="flat-box">

				<h1 class="title-med">{{ $title }}</h1> 

				<form method="POST" action="{{ AdminOptions::base_url() }}admin/kategorije-vesti-edit" enctype="multipart/form-data">
					<input type="hidden" name="kategorije_vesti_id" value="{{ $kategorije_vesti_id }}"> 
					<input type="hidden" name="jezik_id" value="{{ $jezik_id }}"> 

					<div class="row">
						<div class=" columns medium-9 {{ $errors->first('naziv') ? ' error' : '' }}">
							<label for="naziv">{{ AdminLanguage::transAdmin('Naziv kategorije') }}</label>
							<input type="text" name="naziv" data-id="" value="{{ htmlentities(Input::old('naziv') ? Input::old('naziv') : $naziv) }}" autofocus="autofocus" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'readonly' : '' }}> 
						</div>

						<div class="columns medium-3 {{ $errors->first('rbr') ? ' error' : '' }}">
							<label for="rbr">{{ AdminLanguage::transAdmin('Redni broj') }}</label>
							<input type="text" name="rbr" data-id="" value="{{ Input::old('rbr') ? Input::old('rbr') : $rbr }}" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'readonly' : '' }}> 
						</div> 
					</div>

					<br>

					@if(Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')))
					@if($kategorije_vesti_id != 0 && count($jezici) > 1) 
					<div class="languages">
						<ul>	
							@foreach($jezici as $jezik)
							<li><a class="{{ $jezik_id == $jezik->jezik_id ? 'active' : '' }} btn-small btn btn-secondary" href="{{AdminOptions::base_url()}}admin/kategorije_vesti/{{$kategorije_vesti_id}}/{{ $jezik->jezik_id }}">{{ $jezik->naziv }}</a></li>
							@endforeach
						</ul>
					</div> 
					@endif
					@endif

					<div class="row"> 
						<div class="columns">
							<h3 class="center seo-info tooltipz" aria-label="{{ AdminLanguage::transAdmin('SEO (Search Engine Optimization) polja je potrebno popuniti jednostavnim jezikom, kako bi Vas kupci lakše pronašli na internetu - ovi opisi nisu vidljivi direktno u prodavnici, već samo kako bi Google prepoznao Vaš proizvod i povezao Vas sa ljudima koji ga traže') }}.">{{ AdminLanguage::transAdmin('SEO') }}</h3>


							<div class="{{ $errors->first('seo_title') ? ' error' : '' }}">
								<label>{{ AdminLanguage::transAdmin('Naslov (title) do 60 karaktera') }}</label>
								<input type="text" name="seo_title" data-id="" value="{{ htmlentities(Input::old('seo_title') ? Input::old('seo_title') : $seo_title) }}" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'readonly' : '' }}> 
							</div>

							<div class="{{ $errors->first('description') ? ' error' : '' }}">
								<label id="seo_opis_label" for="description">{{ AdminLanguage::transAdmin('Opis (description) do 158 karaktera') }}</label>
								<textarea id="seo_opis" name="description" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'readonly' : '' }}>{{ Input::old('description') ? Input::old('description') : $description }}</textarea>
							</div>

							<div class="{{ $errors->first('keywords') ? ' error' : '' }}">
								<label for="keywords">{{ AdminLanguage::transAdmin('Ključne reči (keywords)') }}</label>
								<input type="text" name="keywords" value="{{ Input::old('keywords') ? Input::old('keywords') : $keywords }}" {{ Admin_model::check_admin(array('SIFARNICI_AZURIRANJE')) == false ? 'readonly' : '' }}>  
							</div>
						</div>
					</div>

					@if(Admin_model::check_admin(array('SIFARNICI_AZURIRANJE'))) 
					<div class="btn-container center">
						<button type="submit" class="btn btn-primary save-it-btn">{{ AdminLanguage::transAdmin('Sačuvaj') }}</button>
						@if($kategorije_vesti_id != 0)
						<a class="btn btn-danger JSbtn-delete" data-link="{{ AdminOptions::base_url() }}admin/kategorije-vesti-delete/{{ $kategorije_vesti_id }}" href="#">{{ AdminLanguage::transAdmin('Obriši') }}</a>
						@endif
					</div> 
					@endif
				</form>
			</div>
		</section>
	</div> 
</div>  
