<?php

class AkcijaController extends Controller {

	public function akcija_products(){
		$offset = Language::segment_offset();
		$lang = Language::multi() ? Request::segment(1) : null;
		$grupa = Request::segment(2+$offset);

		$datum = date('Y-m-d');
		if($grupa==null){
			$check_grupa = "";
		}else{
			foreach(DB::table('grupa_pr')->where('grupa_pr_id','!=',-1)->where('grupa_pr_id','!=',0)->get() as $gr){
			
		    $count = DB::select("SELECT DISTINCT COUNT(r.roba_id) FROM roba r LEFT JOIN roba_grupe rg ON rg.roba_id = r.roba_id".Product::checkImage('join').Product::checkCharacteristics('join')." WHERE r.flag_aktivan = 1 AND r.flag_prikazi_u_cenovniku = 1 AND akcija_flag_primeni = 1 AND CASE WHEN datum_akcije_od IS NOT NULL AND datum_akcije_do IS NOT NULL THEN datum_akcije_od <= '".$datum."' AND datum_akcije_do >= '".$datum."' WHEN datum_akcije_od IS NOT NULL AND datum_akcije_do IS NULL THEN datum_akcije_od <= '".$datum."' WHEN datum_akcije_od IS NULL AND datum_akcije_do IS NOT NULL THEN datum_akcije_do >= '".$datum."' ELSE 1 = 1 END ".Product::checkImage().Product::checkPrice().Product::checkDescription().Product::checkCharacteristics()."AND r.grupa_pr_id = ".$gr->grupa_pr_id."")[0]->count;
		    
				if(Url_mod::slug_trans($gr->grupa) == $grupa && $count > 0){
					$grupa_pr_id = $gr->grupa_pr_id;
					break;
				}
			}

			$level2=Groups::vratiSveGrupe($grupa_pr_id);

			$check_grupa = "AND r.grupa_pr_id IN(".implode(",",$level2).")";
		}

		    $select="SELECT DISTINCT r.roba_id, r.roba_flag_cene_id";
		    $roba=" FROM roba r".Product::checkImage('join').Product::checkCharacteristics('join')."";
			$where=" WHERE r.flag_aktivan = 1 AND r.flag_prikazi_u_cenovniku = 1 AND akcija_flag_primeni = 1 AND CASE WHEN datum_akcije_od IS NOT NULL AND datum_akcije_do IS NOT NULL THEN datum_akcije_od <= '".$datum."' AND datum_akcije_do >= '".$datum."' WHEN datum_akcije_od IS NOT NULL AND datum_akcije_do IS NULL THEN datum_akcije_od <= '".$datum."' WHEN datum_akcije_od IS NULL AND datum_akcije_do IS NOT NULL THEN datum_akcije_do >= '".$datum."' ELSE 1 = 1 END  ".Product::checkImage().Product::checkPrice().Product::checkDescription().Product::checkCharacteristics()."".$check_grupa."";

			$count=count(DB::select($select.$roba.$where));

	        	if(Session::has('limit')){
					$limit=Session::get('limit');
				}
				else {
					$limit=20;
				}

			    if(Input::get('page')){
			    	$pageNo = Input::get('page');
			    }else{
			    	$pageNo = 1;
			    }

			    $offset = ($pageNo-1)*$limit;
				
				if(Session::has('order')){
					if(Session::get('order')=='price_asc')
					{
					$artikli=DB::select("SELECT distinct t.roba_id, max(redniBroj) from (SELECT r.roba_id,ROW_NUMBER() OVER(ORDER BY ".Support::sortByFlagCene().",r.".Options::checkCena()." ASC) AS redniBroj, r.".Options::checkCena()."".$roba.$where." ORDER BY ".Support::sortByFlagCene().",r.".Options::checkCena()." ASC) t group by t.roba_id order by max(t.redniBroj) asc LIMIT ".$limit." OFFSET ".$offset."");
					}
					else if(Session::get('order')=='price_desc'){
					$artikli=DB::select("SELECT distinct t.roba_id, max(redniBroj) from (SELECT r.roba_id,ROW_NUMBER() OVER(ORDER BY ".Support::sortByFlagCene().",r.".Options::checkCena()." DESC) AS redniBroj, r.".Options::checkCena()."".$roba.$where." ORDER BY ".Support::sortByFlagCene().",r.".Options::checkCena()." DESC) t group by t.roba_id order by max(t.redniBroj) asc LIMIT ".$limit." OFFSET ".$offset."");
					}
					else if(Session::get('order')=='news'){
					$artikli=DB::select("SELECT distinct t.roba_id, max(redniBroj),naziv_web from (SELECT r.roba_id, ROW_NUMBER() OVER(ORDER BY ".Support::sortByFlagCene().",r.roba_id DESC) AS redniBroj,r.naziv_web".$roba.$where." ORDER BY ".Support::sortByFlagCene().",r.roba_id DESC) t group by t.roba_id,naziv_web order by max(redniBroj) asc LIMIT ".$limit." OFFSET ".$offset."");
					}
					else if(Session::get('order')=='name'){
					$artikli=DB::select("SELECT distinct t.roba_id, max(redniBroj),naziv_web from (SELECT r.roba_id, ROW_NUMBER() OVER(ORDER BY ".Support::sortByFlagCene().",r.naziv_web ASC) AS redniBroj,r.naziv_web".$roba.$where." ORDER BY ".Support::sortByFlagCene().",r.naziv_web ASC) t group by t.roba_id,naziv_web order by max(redniBroj) asc LIMIT ".$limit." OFFSET ".$offset."");
					}
					else if(Session::get('order') == 'rbr') {
						$artikli=DB::select("SELECT distinct t.roba_id, max(redniBroj),naziv_web from (SELECT r.roba_id, ROW_NUMBER() OVER(ORDER BY ".Support::sortByFlagCene().",r.rbr ASC) AS redniBroj,r.naziv_web".$roba.$where." ORDER BY ".Support::sortByFlagCene().",r.rbr ASC) t group by t.roba_id,naziv_web order by max(redniBroj) asc LIMIT ".$limit." OFFSET ".$offset."");
					}
				}
				else {
			    	$artikli=DB::select("SELECT distinct t.roba_id, max(redniBroj) from (SELECT r.roba_id,ROW_NUMBER() OVER(ORDER BY ".Support::sortByFlagCene().",r.".Options::checkCena()." ".(Options::web_options(207) == 0 ? "ASC" : "DESC").") AS redniBroj, r.".Options::checkCena()."".$roba.$where." ORDER BY ".Support::sortByFlagCene().",r.".Options::checkCena()." ".(Options::web_options(207) == 0 ? "ASC" : "DESC").") t group by t.roba_id order by max(t.redniBroj) asc LIMIT ".$limit." OFFSET ".$offset."");
				}

		$seo = Seo::page("akcija");
		$data=array(
			"strana"=>"akcija",
            "title"=>$seo->title,
            "description"=>$seo->description,
            "keywords"=>$seo->keywords,
            "url"=>"akcija".(!is_null($grupa) ? "/".$grupa : ""),
			"grupa"=>isset($grupa) ? $grupa : '',
			"articles"=>$artikli,
			"limit"=>$limit,
			"count_products"=>$count,
			"filter_prikazi" => 0
		);
 
		return View::make('shop/themes/'.Support::theme_path().'pages/products_list',$data);		
	}
}