<?php


class AdminImportProizvodjacController extends Controller {

    function import_podaci_proizvodjac($partner_id,$partner_proizvodjac_id){
        $proizvodjaci_dobavljaca = DB::table('dobavljac_cenovnik')->select('proizvodjac')->distinct()->where('partner_id',$partner_id)->whereNotIn('proizvodjac',array_map('current',DB::table('partner_proizvodjac')->select('proizvodjac')->where('partner_id',$partner_id)->get()))->orderBy('proizvodjac','ASC')->get();

        $data=array(
            "strana"=>'import_partner_proizvodjac',
            "title"=>'Povezivanje proizvodjača',
            "partner_id"=>$partner_id,
            "partner_proizvodjac_id"=>$partner_proizvodjac_id,
            'parner_proizvodjaci' => $partner_id != 0 ? DB::table('partner_proizvodjac')->where('partner_id',$partner_id)->orderBy('partner_proizvodjac_id','DESC')->get() : array(),
            'parner_proizvodjac_item' => $partner_proizvodjac_id != 0 ? DB::table('partner_proizvodjac')->where('partner_proizvodjac_id',$partner_proizvodjac_id)->first() : (object) array('partner_proizvodjac_id'=>0,'partner_id'=>$partner_id,'proizvodjac_id'=>-1,'partner_id'=>null,'proizvodjac'=>null),
            'proizvodjaci_dobavljaca'=> $proizvodjaci_dobavljaca
        );        
        return View::make('admin/page', $data);     
    }

    function import_podaci_proizvodjac_save(){
        $data = Input::all();

        $rules = array(
            'partner_id' => 'not_in:0',
            'proizvodjac_id' => 'not_in:0,-1',
            'proizvodjac' => 'required|regex:'.AdminSupport::regex().'|max:255|unique:partner_proizvodjac,proizvodjac,'.$data['partner_proizvodjac_id'].',partner_proizvodjac_id,partner_id,'.$data['partner_id'].''
        );
        $messages = array(
            'not_in' => 'Niste izabrali stavku.',
            'required' => 'Niste popunili polje.',
            'regex' => 'Polje sadrži ne dozvoljene karaktere.',
            'max' => 'Sadržaj polja je predugačak.',
            'unique' => 'Vrednost polja već postoji.'
            );

        $validator = Validator::make($data, $rules, $messages);
        if($validator->fails()){
            return Redirect::to(AdminOptions::base_url().'admin/import-podaci-proizvodjac/'.$data['partner_id'].'/'.$data['partner_proizvodjac_id'])->withInput()->withErrors($validator->messages());
        }else{
            if($data['partner_proizvodjac_id'] == 0){
                unset($data['partner_proizvodjac_id']);
                DB::table('partner_proizvodjac')->insert($data);
                $partner_proizvodjac_id = DB::select("SELECT currval('partner_proizvodjac_partner_proizvodjac_id_seq') as new_id")[0]->new_id;
                $mesage = 'Uspešno ste uneli stavku.';
            }else{
                DB::table('partner_proizvodjac')->where('partner_proizvodjac_id',$data['partner_proizvodjac_id'])->update($data);
                $partner_proizvodjac_id = $data['partner_proizvodjac_id'];
                $mesage = 'Uspešno ste sačuvali izmene.';
            }
            return Redirect::to(AdminOptions::base_url().'admin/import-podaci-proizvodjac/'.$data['partner_id'].'/'.$partner_proizvodjac_id)->with('message',$mesage);

        }
    }

    function import_podaci_proizvodjac_delete($partner_id,$partner_proizvodjac_id){

        DB::table('partner_proizvodjac')->where('partner_proizvodjac_id',$partner_proizvodjac_id)->delete();
        
        return Redirect::to(AdminOptions::base_url().'admin/import-podaci-proizvodjac/'.$partner_id.'/0')->with('message','Uspešno ste obrisali stavku');    
    }

}