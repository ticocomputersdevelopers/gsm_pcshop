<?php
use Service\TranslatorService;
use Service\ElasticSearchService;

class AdminAnketeController extends Controller {

    function ankete($anketa_id, $anketa_pitanje_id = null)
    {  
       
        $odgovori= !empty($anketa_pitanje_id) ? DB::table('anketa_odgovor')->where('anketa_pitanje_id',$anketa_pitanje_id)->orderBy('rbr', 'ASC')->get() : [];
        $pitanja=DB::table('anketa_pitanje')->where('anketa_id',$anketa_id)->orderBy('rbr', 'ASC')->get();
        $ankete = DB::table('anketa')->orderBy('anketa_id', 'ASC')->get();
        $aktivan=DB::table('anketa')->where('anketa_id',$anketa_id)->pluck('flag_aktivan');
        $izabran=DB::table('anketa')->where('anketa_id',$anketa_id)->pluck('izabran');
        $narudzbina_izabran=DB::table('anketa')->where('anketa_id',$anketa_id)->pluck('narudzbina_izabran');
        $anketa_odgovor_id=DB::table('anketa_odgovor')->where('anketa_pitanje_id',$anketa_pitanje_id)->pluck('anketa_odgovor_id');
        $tip = DB::table('anketa_pitanje')->where('anketa_pitanje_id',$anketa_pitanje_id)->pluck('tip');
        $data=array(
            "strana"=>'ankete',
            "title"=> $anketa_id != 0 ? AdminAnkete::find($anketa_id, 'naziv') : 'Nova anketa',
            "anketa_id"=> $anketa_id,
            "ankete" => $ankete,
            "anketa"=> $anketa_id != 0 ?  AdminLanguage::trans(AdminAnkete::find($anketa_id, 'naziv')) : null,
            "aktivan"=>$aktivan,
            "izabran"=>$izabran,
            "narudzbina_izabran"=>$narudzbina_izabran,
            "pitanja"=>$pitanja,
            "odgovori"=>$odgovori,
            "pitanje"=>$anketa_pitanje_id != 0 ? AdminAnkete::question($anketa_pitanje_id, 'pitanje') : '',
            "anketa_pitanje_id"=>$anketa_pitanje_id ? $anketa_pitanje_id : 0,
            "anketa_odgovor_id"=>$anketa_odgovor_id,
            "pitanje"=>DB::table('anketa_pitanje')->where('anketa_pitanje_id',$anketa_pitanje_id)->pluck('pitanje'),
            "aktivno_pitanje"=>DB::table('anketa_pitanje')->where('anketa_pitanje_id',$anketa_pitanje_id)->pluck('flag_aktivan'),
            "aktivan_odgovor"=>DB::table('anketa_odgovor')->where('anketa_odgovor_id',$anketa_odgovor_id)->pluck('flag_aktivan'),
            "tip"=>$tip

            
        );
         
            return View::make('admin/page',$data);
    }

    function anketa_edit()
    {   
        $inputs = Input::get();
        $flag_aktivan = Input::get('flag_active');
        $izabran = Input::get('default');
        $narudzbina_izabran = Input::get('order_default');

        if($flag_aktivan == 'on'){
            $flag_aktivan = 1;
        }else{
            $flag_aktivan = 0;
        }
        if($izabran == 'on'){
            $izabran = 1;
        }else{
            $izabran = 0;
        }
        if($narudzbina_izabran == 'on'){
            $narudzbina_izabran = 1;
        }else{
            $narudzbina_izabran = 0;
        }

        $validator_messages = array('required'=>'Polje ne sme biti prazno!',
                                    'max'=>'Prekoračili ste maksimalnu vrednost polja!',
                                    'regex' => 'Unesite odgovarajući karakter!'
                                );

        $validator_rules = array(
                'name' =>  'required|regex:'.Support::regex().'|max:100'
            );
        
        $validator = Validator::make($inputs, $validator_rules, $validator_messages);

        if($validator->fails()){
            return Redirect::to(AdminOptions::base_url().'admin/ankete/0')->withInput()->withErrors($validator->messages());
        }
        else{ 
            $query_result = false;
                if($izabran == 1){
                    DB::table('anketa')->update(['izabran'=>0]);
                }
                if($narudzbina_izabran == 1){
                    DB::table('anketa')->update(['narudzbina_izabran'=>0]);
                }

                if($inputs['survey_id']==0)
                {
                    $query_result = DB::table('anketa')->insert(array('naziv'=>$inputs['name'],'flag_aktivan'=>$flag_aktivan,'izabran'=>$izabran,'narudzbina_izabran'=>$narudzbina_izabran));
                }
                else 
                {
                    $query_result = DB::table('anketa')->where('anketa_id',$inputs['survey_id'])->update(array('naziv'=>$inputs['name'],'flag_aktivan'=>$flag_aktivan,'izabran'=>$izabran,'narudzbina_izabran'=>$narudzbina_izabran));
                }

            
            }
            
            $message='Uspešno ste sačuvali podatke.';
            return Redirect::to(AdminOptions::base_url().'admin/ankete/'.$inputs['survey_id'])->with('message',$message);
        
    }

    function anketa_delete($anketa_id)
    {   
       
         DB::table('anketa')->where('anketa_id',$anketa_id)->delete();
        return Redirect::to(AdminOptions::base_url().'admin/ankete/0')->with('message','Uspešno ste obrisali sadržaj.');

    }

    function anketa_pitanje()
    {
        $inputs = Input::get();
        $aktivno= Input::get('flag_active');
       
            if($aktivno==NULL)
            {
                $aktivno = '0';
            }

            
               $validator_messages = array('required'=>'Polje ne sme biti prazno!',
                                    'max'=>'Prekoračili ste maksimalnu vrednost polja!',
                                    'regex' => 'Unesite odgovarajući karakter!'
                                );

                $validator_rules = array(
                        'question' =>'required|regex:'.Support::regex().'|max:200'
                    );
        
                $validator = Validator::make($inputs, $validator_rules, $validator_messages);

            if($validator->fails()){
                 return Redirect::back()->withInput()->withErrors($validator->messages());
            }
            else{ 

                $query_result = false;

                if($inputs['survey_question_id'] !='anketa_pitanje_id')
                {
                    $rbr = ($max=DB::table('anketa_pitanje')->where('anketa_id',$inputs['survey_id'])->max('rbr')) ? $max+1 : 1;

                     $query_result = DB::table('anketa_pitanje')->insert(array('anketa_id'=>$inputs['survey_id'],'tip'=>$inputs['type'],'pitanje'=>$inputs['question'],'flag_aktivan'=>$aktivno, 'rbr'=>$rbr));

                     $anketa_pitanje = DB::table('anketa_pitanje')->orderBy('anketa_pitanje_id','desc')->first();
                     DB::table('anketa_odgovor')->insert(array('anketa_pitanje_id'=>$anketa_pitanje->anketa_pitanje_id,'odgovor'=>null,'flag_aktivan'=>1));

                }
                 return Redirect::to(AdminOptions::base_url().'admin/ankete/'.$inputs['survey_id'].'/'.$inputs['survey_question_id'])->with('message','Uspešno ste sačuvali pitanje.');
            
             }
        
    }

    function anketa_pitanje_edit($anketa_pitanje_id=null)
    {
        $inputs = Input::get();
        $aktivno= Input::get('flag_active');
        if($aktivno==NULL){
                $aktivno = '0';
            }
          
        $validator = Validator::make($inputs,
            array(
                'question' => 'required|regex:'.AdminSupport::regex().'|max:200',
                'type' => 'required'
                )
            );

        if($validator->fails())
        {
            return Redirect::back()->withInput()->withErrors($validator->messages());
        }
        else
        {
      
              DB::table('anketa_pitanje')->where('anketa_pitanje_id',$inputs['survey_question_id'])->update(array('tip'=>$inputs['type'],'pitanje'=>$inputs['question'],'flag_aktivan'=>$aktivno));  
              return Redirect::back()->with('message','Uspešno ste izmenili pitanje.');
        }

    }

    function pitanje_delete($anketa_pitanje_id)
    {   
       
         DB::table('anketa_pitanje')->where('anketa_pitanje_id',$anketa_pitanje_id)->delete();
       // return Redirect::to(AdminOptions::base_url().'admin/ankete/')->with('message','Uspešno ste obrisali pitanje.');
         return Redirect::back()->with('message','Uspešno ste obrisali pitanje.');


    }

    function anketa_odgovor()
    {
        $inputs = Input::get();
        $aktivno=Input::get('flag_active');

            if($aktivno==NULL){
                $aktivno = '0';
            }
            $validator_messages = array('required'=>'Polje ne sme biti prazno!',
                                    'max'=>'Prekoračili ste maksimalnu vrednost polja!',
                                    'regex' => 'Unesite odgovarajući karakter!'
                                );

            $validator = Validator::make($inputs,
            array(
                'answer' => 'required|regex:'.AdminSupport::regex().'|max:250'
                )
            );

            if($validator->fails())
            {
                return Redirect::back()->withInput()->withErrors($validator->messages());
            }
            else
            {
                $rbr = ($max=DB::table('anketa_odgovor')->where('anketa_pitanje_id',$inputs['survey_question_id'])->max('rbr')) ? $max+1 : 1;
                
                 DB::table('anketa_odgovor')->insert(array('anketa_pitanje_id'=>$inputs['survey_question_id'],'odgovor'=>$inputs['answer'],'flag_aktivan'=>$aktivno, 'rbr' => $rbr));
            }
           // return Redirect::to(AdminOptions::base_url().'admin/ankete/0')->with('message','Uspešno ste sacuvali odgovor.');
            return Redirect::back()->with('message','Uspešno ste sacuvali odgovor.');
        
    }
    function anketa_odgovor_edit($anketa_odgovor_id=null)
    {
        $inputs = Input::get();
        //var_dump($inputs);die;
        
        $aktivno=Input::get('flag_active');

            if($aktivno==NULL){
                $aktivno = '0';
            }
            $validator_messages = array('required'=>'Polje ne sme biti prazno!',
                                    'max'=>'Prekoračili ste maksimalnu vrednost polja!',
                                    'regex' => 'Unesite odgovarajući karakter!'
                                );

            $validator = Validator::make($inputs,
            array(
                'answer' => 'required|regex:'.AdminSupport::regex().'|max:250'
                )
            );

            if($validator->fails())
            {
                return Redirect::back()->withInput()->withErrors($validator->messages());
            }
            else
            {
      
                if(isset($anketa_odgovor_id))
                {  
                    DB::table('anketa_odgovor')->where('anketa_odgovor_id',$inputs['survey_answer_id'])->update(array('odgovor'=>$inputs['answer'],'flag_aktivan'=>$aktivno));
                }
                 return Redirect::back()->with('message','Uspešno ste editovali odgovor.');
            }
    }
    function odgovor_delete($anketa_odgovor_id)
    {   
       
        DB::table('anketa_odgovor')->where('anketa_odgovor_id',$anketa_odgovor_id)->delete();
         return Redirect::back()->with('message','Uspešno ste obrisali odgovor');

    }

    function poll_question_position(){
        foreach(Input::get('order') as $sort => $id){
            DB::table('anketa_pitanje')->where('anketa_pitanje_id',$id)->update(['rbr'=>($sort+1)]);
        }
    }
    function poll_answer_position(){
        foreach(Input::get('order') as $sort => $id){
            DB::table('anketa_odgovor')->where('anketa_odgovor_id',$id)->update(['rbr'=>($sort+1)]);
        }
    }


}

