<?php


class AdminB2BArticlesController extends Controller {

    function article_list($grupa_pr_id, $proizvodjac = null, $dobavljac = null, $magacin = null, $filteri = null, $search = null, $nabavna = null, $order = null){
        $criteria = array("grupa_pr_id" => $grupa_pr_id, "proizvodjac"=>$proizvodjac, "dobavljac"=>$dobavljac, "magacin"=>$magacin, "filteri"=>$filteri, "nabavna"=>$nabavna, "search"=>$search);
        // $limit = AdminB2BOptions::limit_liste_robe();
        $limit = 100;
        $pagination = array(
            'limit' => $limit,
            'offset' => Input::get('page') ? (Input::get('page')-1)*$limit : 0
            );
        $artikli = AdminB2BArticles::fetchAll($criteria, $pagination, $order);
        $grupa_title = AdminB2BCommon::grupa_title($grupa_pr_id);
        $all_articles = array_map('current',AdminB2BArticles::fetchAll($criteria));
        $count = count($all_articles);

        $data=array(
            "strana"=>'b2b_artikli',
            "title"=> 'Rabat grupa i artikala',
            "grupa_pr_id"=>$grupa_pr_id,
            "naziv_grupe"=>$grupa_pr_id != 0 ? $grupa_title : 'Artikli iz svih grupa',
            "criteria"=>$criteria,
            "articles"=>$artikli,
            "count_products"=>$count,
            "limit"=>$limit,
            "all_ids" => json_encode($all_articles)
        );
        return View::make('adminb2b/pages/article_list', $data);
    }

    function ajax() {
        $action = Input::get('action');
        

        if($action == 'rabat_edit') {
            $grupa_id = Input::get('grupa_id');
            $rabat = Input::get('rabat');


            $query = DB::table('grupa_pr')->where('grupa_pr_id', $grupa_id)->update(['rabat' => $rabat]);
            
            if($query == true) {
                AdminSupport::saveLog('B2B_RABAT_IZMENI', array($grupa_id));
                return json_encode(1);
            } else {
                return json_encode(0);
            }
           
        } else if($action == 'akc_rabat_group_edit'){
            $akcrabat= Input::get('akcrabat');
            $roba_ids = Input::get('roba_ids');
            $response = array();

            foreach ($roba_ids as $roba_id) {
                DB::statement("UPDATE roba SET b2b_akcijski_rabat = ". $akcrabat ." WHERE roba_id = ".$roba_id."");
                $response[$roba_id] = DB::table('roba')->where('roba_id',$roba_id)->pluck('b2b_akcijski_rabat');
            }

            AdminSupport::saveLog('B2B_RABAT_AKCIJSKA_GRUPA_IZMENI', $roba_ids);
            return json_encode($response);


        }else if ($action == 'rabat_group_edit') {
            $rabat = Input::get('rabat');
            $roba_ids = Input::get('roba_ids');
            $response = array();

            foreach($roba_ids as $roba_id){
                DB::statement("UPDATE roba SET b2b_max_rabat = ". $rabat ." WHERE roba_id = ".$roba_id."");
                $response[$roba_id] = DB::table('roba')->where('roba_id',$roba_id)->pluck('b2b_max_rabat');
            }

            AdminSupport::saveLog('B2B_RABAT_GRUPA_IZMENI', $roba_ids);
            return json_encode($response);

        } else if ($action = 'rabat_kombinacija_edit') {
            $rabat = Input::get('rabat');
            $id = Input::get('id');


            $query = DB::table('partner_rabat_grupa')->where('id', $id)->update(['rabat' => $rabat]);

                
            if($query == true) {
                AdminSupport::saveLog('B2B_RABAT_KOMBINACIJA_IZMENI', array($id));
                return json_encode(1);
            } else {
                return json_encode(0);
            }
        } 
    }
}