<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;


class ResizeImagesCommand extends Command {
	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'images:resize';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Resize images.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$path = $this->argument('path');
		$width = $this->argument('width');
		if(is_null($width)){
        	$width = DB::table('options')->where('options_id',1331)->pluck('int_data');
		}

		foreach(File::files($path) as $file){
		    if(in_array(getimagesize($file)[2], array(IMAGETYPE_JPEG ,IMAGETYPE_PNG , IMAGETYPE_BMP)))
		    {
				Image::make($file)->resize($width, null, function ($constraint) {
					$constraint->aspectRatio(); })->save();		    	
		    }
		}
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			array('path', InputArgument::OPTIONAL, 'Image path on root.'),
			array('width', InputArgument::OPTIONAL, 'Image width.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
		// 	array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}