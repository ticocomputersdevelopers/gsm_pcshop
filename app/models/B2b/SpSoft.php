<?php

class SpSoft {

    public static function createOrder($orderId){
        $order = DB::table('web_b2b_narudzbina')->where('web_b2b_narudzbina_id',$orderId)->first();

        $cartItems=DB::table('web_b2b_narudzbina_stavka')->where('web_b2b_narudzbina_id',$orderId)->orderBy('broj_stavke','asc')->get();
        $nacin_isporuke = DB::table('web_nacin_isporuke')->where('web_nacin_isporuke_id',$order->web_nacin_isporuke_id)->pluck('naziv');
        $nacin_placanja = DB::table('web_nacin_placanja')->where('web_nacin_placanja_id',$order->web_nacin_placanja_id)->pluck('naziv');
        $note=substr('Način isporuke: '.$nacin_isporuke.'. Način plaćanja: '.$nacin_placanja.'. Napomena: '.$order->napomena,0,255);
        // $avans = (isset($order->web_nacin_placanja_id) && $order->web_nacin_placanja_id != 9) ? 0 : -4.1667;

        $success = false;
        $partner = DB::table('partner')->where('partner_id',$order->partner_id)->first();

        if(!is_null($partner) && !is_null($partner->id_is)){
            // $troskovi = B2bBasket::troskovi();
            self::createOrderDocument($partner,$order,$note);

            foreach($cartItems as $key => $stavka){
                $roba = DB::table('roba')->where('roba_id',$stavka->roba_id)->first();
                if(!empty($roba->id_is)){
                    $rabatObj = B2bArticle::b2bRabatCene($stavka->roba_id,$order->partner_id);
                    self::addOrderItem($order->web_b2b_narudzbina_id,intval($roba->id_is),$stavka->kolicina,$stavka->jm_cena,$rabatObj->ukupan_rabat,($key+1));
                }
            }
            $success = true;
        }
        return (object) array('success'=>$success, 'order_id'=>null);
    }

    public static function createOrderDocument($partner,$order,$note){
        $docKey = null;
        $db = DB::connection('ortopedija');

        $data = array(
            'id' => $order->web_b2b_narudzbina_id,
            'datDok' => $order->datum_dokumenta,
            'vrstaDok' => 'B2B',
            'idPP' => intval($partner->id_is),
            'statusDok' => 0,
            'komentar' => $note
        );
        $db->table('narucenowebz')->insert($data);
    }

    public static function addOrderItem($orderId,$articleId,$quantity,$price,$rebate,$sortNumber){
        $itemOrder = null;
        $db = DB::connection('ortopedija');

        $data = array(
            'id' => $orderId,
            'rb' => $sortNumber,
            'idArt' => $articleId,
            'kolicina' => $quantity,
            'cena' => $price,
            'procRabat' => $rebate
        );
        $db->table('narucenowebs')->insert($data);
    }


}