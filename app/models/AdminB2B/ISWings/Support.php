<?php
namespace ISWings;

use DB;
use finfo;
use Request;

class Support {

	public static function getJedinicaMereId($jedinica_mere){
		$jm = DB::table('jedinica_mere')->where('naziv',$jedinica_mere)->first();

		if(is_null($jm)){
			DB::table('jedinica_mere')->insert(array(
				'jedinica_mere_id' => DB::table('jedinica_mere')->max('jedinica_mere_id')+1,
				'naziv' => $jedinica_mere
				));
			$jm = DB::table('jedinica_mere')->where('naziv',$jedinica_mere)->first();
		}
		return $jm->jedinica_mere_id;
	}

	public static function getProizvodjacId($proizvodjac){
		$pro = DB::table('proizvodjac')->where('naziv',$proizvodjac)->first();

		if(is_null($pro)){
			DB::table('proizvodjac')->insert(array(
				'naziv' => $proizvodjac,
				'sifra_connect' => 0
				));
			$pro = DB::table('proizvodjac')->where('naziv',$proizvodjac)->first();
		}
		return $pro->proizvodjac_id;
	}

	public static function getMappedArticles(){
		$mapped = array();
		$articles = DB::table('roba')->whereNotNull('id_is')->where('id_is','!=','')->get();
		foreach($articles as $article){
			$mapped[trim(strval($article->id_is))] = $article->roba_id;
		}
		return $mapped;
	}

	public static function getMappedArticlesByCode(){
		$mapped = array();
		$articles = DB::table('roba')->whereNotNull('sifra_is')->where('sifra_is','!=','')->get();
		foreach($articles as $article){
			$mapped[trim(strval($article->sifra_is))] = $article->roba_id;
		}
		return $mapped;
	}

	public static function internalGroupMapped($groups){
		$mapped = array();
		foreach($groups as $group){
			$mapped[$group->attributes->sifra] = $group->id;
		}
		return $mapped;
	}

	public static function updateGroupsParent($groups,$internalGroupMapped){
		foreach($groups as $group){
			$group = $group->attributes;
			$group_id = isset($internalGroupMapped[$group->sifra]) ? intval($internalGroupMapped[$group->sifra]) : null;
			$parent_id = isset($group->nad) && $group->nad != 0 ? intval($group->nad) : 0;
			if(!is_null($group_id) && $parent_id != 0){
				$parrent_grupa_pr = DB::table('grupa_pr')->where('id_is',$parent_id)->first();
				if(!is_null($parrent_grupa_pr)){
					$parrent_grupa_pr_id = $parrent_grupa_pr->grupa_pr_id;
					// $parrent_grupa_pr_id = DB::table('grupa_pr')->where('grupa_pr_id',$parrent_grupa_pr_id)->pluck('parrent_grupa_pr_id');
					DB::table('grupa_pr')->where(array('id_is'=>$group_id))->update(array('parrent_grupa_pr_id'=>$parrent_grupa_pr_id));
				}else{
					DB::table('grupa_pr')->where('parrent_grupa_pr_id',$parent_id)->delete();
				}
			}
		}

	}

	public static function getMappedGroups(){
		$mapped = array();
		$groups = DB::table('grupa_pr')->whereNotNull('id_is')->where('id_is','!=','')->get();
		foreach($groups as $group){
			$mapped[$group->id_is] = $group->grupa_pr_id;
		}
		return $mapped;
	}

	public static function saveImages($token,$idIss,$mappedArticles){
		$web_slika_id= DB::select("SELECT MAX(web_slika_id) + 1 AS next_id FROM web_slika")[0]->next_id;
		if(is_null($web_slika_id)){
			$web_slika_id = 1;
		}
		foreach($idIss as $idIs){
		    if(isset($mappedArticles[$idIs])){
				try { 
				    $url = 'https://portal.wings.rs/scripts/imagemanager.php?las_imgSize=large&las_imgId='.$idIs;
				    // $destination = '/var/www/html/auto_shops/'.Request::server("SERVER_NAME").'/images/products/big/';
				    $destination = '/var/www/html/wbp/images/products/big/';

					$options = array(
					  'http'=>array(
					    'method'=>"GET",
					    'header'=>
					            "Host: portal.wings.rs\r\n".
								"User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:66.0) Gecko/20100101 Firefox/66.0\r\n".
								"Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8\r\n".
								"Accept-Language: en-US,en;q=0.5\r\n".
								"Accept-Encoding: gzip, deflate, br\r\n".
								"Connection: keep-alive\r\n".
								"Cookie: PHPSESSID=".$token.";\r\n".
								"Upgrade-Insecure-Requests: 1\r\n"
					  )
					);

					$context = stream_context_create($options);
					$content = @file_get_contents($url, false, $context);
					$finfo = new finfo(FILEINFO_MIME_TYPE);
					$buffer = $finfo->buffer($content);

					if(strpos($buffer, 'image') !== false){
						$name = $web_slika_id.'.'.str_replace('image/','',$buffer);
						file_put_contents($destination.$name,$content);	

						DB::statement("INSERT INTO web_slika (web_slika_id,roba_id,akcija,putanja) VALUES (".$web_slika_id.",".$mappedArticles[$idIs].",1,'images/products/big/".$name."')");
						$web_slika_id++;
					}
				}
				catch (Exception $e) {
				}
			}
		}

	}

}