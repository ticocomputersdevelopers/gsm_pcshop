<?php
namespace ISCustom;
use AdminB2BIS;
use PHPExcel;
use PHPExcel_IOFactory;

class ManufacturersExcelNiansa {

	public static function table_body(){

		$products_file = "files/IS/excel/proizvodjac.xlsx";
		$excelReader = PHPExcel_IOFactory::createReaderForFile($products_file);
		$excelObj = $excelReader->load($products_file);
		$worksheet = $excelObj->getSheet(1);
		$lastRow = $worksheet->getHighestRow();

		$result_arr = array("(-1,'Svi proizvodjaci',NULL,-1,-1,0,NULL,NULL,NULL,0,0)");
		$manufacturer_ids = array(-1);
		
		for ($row = 1; $row <= $lastRow; $row++) {
		    $A = $worksheet->getCell('A'.$row)->getValue();
		    $B = $worksheet->getCell('B'.$row)->getValue();

		    if(is_numeric($A) && isset($B)){
				$manufacturer_ids[] = intval($A);
				$result_arr[] = "(".$A.",'".addslashes(AdminB2BIS::encodeTo1250($B))."',NULL,-1,-1,0,NULL,NULL,NULL,0,0)";
		    }

		}
		return (object) array("body"=>implode(",",$result_arr),"ids"=>$manufacturer_ids);
	}


}