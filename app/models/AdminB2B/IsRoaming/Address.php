<?php
namespace IsRoaming;

use DB;

class Address {

    public static function table_body($address,$getMappedPartners){
        $result_arr = array();
        $poslovnica_id = DB::select("SELECT nextval('poslovnica_poslovnica_id_seq')")[0]->nextval;

        foreach($address as $item) {
            $partner_id = isset($getMappedPartners[strval($item->id)]) ? $getMappedPartners[strval($item->id)] : null;

            if(!is_null($partner_id)){
                $poslovnica_id++;

                $naziv = isset($item->naziv) ? $item->naziv : '';
                $adresa = isset($item->adresa) ? $item->adresa : '';
                $posta = isset($item->posta) ? $item->posta : '';
                $grad = isset($item->grad) ? $item->grad : '';


                $result_arr[] = "(".$poslovnica_id.",".strval($partner_id).",'".$naziv."','".$adresa."','".$posta."','".$grad."')";

            }
        }

        return (object) array("body"=>implode(",",$result_arr));
    }

    public static function query_insert_update($table_temp_body,$upd_cols=array()) {
        if($table_temp_body == ''){
            return false;
        }
        $columns = array_map('current',DB::select("SELECT column_name FROM information_schema.columns where table_name='poslovnica'"));
        $table_temp = "(VALUES ".$table_temp_body.") poslovnica_temp(".implode(',',$columns).")";

        // update
        $updated_columns=array();
        if(count($upd_cols)>0){
            $columns = $upd_cols;
        }
        $updated_columns = array();
        foreach($columns as $col){
            if($col!="poslovnica_id"){
                $updated_columns[] = "".$col." = poslovnica_temp.".$col."";
            }
        }


        //delete
        DB::statement("DELETE FROM poslovnica WHERE (partner_id,naziv,adresa,posta,grad) NOT IN (SELECT partner_id,naziv,adresa,posta,grad FROM ".$table_temp.")");

        //insert
        DB::statement("INSERT INTO poslovnica (SELECT * FROM ".$table_temp." WHERE (partner_id,naziv,adresa,posta,grad) NOT IN (SELECT partner_id,naziv,adresa,posta,grad FROM poslovnica))");

        DB::statement("SELECT setval('poslovnica_poslovnica_id_seq', (SELECT MAX(poslovnica_id) FROM poslovnica), FALSE)");
    }

    public static function query_delete_unexists($table_temp_body) {
        //
    }
}
