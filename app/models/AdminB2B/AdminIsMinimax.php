<?php
ini_set('memory_limit', '-1');
ini_set('max_execution_time', 3600);


use IsMinimax\ApiData;
use IsMinimax\Article;
use IsMinimax\Stock;
use IsMinimax\Partner;
use IsMinimax\PartnerCard;
use IsMinimax\Support;



class AdminIsMinimax {
    // http://aquatec.selltico.com/auto-import-is/1771be861949aebbbb9aeb3ad54cc346
    public static function execute(){

        try {
            $token = ApiData::getToken();
            if(!is_null($token)){
                
                //partner
                $partners=array();
                $partnarIds=array();
                $apiPartners = ApiData::partners($token, 1);
                $pages=intdiv($apiPartners['TotalRows'], 100) + (fmod($apiPartners['TotalRows'],100) >0 ? 1 : 0);
                foreach (range(1, $pages) as $page) {
                    $apiPartners = ApiData::partners($token, $page);
                    foreach($apiPartners['Rows'] as $row){
                        if(!in_array($row['CustomerId'],$partnarIds)){
                            $partnarIds[] = $row['CustomerId'];
                            $partners[] = $row;
                        }
                    }
                }
                $resultPartner = Partner::table_body($partners);
                Partner::query_insert_update($resultPartner->body,array('sifra','naziv','adresa','mesto'));

                //ARTIKLI
                $articles=array();
                $articleIds=array();
                $apiArticles = ApiData::articles($token, 1);
                $pages=intdiv($apiArticles['TotalRows'], 100) + (fmod($apiArticles['TotalRows'],100) >0 ? 1 : 0);
                foreach (range(1, $pages) as $page) {
                    $apiArticles = ApiData::articles($token, $page);
                    foreach($apiArticles['Rows'] as $row){
                        if(!in_array($row['ItemId'],$articleIds)){
                            $articleIds[] = $row['ItemId'];
                            $articles[] = $row;
                        }
                    }
                }
                $resultArticle = Article::table_body($articles);
                Article::query_insert_update($resultArticle->body,array('naziv','naziv_web','racunska_cena_nc','racunska_cena_a','racunska_cena_end','mpcena','web_cena','jedinica_mere_id'));
                Article::query_update_unexists($resultArticle->body);
                Support::entityDecode();


                // LAGER
                $mapped_articles = Support::getMappedArticles();
                $articlesStock=array();
                $articlesStockCodes=array();
                $apiArticlesStock = ApiData::stock($token, 1);
                $pages=intdiv($apiArticlesStock['TotalRows'], 100) + (fmod($apiArticlesStock['TotalRows'],100) >0 ? 1 : 0);
                foreach (range(1, $pages) as $page) {
                    $apiArticlesStock = ApiData::stock($token, $page);
                    foreach($apiArticlesStock['Rows'] as $row){
                        if(!in_array($row['ItemCode'],$articlesStockCodes)){
                            $articlesStockCodes[] = $row['ItemCode'];
                            $articlesStock[] = $row;
                        }
                    }
                }
                $resultArticleStock = Stock::table_body($articlesStock,$mapped_articles);
                Stock::query_insert_update($resultArticleStock->body);
               
                Support::postUpdate();
            }
            else{
                AdminB2BIS::saveISLog('false');
                return (object) array('success'=>false);
            }

            AdminB2BIS::saveISLog('true');
            return (object) array('success'=>true);

        } catch (Exception $e) {
            AdminB2BIS::saveISLog('false');
            return (object) array('success'=>false,'message'=>$e->getMessage());            
        }
    }

    // http://aquatec.selltico.com/auto-import-is-partner-full/1771be861949aebbbb9aeb3ad54cc346
    public static function executePartnerFull(){

        try {
            $token = ApiData::getToken();
            if(!is_null($token)){
                
                //partner
                $partners=array();
                $partnarIds=array();
                $apiPartners = ApiData::partners($token, 1);
                $pages=intdiv($apiPartners['TotalRows'], 100) + (fmod($apiPartners['TotalRows'],100) >0 ? 1 : 0);
                $pages=1;
                foreach (range(1, $pages) as $page) {
                    $apiPartners = ApiData::partners($token,$page,true);
                    foreach($apiPartners['Rows'] as $row){
                        if(!in_array($row['CustomerId'],$partnarIds)){
                            $partnarIds[] = $row['CustomerId'];
                            $partners[] = $row;
                        }
                    }
                }
                $resultPartner = Partner::table_body($partners);
                Partner::query_insert_update($resultPartner->body,array('sifra','naziv','adresa','mesto','rabat'));
               
            }
            else{
                AdminB2BIS::saveISLog('false');
                return (object) array('success'=>false);
            }

            AdminB2BIS::saveISLog('true');
            return (object) array('success'=>true);

        } catch (Exception $e) {
            AdminB2BIS::saveISLog('false');
            return (object) array('success'=>false,'message'=>$e->getMessage());            
        }
    }


}