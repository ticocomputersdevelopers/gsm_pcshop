<?php
namespace IsGSM;

use DB;
use AdminImport;
use All;

class Support {


	public static function getPartnerId($sifra_kupca_logik){
		$partner = DB::table('partner')->where('id_is',$sifra_kupca_logik)->first();
		if(!is_null($partner)){
			return $partner->partner_id;
		}
		return null;
	}

	public static function getTarifnaGrupaId($naziv_tarifne_grupe,$vrednost_tarifne_grupe=20){
		$tg = DB::table('tarifna_grupa')->where(array('porez'=>$vrednost_tarifne_grupe))->first();

		if(is_null($tg)){
			$next_id = DB::table('tarifna_grupa')->max('tarifna_grupa_id')+1;
			DB::table('tarifna_grupa')->insert(array(
				'tarifna_grupa_id' => $next_id,
				'sifra' => $next_id,
				'naziv' => substr($naziv_tarifne_grupe,0,99),
				'porez' => $vrednost_tarifne_grupe,
				'active' => 1,
				'default_tarifna_grupa' => 0,
				'tip' => substr($vrednost_tarifne_grupe,0,19)
				));
			$tg = DB::table('tarifna_grupa')->where('porez',$vrednost_tarifne_grupe)->first();
		}
		return $tg->tarifna_grupa_id;
	}

	public static function getJedinicaMereId($jedinica_mere){
		$jm = DB::table('jedinica_mere')->where('naziv',$jedinica_mere)->first();

		if(is_null($jm)){
			DB::table('jedinica_mere')->insert(array(
				'jedinica_mere_id' => DB::table('jedinica_mere')->max('jedinica_mere_id')+1,
				'naziv' => $jedinica_mere
				));
			$jm = DB::table('jedinica_mere')->where('naziv',$jedinica_mere)->first();
		}
		return $jm->jedinica_mere_id;
	}

	public static function getProizvodjacId($proizvodjac){
		$pro = DB::table('proizvodjac')->where('naziv',trim($proizvodjac))->first();

		if(is_null($pro)){
			DB::table('proizvodjac')->insert(array(
				'naziv' => trim($proizvodjac),
				'sifra_connect' => 0,
				'id_is' => trim($proizvodjac)
				));
			$pro = DB::table('proizvodjac')->where('naziv',$proizvodjac)->first();
		}
		return $pro->proizvodjac_id;
	}
	public static function saveProizvodjac($proizvodjac){
		$pro = DB::table('proizvodjac')->where('naziv',trim($proizvodjac))->first();

		if(is_null($pro)){
			DB::table('proizvodjac')->insert(array(
				'naziv' => trim($proizvodjac),
				'sifra_connect' => 0,
				'id_is' => trim($proizvodjac)
				));
		}
	}

	public static function getGrupaId($group_name){
		$group_name = trim($group_name);
		$group = DB::table('grupa_pr')->where('grupa',$group_name)->first();

		if(is_null($group)){
			$grupa_pr_id = DB::select("SELECT MAX(grupa_pr_id) + 1 AS max FROM grupa_pr")[0]->max;
			DB::table('grupa_pr')->insert(array(
				'grupa_pr_id' => $grupa_pr_id,
				'grupa' => $group_name,
				'parrent_grupa_pr_id' => 0,
				'sifra' => $grupa_pr_id
				));
		}else{
			$grupa_pr_id = $group->grupa_pr_id;
		}
		return $grupa_pr_id;
	}

	public static function getPartnerCategoryId($category_name){
		$category_name = trim($category_name);
		$category = DB::table('partner_kategorija')->where('naziv',$category_name)->first();

		if(is_null($category)){
			$id_kategorije = DB::select("SELECT MAX(id_kategorije) + 1 AS max FROM partner_kategorija")[0]->max;
			if(is_null($id_kategorije)){
				$id_kategorije = 1;
			}
			DB::table('partner_kategorija')->insert(array(
				'id_kategorije' => $id_kategorije,
				'naziv' => $category_name,
				'rabat' => 0,
				'active' => 1
				));
		}else{
			$id_kategorije = $category->id_kategorije;
		}
		return $id_kategorije;
	}

	public static function getSellers($partners){
		$sellers = array();
		foreach($partners as $partner){
			$sellers[] = $partner->komercijalista;
		}
		return array_unique($sellers);
	}

	public static function getMappedArticles(){
		$mapped = array();
		$articles = DB::table('roba')->whereNotNull('id_is')->where('id_is','!=','')->get();
		foreach($articles as $article){
			$mapped[trim(strval($article->id_is))] = $article->roba_id;
		}
		return $mapped;
	}
	public static function linkedWidthDC(){
		DB::table("UPDATE dobavljac_cenovnik dc SET roba_id = r.roba_id, povezan = 1 FROM roba r WHERE dc.partner_id = r.dobavljac_id AND dc.sifra_kod_dobavljaca = r.sifra_d AND povezan <> 1 AND dc.roba_id = -1 AND r.sifra_d IS NOT NULL AND r.sifra_d <> '' AND r.dobavljac_id <> -1 AND r.id_is IS NOT NULL AND r.id_is <> ''");

		DB::table("UPDATE dobavljac_cenovnik dc SET web_cena = r.web_cena, mpcena = r.mpcena, grupa_pr_id = r.grupa_pr_id, proizvodjac_id = r.proizvodjac_id, tarifna_grupa_id = r.tarifna_grupa_id FROM roba r WHERE dc.partner_id = r.dobavljac_id AND dc.sifra_kod_dobavljaca = r.sifra_d AND dc.roba_id = r.roba_id AND r.id_is IS NOT NULL AND r.id_is <> ''");

		DB::table("UPDATE dobavljac_cenovnik dc SET roba_id = -1, povezan = 0 WHERE roba_id <> -1 AND povezan = 1 AND roba_id NOT IN (SELECT roba_id FROM roba)");

		DB::table("UPDATE roba SET sifra_d = null, dobavljac_id = -1 WHERE (sifra_d is not null OR dobavljac_id <> -1) AND (sifra_d, dobavljac_id) NOT IN (SELECT sifra_kod_dobavljaca, partner_id FROM dobavljac_cenovnik)");
	}

	public static function getMappedPartners(){
		$mapped = array();
		$partners = DB::table('partner')->select('id_is','partner_id')->whereNotNull('id_is')->get();
		foreach($partners as $partner){
			$mapped[$partner->id_is] = $partner->partner_id;
		}
		return $mapped;
	}
	public static function getMappedBills(){
		$mapped = array();
		$bills = DB::table('racun')->select('broj_dokumenta','racun_id')->whereNotNull('broj_dokumenta')->get();
		foreach($bills as $bill){
			$mapped[strval($bill->broj_dokumenta)] = $bill->racun_id;
		}
		return $mapped;
	}
	public static function getMappedSellers(){
		$mapped = array();
		$sellers = DB::table('imenik')->select('imenik_id','ime','prezime')->whereNotNull('ime')->whereNotNull('prezime')->where('kvota',32.00)->get();
		foreach($sellers as $seller){
			$mapped[strval(($seller->ime.' '.$seller->prezime))] = $seller->imenik_id;
		}
		return $mapped;
	}

	public static function getMappedManufacturers(){
		$mapped = array();
		$manufacturers = DB::table('proizvodjac')->whereNotNull('id_is')->where('id_is','!=','')->get();
		foreach($manufacturers as $manufacturer){
			$mapped[trim(strval($manufacturer->id_is))] = $manufacturer->proizvodjac_id;
		}
		return $mapped;
	}
	public static function uniqueManufacturers($articles){
		$uniqueManufacturers = [];
		foreach($articles as $article){
			if(isset($article->BRAND) && !empty($article->BRAND)){
				$manufacturer = trim(Support::convert($article->BRAND));
				$uniqueManufacturers[$manufacturer] = $manufacturer;
			}
		}
		return $uniqueManufacturers;
	}
	
	public static function getMappedVats(){
		$mapped = array();
		$vats = DB::table('tarifna_grupa')->whereNotNull('porez')->get();
		foreach($vats as $vat){
			$mapped[intval($vat->porez)] = $vat->tarifna_grupa_id;
		}
		return $mapped;
	}
	public static function getMappedMeasures(){
		$mapped = array();
		$measures = DB::table('jedinica_mere')->whereNotNull('naziv')->where('naziv','!=','')->get();
		foreach($measures as $measure){
			$mapped[strval($measure->naziv)] = $measure->jedinica_mere_id;
		}
		return $mapped;
	}

	public static function convert($text){
	    $text = preg_replace("/[áàâãªä]/u","a",$text);
	    $text = preg_replace("/[ÁÀÂÃÄ]/u","A",$text);
	    $text = preg_replace("/[ÍÌÎÏ]/u","I",$text);
	    $text = preg_replace("/[íìîï]/u","i",$text);
	    $text = preg_replace("/[éèêë]/u","e",$text);
	    $text = preg_replace("/[ÉÈÊË]/u","E",$text);
	    $text = preg_replace("/[óòôõºö]/u","o",$text);
	    $text = preg_replace("/[ÓÒÔÕÖ]/u","O",$text);
	    $text = preg_replace("/[úùûü]/u","u",$text);
	    $text = preg_replace("/[ÚÙÛÜ]/u","U",$text);
	    $text = preg_replace("/[’‘‹›‚]/u","'",$text);
	    $text = preg_replace("/[“”«»„]/u",'"',$text);
	    $text = str_replace("–","-",$text);
	    $text = str_replace(" "," ",$text);
	    $text = str_replace("ç","c",$text);
	    $text = str_replace("Ç","C",$text);
	    $text = str_replace("ñ","n",$text);
	    $text = str_replace("Ñ","N",$text);
	 
	    //2) Translation CP1252. &ndash; => -
	    $trans = get_html_translation_table(HTML_ENTITIES); 
	    $trans[chr(130)] = '&sbquo;';    // Single Low-9 Quotation Mark 
	    $trans[chr(131)] = '&fnof;';    // Latin Small Letter F With Hook 
	    $trans[chr(132)] = '&bdquo;';    // Double Low-9 Quotation Mark 
	    $trans[chr(133)] = '&hellip;';    // Horizontal Ellipsis 
	    $trans[chr(134)] = '&dagger;';    // Dagger 
	    $trans[chr(135)] = '&Dagger;';    // Double Dagger 
	    $trans[chr(136)] = '&circ;';    // Modifier Letter Circumflex Accent 
	    $trans[chr(137)] = '&permil;';    // Per Mille Sign 
	    $trans[chr(138)] = '&Scaron;';    // Latin Capital Letter S With Caron 
	    $trans[chr(139)] = '&lsaquo;';    // Single Left-Pointing Angle Quotation Mark 
	    $trans[chr(140)] = '&OElig;';    // Latin Capital Ligature OE 
	    $trans[chr(145)] = '&lsquo;';    // Left Single Quotation Mark 
	    $trans[chr(146)] = '&rsquo;';    // Right Single Quotation Mark 
	    $trans[chr(147)] = '&ldquo;';    // Left Double Quotation Mark 
	    $trans[chr(148)] = '&rdquo;';    // Right Double Quotation Mark 
	    $trans[chr(149)] = '&bull;';    // Bullet 
	    $trans[chr(150)] = '&ndash;';    // En Dash 
	    $trans[chr(151)] = '&mdash;';    // Em Dash 
	    $trans[chr(152)] = '&tilde;';    // Small Tilde 
	    $trans[chr(153)] = '&trade;';    // Trade Mark Sign 
	    $trans[chr(154)] = '&scaron;';    // Latin Small Letter S With Caron 
	    $trans[chr(155)] = '&rsaquo;';    // Single Right-Pointing Angle Quotation Mark 
	    $trans[chr(156)] = '&oelig;';    // Latin Small Ligature OE 
	    $trans[chr(159)] = '&Yuml;';    // Latin Capital Letter Y With Diaeresis 
	    $trans['euro'] = '&euro;';    // euro currency symbol 
	    ksort($trans); 
	     
	    foreach ($trans as $k => $v) {
	        $text = str_replace($v, $k, $text);
	    }
	 
	    // 3) remove <p>, <br/> ...
	    $text = strip_tags($text); 
	     
	    // 4) &amp; => & &quot; => '
	    $text = html_entity_decode($text);
	     
	    // 5) remove Windows-1252 symbols like "TradeMark", "Euro"...
	    $text = preg_replace('/[^(\x20-\x7F)]*/','', $text); 
	     
	    $targets=array("\r\n","\n","\r","\t","''","'");
	    $results=array(" "," "," ","",'"','"');
	    $text = str_replace($targets,$results,$text);
	     
		return ($text);
	}

	public static function filteredGroups($articles){
		$groups = array();
		foreach($articles as $article) {
			if(isset($article->PODGRUPA) && $article->PODGRUPA != ''){
				$groups[$article->PODGRUPA] = $article->GRUPA;
			}
		}
		return $groups;
	}
	public static function saveGroups($groups){
		foreach($groups as $grupa => $nadgrupa) {
			if($grupa != $nadgrupa){
				$tree = array($grupa);
				self::parentGroups($groups,$nadgrupa,$tree);
				for ($i=(count($tree)-1);$i>=0;$i--) {
					self::saveSingleGroup($tree[$i],(isset($tree[$i+1]) ? $tree[$i+1] : null),true);
				}
			}
		}
	}
	public static function parentGroups($allgroups,$group,&$tree){
		if(!is_null($group) && $group != ''){
			$tree[] = $group;
			if(isset($allgroups[$group]) && $allgroups[$group] != ''){
				self::parentGroups($allgroups,$allgroups[$group],$tree);
			}
		}
	}
	public static function saveSingleGroup($group_name,$group_parent=null,$update_parent=false){
		$group_name = trim($group_name);
		$group = DB::table('grupa_pr')->where('grupa',$group_name)->first();

		$group_parent_id = 0;
		if(!is_null($group_parent) && $group_parent != ''){
			$group_parent_id = self::getGrupaId($group_parent);
		}

		if(is_null($group)){
			$grupa_pr_id = DB::select("SELECT MAX(grupa_pr_id) + 1 AS max FROM grupa_pr")[0]->max;
			if(is_null($grupa_pr_id)){
				$grupa_pr_id = 1;
			}
			DB::table('grupa_pr')->insert(array(
				'grupa_pr_id' => $grupa_pr_id,
				'grupa' => $group_name,
				'parrent_grupa_pr_id' => $group_parent_id,
				'sifra' => $grupa_pr_id
				));
		}else{
			$grupa_pr_id = $group->grupa_pr_id;
			if($update_parent){
				DB::table('grupa_pr')->where('grupa_pr_id',$grupa_pr_id)->update(array('parrent_grupa_pr_id' => $group_parent_id));				
			}
		}
		return $grupa_pr_id;
	}

	public static function DCgetCharsAndPicts($idIss,$mappedArticles){

		$robaIds = array_map(function($idIs) use ($mappedArticles) {
			if(isset($mappedArticles[$idIs])){
				return $mappedArticles[$idIs];
			}
		},$idIss);
		$dobavljac_cenovnik = DB::table('dobavljac_cenovnik')->select('roba_id','dobavljac_cenovnik_id','sifra_kod_dobavljaca','partner_id')->where('roba_id','<>',-1)->whereIn('roba_id',$robaIds)->get();

        foreach($dobavljac_cenovnik as $dc){
        	$roba_id = $dc->roba_id;
            $dobavljac_cenovnik_id = $dc->dobavljac_cenovnik_id;
            $sifra_kod_dobavljaca = $dc->sifra_kod_dobavljaca;
            $partner_id = $dc->partner_id;
            
            //opis
            DB::statement("UPDATE roba r SET web_karakteristike = dc.opis FROM dobavljac_cenovnik dc WHERE r.roba_id = ".$roba_id." AND dobavljac_cenovnik_id = ".$dobavljac_cenovnik_id."");

            //dobavljac-karakteristike
            DB::table('dobavljac_cenovnik_karakteristike')->where(array('roba_id'=>-1,'sifra_kod_dobavljaca'=>$sifra_kod_dobavljaca))->update(array('roba_id'=>$roba_id));

            //slike
            $slike_query_dob = DB::table('dobavljac_cenovnik_slike')->where(array('sifra_kod_dobavljaca'=>$sifra_kod_dobavljaca, 'partner_id'=>$partner_id ));
            $slike_count_roba = DB::table('web_slika')->where('roba_id',$roba_id)->count();

            $diff_count = $slike_query_dob->count() - $slike_count_roba;
            if($diff_count > 0){
                $slike = $slike_query_dob->orderBy('dobavljac_cenovnik_slike_id','desc')->limit($diff_count)->get();
                AdminImport::saveImages($slike,$roba_id);
            }
        }		
	}
	public static function postUpdates(){
		$date=date('Y-m-d');

		DB::statement("update roba r set stara_cena = web_cena where grupa_pr_id > 0 and flag_zakljucan = 'false' and akcija_flag_primeni = 0 and (select rabat from grupa_pr where grupa_pr_id = r.grupa_pr_id limit 1) > 0 and (select kolicina from lager where roba_id=r.roba_id and orgj_id=2 limit 1) > 0 and id_is is not null and id_is <> ''");
		DB::statement("update roba r set web_cena = web_cena*(1-(select rabat from grupa_pr where grupa_pr_id = r.grupa_pr_id limit 1)/100) where grupa_pr_id > 0 and flag_zakljucan = 'false' and (akcija_flag_primeni = 0 or datum_akcije_od >= '".$date."') and (select rabat from grupa_pr where grupa_pr_id = r.grupa_pr_id limit 1) > 0 and (select kolicina from lager where roba_id=r.roba_id and orgj_id=2 limit 1) > 0 and id_is is not null and id_is <> ''");
	}
	public static function getUniqueSuppliers($articles){
		$suppliers = array_map(function($article){
			$supplier = trim($article->DOBAVLJAC);
			if(!empty($supplier)){
				return $supplier;
			}
		},$articles);
		return array_unique($suppliers);
	}

	public static function getDobavljacId($supplier){
		switch ($supplier) {
			case 'COMTRADE DISTRIBUTION D.O.O.':
				return 16;
				break;
			case 'KIM-TEC D.O.O.':
				return 17;
				break;
			case 'VELTEH PRO DOO':
				return 18;
				break;
			case 'GAMA GROUP DOO':
				return 19;
				break;	
			case 'EWE COMP D.O.O.':
				return 23;
				break;	
			case 'ERG D.O.O VOX ELECTRONICS':
				return 24;
				break;		
			default:
				return -1;
				break;
		}
	}

	public static function updatePmpPriceEponuda() {
		
	    $products_file = 'files/exporti/eponuda/eponuda_pmp_cene.csv';
		file_put_contents($products_file, file_get_contents('https://editor-api.eponuda.com/api/shoptok/reports/download?guid=331a5ca2-7bdc-444f-83ce-962e5fda04a4',false, stream_context_create(array(
	                "ssl"=>array(
	                    "verify_peer"=>false,
	                    "verify_peer_name"=>false,
	            )))));

	    $handle = fopen($products_file, "r");
	    $i = 0;
	    $cene_array = array();
	    while (($data = fgetcsv($handle,10000,",")) !== FALSE) {
	        if($i > 0) {
	            $datum = $data[0];
	            $roba_id = $data[1];
	            $cena = $data[2];
	            $cene_array[$roba_id] = $cena;
	        }
	        $i = $i + 1;
	    }
	    foreach ($cene_array as $key => $row_cena) {
	        $row = DB::table('roba')->where(array('roba_id'=>$key))->first();
	        if($row && !empty($row->sifra_is) && DB::table('roba_export')->where(array('roba_id'=>$row->roba_id,'export_id'=>7))->first()) {
	            if ($row->akcija_flag_primeni == 1 && (is_null($row->datum_akcije_od) || $row->datum_akcije_od <= date('Y-m-d')) && (is_null($row->datum_akcije_do) || $row->datum_akcije_do >= date('Y-m-d')) ) {
	                if($row->racunska_cena_end > 1000 && $row->racunska_cena_end*1.2*1.05 < $row_cena && $row_cena < $row->web_cena && $row_cena > 0) {
	                    DB::statement("UPDATE roba set akcijska_cena = ".$row_cena."-1 where roba_id = ".$row->roba_id."");
	                }
	            } else if($row->racunska_cena_end > 1000 && $row->racunska_cena_end*1.2*1.05 < $row_cena  && $row_cena > 0) {
	                   DB::statement("UPDATE roba set web_cena = ".$row_cena."-1 where roba_id = ".$row->roba_id."");
	            }
	        }
	    }
	}

}