<?php
namespace ISCalculus;
use SimpleXMLElement;
use AdminB2BOptions;

class CalculusAPI {
	public static function groups(){
		$url = AdminB2BOptions::info_sys('calculus')->api_url.'/CalculusWebService.asmx/GrupaArtUsl';

		$post_array = array(
			'artusl'=>'a',
			'klasifikacija'=>'',
			'sifra'=>'',
			'naziv'=>'',
			'nivo'=>''
			);
	    return self::postCurl($url,$post_array);
	}

	public static function articles(){
		$url = AdminB2BOptions::info_sys('calculus')->api_url.'/CalculusWebService.asmx/PodaciArtikla';

		$post_array = array(
			'grupa'=>'',
			'sifra'=>'',
			'naziv'=>'',
			'barkod'=>'',
			'serbr'=>'',
			'nazivsvojstva'=>'',
			'vredsvoj'=>'',
			'sort'=>'',
			'uslov'=>''
			);
		return self::postCurl($url,$post_array);		
	}

	public static function stock($sifra_magacin='VP'){
		//PodaciOrgJed
		$url = AdminB2BOptions::info_sys('calculus')->api_url.'/CalculusWebService.asmx/StanjeArtikla';

		$post_array = array(
			'sifmag'=>$sifra_magacin,
			'tipcen'=>'',
			'valcen'=>'',
			'ojcen'=>'',
			'sifgrart'=>'',
			'sifart'=>'',
			'nazivart'=>'',
			'zr'=>'',
			'sort'=>'',
			'uslov'=>'',
			'stanje0'=>''
			);
	    return self::postCurl($url,$post_array);
	}

	public static function prices($tip_cene=1,$sifra_artikla=''){
		//TipoviCenovnika
		$url = AdminB2BOptions::info_sys('calculus')->api_url.'/CalculusWebService.asmx/CenovnikArtUsl';
		$post_array = array(
			'tipcen'=>$tip_cene,
			'valcen'=>'',
			'ojcen'=>'',
			'sifgrart'=>'',
			'sifart'=>strval($sifra_artikla),
			'nazivart'=>''
			);
	    return self::postCurl($url,$post_array);	
	}

	public static function partners(){
		$url = AdminB2BOptions::info_sys('calculus')->api_url.'/CalculusWebService.asmx/PodaciKomitenta';
		$post_array = array(
			'sifra'=>'',
			'naziv'=>'',
			'pib'=>'',
			'ID'=>''
			);
		return self::postCurl($url,$post_array);
	}

	public static function postCurl($url,$post_array){
	    $post_string = http_build_query($post_array);

	    $ch = curl_init();
	    curl_setopt($ch, CURLOPT_URL, $url);
	    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded','Content-Length: '.strlen($post_string)));
	    curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
	    $response = curl_exec($ch);
	    curl_close($ch);

		$xml = new SimpleXMLElement($response);
		$ns = $xml->getNamespaces(true);
		$xml->registerXPathNamespace('d', $ns['diffgr']);
		$result = $xml->xpath('//d:diffgram/NewDataSet/Table');

		return $result;
	}

}