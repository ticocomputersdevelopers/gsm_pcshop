<?php
ini_set('memory_limit', '-1');
ini_set('max_execution_time', 3600);

use IsGSM\FileData;
use IsGSM\Article;
use IsGSM\Stock;
use IsGSM\Support;


class AdminIsGSM {

    public static function execute(){

     try {
            //articles
            $articleDetails = FileData::articles();
            // $groups = Support::filteredGroups($articleDetails->articles);
            // Support::saveGroups($groups);

            foreach(Support::uniqueManufacturers($articleDetails->articles) as $manufacturer){
                Support::saveProizvodjac($manufacturer);
            }

            $resultArticle = Article::table_body($articleDetails->articles);
            $new_id_iss = Article::query_insert_update($resultArticle->body,array('sifra_d','dobavljac_id','racunska_cena_nc','racunska_cena_end','mpcena','web_cena','flag_aktivan'));
            Article::query_update_unexists($resultArticle->body);
            
            Support::linkedWidthDC();
            $mappedArticles = Support::getMappedArticles();

            //stock
            $resultStock = Stock::table_body($articleDetails->articles,$mappedArticles,2);
            Stock::query_insert_update($resultStock->body,array('kolicina'));

            Support::DCgetCharsAndPicts($new_id_iss,$mappedArticles);
            Support::postUpdates();
            Support::updatePmpPriceEponuda();

            AdminB2BIS::saveISLog('true');
            return (object) array('success'=>true);
        }catch (Exception $e){
            AdminB2BIS::saveISLog('false');
            AdminB2BIS::sendNotification(array(9,12,15,18),20,20);
            return (object) array('success'=>false,'message'=>$e->getMessage());
        }
    }



}