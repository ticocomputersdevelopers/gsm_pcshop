<?php
ini_set('memory_limit', '-1');
ini_set('max_execution_time', 3600);

use IsSbcs\FileData;
use IsSbcs\Article;
use IsSbcs\Stock;
use IsSbcs\Partner;
use IsSbcs\PartnerCard;
use IsSbcs\Support;



class AdminIsSbcs {

    public static function execute(){
        try {
            //partner
            $partners = FileData::partners();
            $resultPartner = Partner::table_body($partners);
            Partner::query_insert_update($resultPartner->body);

            //partner card
            $resultPartner = PartnerCard::table_body($partners);
            PartnerCard::query_insert_update($resultPartner->body);

            //articles
            $articles = FileData::articles();
            // $groups = Support::filteredGroups($articles);
            // Support::saveGroups($groups);

            $resultArticle = Article::table_body($articles);
            Article::query_insert_update($resultArticle->body,array('flag_prikazi_u_cenovniku','jedinica_mere_id','tarifna_grupa_id','racunska_cena_nc','web_cena','mpcena','web_marza'));
            Article::query_update_unexists($resultArticle->body);

            //b2b stock
            if(AdminB2BOptions::info_sys('sbcs')->b2b_magacin){
                $resultStock = Stock::table_body($articles,AdminB2BOptions::info_sys('sbcs')->b2b_magacin);
                Stock::query_insert_update($resultStock->body);
            }
            //b2c stock
            if(AdminB2BOptions::info_sys('sbcs')->b2c_magacin){
                $resultStock = Stock::table_body($articles,AdminB2BOptions::info_sys('sbcs')->b2c_magacin);
                Stock::query_insert_update($resultStock->body);
            }

            AdminB2BIS::saveISLog('true');
            return (object) array('success'=>true);
        }catch (Exception $e){
            AdminB2BIS::saveISLog('false');
            return (object) array('success'=>false,'message'=>$e->getMessage());
        }
    }



}