<?php
namespace Export;

use Export\Support;
use DB;
use AdminOptions;
use AdminArticles;
use AdminCommon;
use DOMDocument;
use Response;
use SimpleXMLElement;
use View;

class Idealno {

	public static function execute($export_id,$kind,$short=false){

		$export_products = DB::select("SELECT roba_id, web_cena, mpcena, naziv_web,sifra_is, web_flag_karakteristike,grupa_pr_id,mpcena,web_karakteristike, web_opis, (SELECT naziv FROM proizvodjac WHERE proizvodjac_id = roba.proizvodjac_id) AS proizvodjac, (SELECT grupa FROM grupa_pr WHERE grupa_pr_id = roba.grupa_pr_id) AS grupa, model, (SELECT ROUND(kolicina) FROM lager WHERE roba_id = roba.roba_id AND poslovna_godina_id = (SELECT poslovna_godina_id FROM poslovna_godina WHERE status=0) AND orgj_id = (SELECT orgj_id FROM imenik_magacin WHERE izabrani=1)) AS kolicina, akcija_flag_primeni, datum_akcije_od, datum_akcije_do, akcijska_cena, barkod, napomena, garancija, tezinski_faktor FROM roba WHERE roba_id IN (SELECT roba_id FROM roba_export WHERE export_id=".$export_id.") AND proizvodjac_id <> -1 AND flag_aktivan = 1 AND flag_prikazi_u_cenovniku = 1 AND web_cena > 0");

		if($kind=='xml'){
			return self::xml_exe($export_products);
		}else{
			echo '<h2>Dati format nije podržan!</h2>';
		}

	}

	public static function xml_exe($products){
		
		$xml = new DOMDocument("1.0","UTF-8");
		$root = $xml->createElement("CNJExport");
		$xml->appendChild($root);

		$barkodovi = array();
		foreach($products as $article){
			
			$video = DB::select("SELECT * FROM web_files f left join vrsta_fajla vf on vf.vrsta_fajla_id = f.vrsta_fajla_id WHERE roba_id = ".$article->roba_id." and ekstenzija = 'YOUTUBE' ORDER BY f.vrsta_fajla_id ASC");
			 $cena = !empty($article->akcijska_cena) && $article->akcija_flag_primeni==1 && (is_null($article->datum_akcije_od) || $article->datum_akcije_od <= date('Y-m-d')) && (is_null($article->datum_akcije_do) || $article->datum_akcije_do >= date('Y-m-d')) ? $article->akcijska_cena : $article->web_cena;
			
			$product   = $xml->createElement("Item");

		    Support::xml_node_cdata($xml,"ID",$article->roba_id,$product);
		    Support::xml_node_cdata($xml,"name",$article->naziv_web,$product);
		    Support::xml_node_cdata($xml,"description",$article->web_karakteristike,$product);
			Support::xml_node_cdata($xml,"specifications",str_replace("c&#769;", "ć", $article->web_opis ),$product);
		    Support::xml_node_cdata($xml,"link",Support::product_link_ceners($article->roba_id),$product);
		    Support::xml_node_cdata($xml,"mainImage",Support::major_image($article->roba_id),$product);			    	
			Support::xml_node_cdata($xml,"moreImages",self::slike($article->roba_id),$product);
			foreach($video as $row){
				Support::xml_node_cdata($xml,"videoUrl",$row->putanja_etaz,$product);
			}		    
		    Support::xml_node($xml,"price", $article->akcija_flag_primeni == 1 && (is_null($article->datum_akcije_od) || $article->datum_akcije_od <= date('Y-m-d')) && (is_null($article->datum_akcije_do) || $article->datum_akcije_do >= date('Y-m-d')) ? $article->akcijska_cena : $article->web_cena,$product);
			Support::xml_node($xml,"regularPrice", $article->mpcena > 0 && $article->mpcena > $article->web_cena ? $article->mpcena : $article->web_cena,$product);
			Support::xml_node($xml,"curCode",'RSD',$product);
		    Support::xml_node($xml,"stock",AdminArticles::kolicina(1,$article->roba_id,$article->kolicina,0) > 0 ? 'in stock' : 'out of stock',$product);
		    Support::xml_node_cdata($xml,"fileUnder",self::group_link($article->grupa_pr_id),$product);
		    Support::xml_node_cdata($xml,"brand",$article->proizvodjac,$product);

		    if ((strlen($article->barkod) == 12 || strlen($article->barkod) == 13) && is_numeric($article->barkod) && !in_array($article->barkod,$barkodovi)) {
				Support::xml_node_cdata($xml,"EAN",$article->barkod,$product);
				$barkodovi[] = $article->barkod;
		    } else {
		    	Support::xml_node_cdata($xml,"EAN","",$product);
		    }

			Support::xml_node_cdata($xml,"productCode",$article->sifra_is,$product);
			Support::xml_node_cdata($xml,"condition",'new',$product);
			Support::xml_node($xml,"deliveryCost",$cena <=2200 ? 199.00 : 0,$product);
			Support::xml_node($xml,"deliveryTimeMin",2,$product);
			Support::xml_node($xml,"deliveryTimeMax",10,$product);
		
			$root->appendChild($product);
		}

		$xml->formatOutput = true;
		$store_path = 'files/exporti/idealno/idealno.xml';
		$xml->save($store_path) or die("Error");

		// header('Content-type: text/xml');
		// //header('Content-Disposition: attachment; filename="CeneRS.xml"');
		// echo file_get_contents($store_path); die;	
		return $store_path;
	}
    public static function group_link($group_id,$lang=null)
	{	$slug = null;
		$grupa = DB::table('grupa_pr')->where('grupa_pr_id','>',0)->where(array('grupa_pr_id'=>$group_id,'web_b2c_prikazi'=>1))->first();
		if(!is_null($grupa)){
			$slug = AdminOptions::slug_trans($grupa->grupa,$lang);
			if($grupa->parrent_grupa_pr_id > 0 && $grupa->grupa_pr_id != $grupa->parrent_grupa_pr_id){
				$subSlug = self::group_link($grupa->parrent_grupa_pr_id,$lang);
				if(is_null($subSlug)){
					return null;
				}
				$slug = $subSlug.' - '.$slug;
			}
		}
		return $slug;
	}
	public static function slike($roba_id){
		$slike = DB::table('web_slika')->where('akcija',0)->where('roba_id',$roba_id)->limit(8)->orderBy('akcija','desc')->get();
		$img=[];
		foreach ($slike as $slika) {		    
		    $img[]=AdminOptions::base_url().$slika->putanja ;
		}
		return implode(',', $img);
	}
}