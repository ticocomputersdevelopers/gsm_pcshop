<?php
namespace Import;
use Import\Support;
use DB;
use AdminOptions;
use File;
use ZipArchive;

class NTCompany {

	public static function execute($dobavljac_id,$kurs=null,$extension=null){
		Support::initQueryExecute();

		if($extension==null){
			if(Support::autoLink($dobavljac_id) != null && Support::serviceUsername($dobavljac_id) != null && Support::servicePassword($dobavljac_id) != null){
				self::autoDownload(Support::autoLink($dobavljac_id),Support::serviceUsername($dobavljac_id),Support::servicePassword($dobavljac_id));
			}
			$file_name = Support::file_name("files/ntcompany/ntcompany_xml/");
			if($file_name!==false){
				$products = simplexml_load_file("files/ntcompany/ntcompany_xml/".$file_name);
			}else{
				$products = array();
			}
		}else{
			$file_name = AdminOptions::base_url().'files/import.'.$extension;
			$products = simplexml_load_file($file_name);	

		}

			foreach ($products as $product):
				$sPolja = '';
				$sVrednosti = '';

				$naziv=$product->name;
				$naziv=pg_escape_string($naziv);
				
				$model=$product->model;
				$model = pg_escape_string($model);
												
				$opis = $product->description;
				$opis = pg_escape_string($opis);

				$sifra=$product->attributes();

				$images = $product->image;
				//$images = str_replace(".rs", ".rs/b2b", $images);
				$flag_slika_postoji = "0";
				$i=0;
				foreach ($images as $slika):
				if($i==0){
					DB::statement("INSERT INTO dobavljac_cenovnik_slike_temp(partner_id,sifra_kod_dobavljaca,putanja,akcija)VALUES(".$dobavljac_id.",".Support::quotedStr($sifra).",'".Support::encodeTo1250(str_replace(".rs",".rs/b2b",$slika))."',1 )");
				}else{
					DB::statement("INSERT INTO dobavljac_cenovnik_slike_temp(partner_id,sifra_kod_dobavljaca,putanja,akcija)VALUES(".$dobavljac_id.",".Support::quotedStr($sifra).",'".Support::encodeTo1250(str_replace(".rs",".rs/b2b",$slika))."',0 )");
				}
				$flag_slika_postoji = "1";
				$i++;
			endforeach;	
				
				if($product->onStock=="true"){
					$kolicina=1.00;
				}else{
					$kolicina=0.00;
				}

				$sPolja .= " partner_id,";				$sVrednosti .= " " . $dobavljac_id . ",";
				$sPolja .= " sifra_kod_dobavljaca,";	$sVrednosti .= " '" . $sifra . "',";	
				$sPolja .= " naziv,";					$sVrednosti .= " '" . Support::encodeTo1250($naziv). "',";
				$sPolja .= " grupa,";					$sVrednosti .= " '" . Support::encodeTo1250($product->group) . "',";
				$sPolja .= " proizvodjac,";				$sVrednosti .= " '" . Support::encodeTo1250($product->producer) . "',";
				$sPolja .= " model,";					$sVrednosti .= " '" . Support::encodeTo1250($model) . "',";
				$sPolja .= " opis,";					$sVrednosti .= " '" . Support::encodeTo1250($opis) . "',";
				if(empty($opis)){
				$sPolja .= " flag_opis_postoji,";		$sVrednosti .= "" . 0 . ",";	
				}else{
				$sPolja .= " flag_opis_postoji,";		$sVrednosti .= "" . 1 . ",";	
				}
				$sPolja .= " cena_nc,";					$sVrednosti .= " " . Support::replace_empty_numeric(number_format((float)$product->priceA, 2, '.', ''),1,'',''). ",";
				$sPolja .= " pmp_cena,";				$sVrednosti .= " " . Support::replace_empty_numeric(number_format((float)$product->priceC, 2, '.', ''),1,'','') . ",";
				$sPolja .= " flag_slika_postoji,";		$sVrednosti .= "" . ($product->image == "" || $product->image == "http://b2b.nt.rs/res/basic/images/no_image.png" ? "0" : "1" ). ",";	
				$sPolja .= " kolicina";					$sVrednosti .= " " . $kolicina. "";
					
				DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");
				
				//DB::statement("INSERT INTO dobavljac_cenovnik_slike_temp (partner_id,sifra_kod_dobavljaca,putanja,akcija) VALUES(".$dobavljac_id.",'".$sifra."','".$image."',1 )");

			
			endforeach;



			Support::queryExecute($dobavljac_id,array('i','u'),array('i'),array('i','u'));
			
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($file_name)){
                    File::delete($file_name);
                }				
			}
		}



public static function executeShort($dobavljac_id,$kurs=null,$extension=null){
		Support::initQueryExecute();

		if($extension==null){
			if(Support::autoLink($dobavljac_id) != null && Support::serviceUsername($dobavljac_id) != null && Support::servicePassword($dobavljac_id) != null){
				self::autoDownload(Support::autoLink($dobavljac_id),Support::serviceUsername($dobavljac_id),Support::servicePassword($dobavljac_id));
			}
			$file_name = Support::file_name("files/ntcompany/ntcompany_xml/");
			if($file_name!==false){
				$products = simplexml_load_file("files/ntcompany/ntcompany_xml/".$file_name);
			}else{
				$products = array();
			}
		}else{
			$file_name = AdminOptions::base_url().'files/import.'.$extension;
			$products = simplexml_load_file($file_name);			
		}

			foreach ($products as $product):
				$sPolja = '';
				$sVrednosti = '';

				$naziv=$product->name;
				$naziv=pg_escape_string($naziv);
																				
				$sifra=$product->attributes();

				if($product->onStock=="true"){
					$kolicina=1.00;
				}else{
					$kolicina=0.00;
				}	


				$sPolja .= " partner_id,";				$sVrednosti .= " " . $dobavljac_id . ",";
				$sPolja .= " sifra_kod_dobavljaca,";	$sVrednosti .= " '" . addslashes($sifra) . "',";	
				$sPolja .= " naziv,";					$sVrednosti .= " '" . Support::encodeTo1250($naziv). "',";
				$sPolja .= " cena_nc,";					$sVrednosti .= " " . Support::replace_empty_numeric(number_format((float)$product->priceA, 2, '.', ''),1,'',''). ",";
				$sPolja .= " pmp_cena,";				$sVrednosti .= " " . Support::replace_empty_numeric(number_format((float)$product->priceC, 2, '.', ''),1,'','') . ",";
				$sPolja .= " kolicina";					$sVrednosti .= " " . $kolicina . "";
					
				DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");
											
			endforeach;

			Support::queryExecute($dobavljac_id,array('i','u'),array('i'),array('i','u'));
			
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($file_name)){
                    File::delete($file_name);
                }				
			}
		}



public static function autoDownload($links,$username,$password){

		$links = explode('---',$links);
		$postinfo = "j_username=".$username."&j_password=".$password."";
		$cookie_file_path = 'cookie.txt';

		$myfile = fopen("files/ntcompany/ntcompany_xml/ntcompany.zip", "w");

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_NOBODY, false);
		curl_setopt($ch, CURLOPT_URL, $links[0]);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);

		curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie_file_path);
		curl_setopt($ch, CURLOPT_COOKIE, "cookiename=0");
		curl_setopt($ch, CURLOPT_USERAGENT,
		    "Mozilla/5.0 (Windows; U; Windows NT 5.0; en-US; rv:1.7.12) Gecko/20050915 Firefox/1.0.7");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_REFERER, $_SERVER['REQUEST_URI']);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);

		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postinfo);
		curl_exec($ch);

		//dovlacenje xml-a
		curl_setopt($ch, CURLOPT_URL, $links[1]);
		curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
		curl_setopt($ch, CURLOPT_VERBOSE, 1);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
		curl_setopt($ch, CURLOPT_FILE, $myfile);
		curl_exec ($ch);
		curl_close($ch);

		$zip = new ZipArchive;
		if ($zip->open('files/ntcompany/ntcompany_xml/ntcompany.zip') === TRUE) {
		    $zip->extractTo('files/ntcompany/ntcompany_xml');
		    $zip->close();
		    File::delete('files/ntcompany/ntcompany_xml/ntcompany.zip');
		}
	
}
}