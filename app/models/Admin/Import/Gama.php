<?php
namespace Import;
use Import\Support;
use DB;
use File;

class Gama {

	public static function execute($dobavljac_id,$kurs=null,$extension=null){

		if($extension==null){
			Support::autoDownload(Support::autoLink($dobavljac_id),'files/gama/gama_xml/gama.xml');
			$products_file = "files/gama/gama_xml/gama.xml";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;
		}
		
		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}

			$products = simplexml_load_file($products_file);
	        
			foreach ($products as $product):
				$kolicina=isset($product->stock) ? $product->stock : '0' ;

				$sPolja = '';
				$sVrednosti = '';
				$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
				$sPolja .= "sifra_kod_dobavljaca,";		$sVrednosti .= "'" . $product->ident . "',";
				$sPolja .= "naziv,";					$sVrednosti .= "'" . addslashes(Support::encodeTo1250(trim($product->naziv))) ." ( ". $product->ident ." )"."',";
				$sPolja .= "grupa,";					$sVrednosti .= "'" . addslashes(Support::encodeTo1250(trim($product->kategorija))) . "',";
				$sPolja .= "proizvodjac,";				$sVrednosti .= "'" . addslashes(Support::encodeTo1250(trim($product->Brend))) . "',";
				$sPolja .= "kolicina,";					$sVrednosti .= " " . number_format(floatval($kolicina), 2, '.', '') . ",";
				$sPolja .= "barkod,";					$sVrednosti .=  "'" . $product->ean . "',";
				//$sPolja .= "web_flag_karakteristike,";	$sVrednosti .= " 0,";
				//$sPolja .= "flag_opis_postoji,";		$sVrednosti .= " 1,";
				$sPolja .= "flag_slika_postoji,";		$sVrednosti .= " 1,";
				//$sPolja .= "pdv,";						$sVrednosti .= "" . $product->stopa . ",";							
				$sPolja .= "cena_nc,";					$sVrednosti .= "" . $product->price . ",";
				//$sPolja .= "web_cena,";					$sVrednosti .= "" . $product->pmpc . ",";
				//$sPolja .= "mpcena,";					$sVrednosti .= "" . $product->pmpc . ",";
				$sPolja .= "pmp_cena";					$sVrednosti .= "" . $product->pmpc . "";	
		
				DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");	

				DB::statement("INSERT INTO dobavljac_cenovnik_slike_temp(partner_id,sifra_kod_dobavljaca,putanja,akcija)VALUES(".$dobavljac_id.",'".$product->ident."','".addslashes(Support::encodeTo1250($product->img))."',1 )");

			endforeach;

			Support::queryExecute($dobavljac_id,array('i','u'),array('i'),array());
			
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}

	}

	public static function executeShort($dobavljac_id,$extension=null){
		if($extension==null){
			Support::autoDownload(Support::autoLink($dobavljac_id),'files/gama/gama_xml/gama.xml');
			$products_file = "files/gama/gama_xml/gama.xml";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;			
		}

		if($continue){
			Support::initQueryExecute();

		$products = simplexml_load_file($products_file);
	        
			foreach ($products as $product):
				$kolicina=isset($product->stock) ? $product->stock : '0' ;
				
				$sPolja = '';
				$sVrednosti = '';
				$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
				$sPolja .= "sifra_kod_dobavljaca,";		$sVrednosti .= "'" . $product->ident . "',";
				$sPolja .= "cena_nc,";					$sVrednosti .= "" . $product->price . ",";
				$sPolja .= "kolicina,";					$sVrednosti .= " " . number_format(floatval($kolicina), 2, '.', '') . ",";
				$sPolja .= "pmp_cena";					$sVrednosti .= "" . $product->pmpc . "";

		
				DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");

			endforeach;

			//Support::queryShortExecute($dobavljac_id);
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}
	}


}