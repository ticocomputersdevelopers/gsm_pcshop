<?php
namespace Import;
use Import\Support;
use DB;
use AdminOptions;
use File;

class B220 {

	public static function execute($dobavljac_id,$kurs=null,$extension=null){
		Support::initQueryExecute();
		
		if($extension==null){

			Support::autoDownload(Support::autoLink($dobavljac_id),'files/b220/b220_xml/b220.xml');
			$products_file = "files/b220/b220_xml/b220.xml";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;
		}
		$products = simplexml_load_file($products_file);

		foreach ($products as $product) {
			$kolicina = (int) $product->kolicina;
			if($kolicina > 0) {
				//karakteristike
				$karakteristike = $product->karakteristike->xpath('karakteristike_grupa/karakteristika');
				foreach ($karakteristike as $karakteristika) {
					$karakteristika_naziv = (string) $karakteristika->attributes()->ime;
					$karakteristika_vrednost = (string) $karakteristika->vrednost;

					$sPolja = '';
					$sVrednosti = '';
					$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
					$sPolja .= "sifra_kod_dobavljaca,";		$sVrednosti .= "" . Support::quotedStr(Support::encodeTo1250($product->sifra)) . ",";	
					$sPolja .= "karakteristika_naziv,";		$sVrednosti .= "" . Support::quotedStr(pg_escape_string(trim(Support::encodeTo1250(($karakteristika_naziv))))) . ",";
					$sPolja .= "karakteristika_vrednost";	$sVrednosti .= "'" . trim(Support::encodeTo1250(pg_escape_string($karakteristika_vrednost))) . "'";
						
					DB::statement("INSERT INTO dobavljac_cenovnik_karakteristike_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");
				}
				
				//slike
				$images = $product->xpath('slike/slika');
				$flag_slika_postoji = "0";
				$i=0;
				foreach ($images as $slika){
					if($i==0){
						DB::statement("INSERT INTO dobavljac_cenovnik_slike_temp(partner_id,sifra_kod_dobavljaca,putanja,akcija)VALUES(".$dobavljac_id.",".Support::quotedStr($product->sifra).",'".Support::encodeTo1250($slika)."',1 )");
					}else{
						DB::statement("INSERT INTO dobavljac_cenovnik_slike_temp(partner_id,sifra_kod_dobavljaca,putanja,akcija)VALUES(".$dobavljac_id.",".Support::quotedStr($product->sifra).",'".Support::encodeTo1250($slika)."',0 )");
					}
					$flag_slika_postoji = "1";
					$i++;
				}	
				$opis=$product->opis;
				$opis = preg_replace("/[\n\r]/","",$opis);
				//proizvodi
				$sPolja = '';
				$sVrednosti = '';
				$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
				$sPolja .= "sifra_kod_dobavljaca,";		$sVrednosti .= "" . Support::quotedStr(Support::encodeTo1250( $product->sifra)) . ",";
				//var_dump($product->naziv);die;
				$sPolja .= "naziv,";					$sVrednosti .= "" . Support::quotedStr(Support::encodeTo1250($product->naziv)) . ",";
				$sPolja .= "grupa,";					$sVrednosti .= "" . Support::quotedStr(Support::encodeTo1250($product->nadgrupa)) . ",";		
				$sPolja .= "podgrupa,";					$sVrednosti .= "" . Support::quotedStr(Support::encodeTo1250($product->grupa)) . ",";			
				$sPolja .= "opis,";						$sVrednosti .= "'" . pg_escape_string(Support::encodeTo1250($opis)). "',";
				if(empty($product->opis)){
				$sPolja .= "flag_opis_postoji,"; 		$sVrednosti .= " 0,";	
				}else{
				$sPolja .= "flag_opis_postoji,"; 		$sVrednosti .= " 1,";	
				}
				$sPolja .= "web_flag_karakteristike,";	$sVrednosti .= " 0,";
				if($product->kolicina != '10+'){
				$sPolja .= "kolicina,";					$sVrednosti .= "" . $product->kolicina . ",";
				}else{
				$sPolja .= "kolicina,";					$sVrednosti .= " 10,";
				}
				$sPolja .= "proizvodjac,";				$sVrednosti .= "" . Support::quotedStr(Support::encodeTo1250($product->proizvodjac)) . ",";			
				$sPolja .= "cena_nc,";					$sVrednosti .= "" . number_format(Support::replace_empty_numeric($product->cena,1,1,2),2, '.', '') . ",";
				
				// $sPolja .= "mpcena,";					$sVrednosti .= "" . number_format(Support::replace_empty_numeric($product->mpcena,1,1,2),2, '.', '') . ",";	
				$sPolja .= "pmp_cena,";					$sVrednosti .= "" . number_format(Support::replace_empty_numeric($product->mpcena,1,1,2),2, '.', '') . ",";	

				$sPolja .= "pdv,";						$sVrednosti .= "" . $product->pdv.",";
				$sPolja .= "barkod,";					$sVrednosti .= "" .  Support::quotedStr($product->barkod).",";

				$sPolja .= "flag_slika_postoji";		$sVrednosti .= "" . $flag_slika_postoji."";
								
				DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");
			}
		}


		Support::queryExecute($dobavljac_id,array('i','u'),array('i'),array('i','u'));
		
		
		//Brisemo fajl
		if($extension!=null){
			File::delete('files/import.'.$extension);
		}else{
           if(File::exists($products_file)){
            	File::delete($products_file);
            }				
		}
	
	}

	public static function executeShort($dobavljac_id,$kurs=null,$extension=null){
		Support::initQueryExecute();
		
		if($extension==null){

			Support::autoDownload(Support::autoLink($dobavljac_id),'files/b220/b220_xml/b220.xml');
			$products_file = "files/b220/b220_xml/b220.xml";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;
		}

		$products = simplexml_load_file($products_file);

		foreach ($products as $product) {
			$kolicina = (int) $product->kolicina;
			if($kolicina > 0) {
				$sPolja = '';
				$sVrednosti = '';
				$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
				$sPolja .= "sifra_kod_dobavljaca,";		$sVrednosti .= "" . Support::quotedStr(Support::encodeTo1250( $product->sifra)) . ",";
				if($product->kolicina != '10+'){
				$sPolja .= "kolicina,";					$sVrednosti .= "" . $product->kolicina . ",";
				}else{
				$sPolja .= "kolicina,";					$sVrednosti .= " 10,";
				}
				$sPolja .= "cena_nc,";					$sVrednosti .= "" . number_format(Support::replace_empty_numeric($product->cena,1,1,2),2, '.', '') . ",";
				$sPolja .= "pmp_cena";					$sVrednosti .= "" . number_format(Support::replace_empty_numeric($product->mpcena,1,1,2),2, '.', '') . "";	

				// $sPolja .= "mpcena";					$sVrednosti .= "" . number_format(Support::replace_empty_numeric($product->mpcena,1,1,2),2, '.', '') . "";	


								
				DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");						
			}
		}
		
		//Support::queryShortExecute($dobavljac_id);
		//Brisemo fajl
		if($extension!=null){
			File::delete('files/import.'.$extension);
		}else{
           if(File::exists($products_file)){
            	File::delete($products_file);
            }				
		}
	
	}

}