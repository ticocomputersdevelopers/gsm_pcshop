<?php
namespace Import;
use Import\Support;
use SimpleXMLElement;
use DB;
use File;
use DOMDocument;

class ERG {

	public static function execute($dobavljac_id,$kurs=null,$extension=null){

		if($extension==null){
			Support::autoDownload(Support::autoLink($dobavljac_id),'files/erg/erg_xml/erg.xml');
			$products_file = "files/erg/erg_xml/erg.xml";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;
		}
		
		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}

			$products = simplexml_load_file($products_file);	

	   
	        foreach ($products as $product):
	       	$opis = $product->xpath('item/attribute_name');
	       
			$redni_br_grupe=0;
	       	foreach($opis->children() as $opis_line){
	       		
			$redni_br_grupe++;
				// $parent = $opis_line->xpath("parent::*");
				$parent_group=$opis_line->attributes()->name;
				//var_dump($parent_group);die;
				foreach ($opis_line->children() as $karakteristika){
					var_dump($karakteristika);die;
				    $novaVrednost="";
					foreach ($karakteristika->children() as $deca) {
						$novaVrednost.=$deca;
					}
					
					DB::statement("INSERT INTO dobavljac_cenovnik_karakteristike_temp(partner_id,sifra_kod_dobavljaca,karakteristika_naziv,karakteristika_vrednost,karakteristika_grupa,redni_br_grupe)"
					."VALUES(".$dobavljac_id."," . Support::quotedStr($product->id) . ",".Support::quotedStr(Support::encodeTo1250($karakteristika['name'])).","
					." ".Support::quotedStr(Support::encodeTo1250($novaVrednost)) .",".Support::quotedStr(Support::encodeTo1250($parent_group)).",".$redni_br_grupe.")");
					// $flag_opis_postoji = "1";

				}
			}



	  //      	// var_dump($opis);die;
			// foreach ($opis->children() as $child) {
			// echo $child->getName() . ": " . $child . "<br />";
			// }

	    	$flag_opis_postoji = "0";
			foreach($opis as $opis_line){
				DB::statement("INSERT INTO dobavljac_cenovnik_karakteristike_temp(partner_id,sifra_kod_dobavljaca,karakteristika_naziv,karakteristika_vrednost)"
				."VALUES(".$dobavljac_id."," . Support::quotedStr($product->ProductCode) . ",".Support::quotedStr(Support::encodeTo1250($opis_line->attributes()->Name)).","
				." ".Support::quotedStr(Support::encodeTo1250($opis_line->attributes()->Value)).") ");
				$flag_opis_postoji = "1";
			}

			$images = $product->xpath('Images');
			$flag_slika_postoji = "0";
			$i = 0;
			foreach ($images as $slika):
				if($i==0){
					$sqlSlikaInsert = "INSERT INTO dobavljac_cenovnik_slike_temp(partner_id,sifra_kod_dobavljaca,putanja,akcija)VALUES(".$dobavljac_id.",".Support::quotedStr($product->ProductCode).",'".$slika."',1 )";
				}else{
					$sqlSlikaInsert = "INSERT INTO dobavljac_cenovnik_slike_temp(partner_id,sifra_kod_dobavljaca,putanja,akcija)VALUES(".$dobavljac_id.",".Support::quotedStr($product->ProductCode).",'".$slika."',0 )";
				}
				DB::statement($sqlSlikaInsert);
				$i++;
				$flag_slika_postoji = "1";
			endforeach;	

				$sPolja = '';
				$sVrednosti = '';

				$naziv=$product->name;		
				$grupa=$product->category;		
				$podgrupa = $product->category2;		
				$kolicina=$product->stock;	
				$sifra=$product->id;
				$cena = $product->price;			

				$sPolja .= " partner_id,";				$sVrednosti .= " " . $dobavljac_id . ",";
				$sPolja .= " sifra_kod_dobavljaca,";	$sVrednosti .= " '" . addslashes($sifra) . "',";	
				$sPolja .= " naziv,";					$sVrednosti .= " '" . Support::encodeTo1250($naziv). "',";
				$sPolja .= " grupa,";					$sVrednosti .= " '" . addslashes(Support::encodeTo1250($grupa)) . "',";
				$sPolja .= " podgrupa,";				$sVrednosti .= " '" . addslashes(Support::encodeTo1250($podgrupa)) . "',";
				$sPolja .= " cena_nc,";					$sVrednosti .= " " . Support::replace_empty_numeric(number_format((float)$cena, 2, '.', ''),1,'',''). ",";
				if($kolicina=='>10'){
				$sPolja .= " kolicina";					$sVrednosti .= " 10";
				}else{
				$sPolja .= " kolicina";					$sVrednosti .= " " . $kolicina . "";	
				}
				DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");

			endforeach;

			Support::queryExecute($dobavljac_id,array('i','u'),array('i'),array());
			
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}

	}

	public static function executeShort($dobavljac_id,$extension=null){
		if($extension==null){
			Support::autoDownload(Support::autoLink($dobavljac_id),'files/erg/erg_xml/erg.xml');
			$products_file = "files/erg/erg_xml/erg.xml";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;			
		}

		if($continue){
			Support::initQueryExecute();
			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}

			}
		$products = simplexml_load_file($products_file);	
			foreach ($products as $product):

				$sPolja = '';
				$sVrednosti = '';

				$naziv=$product->ARTIKAL;		
				$grupa=$product->KATEGORIJA;		
				$podgrupa = $product->PODKATEGORIJA;		
				$kolicina=$product->KOLICINA;	
				$sifra=$product->SIFRA;
				$cena = $product->CENA;

				$cena= $cena*$kurs;

				$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
				$sPolja .= "sifra_kod_dobavljaca,";		$sVrednosti .= "'" . $sifra . "',";
				$sPolja .= "cena_nc,";					$sVrednosti .= "" . $cena . ",";
				$sPolja .= "kolicina";					$sVrednosti .= "" .$kolicina. "";
						
				DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");

			endforeach;

			//Support::queryShortExecute($dobavljac_id);
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}
	


}