<?php
namespace Import;
use Import\Support;
use DB;
use File;
use PHPExcel; 
use Net_SFTP; 
use PHPExcel_IOFactory;

class Bosh {

	public static function ftpConnection() {

	$sftp = new Net_SFTP('edi.bsh-partner.com');

    $sftp->login('ftp_SRBGSM', 'qt.R|V^yMw');
    if(!$sftp){
    	echo "Greska";
    }
	$sftp->get('BSHOUT/bosch.xml','files/bosh/bosh_xml/bosch.xml');
	
    }
	public static function execute($dobavljac_id,$kurs=null,$extension=null){
		
		$extension = 'xml';
		if($extension==null){
			$products_file = "files/bosh/bosh_xml/bosch.xml";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			self::ftpConnection();
			$continue = true;
			$products_file = "files/bosh/bosh_xml/bosch.xml";
		}
		
		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}

			$products = simplexml_load_file($products_file);	        
			foreach ($products as $product):
				if(!empty($product->ean)){
					$proizvodjac = "Bosch";
					$sPolja = '';
					$sVrednosti = '';
					
					// if($product->sifra =='WTH83002BY'){
					// 	var_dump($product->ppc);die;
					// }

					$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
					$sPolja .= "sifra_kod_dobavljaca,";		$sVrednosti .= "'" . addslashes(Support::encodeTo1250($product->sifra)) . "',";
					$sPolja .= "barkod,";					$sVrednosti .= "'" . addslashes(Support::encodeTo1250($product->ean)) . "',";
					$sPolja .= "naziv,";					$sVrednosti .= "'" ."Bosch " . addslashes(Support::encodeTo1250( $product->naziv) ." ( ".  Support::encodeTo1250($product->sifra ) . " )") . "',";					
					$sPolja .= "kolicina,";					$sVrednosti .= "'" . $product->zaloga . "',";
					$sPolja .= "proizvodjac,";				$sVrednosti .= "'" . $proizvodjac . "',";
					$sPolja .= "pmp_cena,";					$sVrednosti .= "" . number_format(floatval(Support::replace_empty_numeric($product->ppc,1,$kurs,$valuta_id_nc)), 2, '.', '') . ",";
					$sPolja .= "web_cena,";					$sVrednosti .= "" . number_format(floatval(Support::replace_empty_numeric($product->ppc,1,$kurs,$valuta_id_nc)), 2, '.', '') . ",";
					$sPolja .= "cena_nc";					$sVrednosti .= "" . number_format(floatval(Support::replace_empty_numeric($product->cena,1,$kurs,$valuta_id_nc)), 2, '.', '') . "";
						
					DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");
				}

			endforeach;

			Support::queryExecute($dobavljac_id,array('i','u'),array(),array());
			
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}

	}

	public static function executeShort($dobavljac_id,$kurs=null,$extension=null){

		$extension = 'xml';
		if($extension==null){
			$products_file = "files/bosh/bosh_xml/bosch.xml";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			self::ftpConnection();
			$continue = true;
			$products_file = "files/bosh/bosh_xml/bosch.xml";
		}
		
		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}

			$products = simplexml_load_file($products_file);	        
			foreach ($products as $product):
				if(!empty($product->ean)){
					
					$sPolja = '';
					$sVrednosti = '';
				
					$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
					$sPolja .= "sifra_kod_dobavljaca,";		$sVrednosti .= "'" . addslashes(Support::encodeTo1250($product->sifra)) . "',";					
					$sPolja .= "kolicina,";					$sVrednosti .= "'" . $product->zaloga . "',";
					$sPolja .= "pmp_cena,";					$sVrednosti .= "" . number_format(floatval(Support::replace_empty_numeric($product->ppc,1,$kurs,$valuta_id_nc)), 2, '.', '') . ",";
					$sPolja .= "web_cena,";					$sVrednosti .= "" . number_format(floatval(Support::replace_empty_numeric($product->ppc,1,$kurs,$valuta_id_nc)), 2, '.', '') . ",";
					$sPolja .= "cena_nc";					$sVrednosti .= "" . number_format(floatval(Support::replace_empty_numeric($product->cena,1,$kurs,$valuta_id_nc)), 2, '.', '') . "";
						
					DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");
				}

			endforeach;

			//Support::queryShortExecute($dobavljac_id);
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}
	}

}