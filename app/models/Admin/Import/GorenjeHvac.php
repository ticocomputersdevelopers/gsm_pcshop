<?php
namespace Import;
use Import\Support;
use DB;
use File;
use PHPExcel; 
use PHPExcel_IOFactory;

class GorenjeHvac {

	public static function execute($dobavljac_id,$kurs=null,$extension=null){
		if($extension==null){
			$products_file = "files/gorenjehvac/gorenjehvac_excel/gorenjehvac.xlsx";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;			
		}

		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}

	        $excelReader = PHPExcel_IOFactory::createReaderForFile($products_file);
	        $excelObj = $excelReader->load($products_file);
	        $worksheet = $excelObj->getSheet(0);
	        $lastRow = $worksheet->getHighestRow();
	        
            $begin = false;
            $podgrupa = 'NULL';

	        for ($row = 1; $row <= $lastRow; $row++) {
	            $model = $worksheet->getCell('A'.$row)->getValue();
	            $sifra = $worksheet->getCell('B'.$row)->getValue();
	            $naziv = $worksheet->getCell('C'.$row)->getValue();
				$cena_nc = $worksheet->getCell('H'.$row)->getFormattedValue();
	            $mpcena = $worksheet->getCell('I'.$row)->getFormattedValue();
	            $web_cena = $worksheet->getCell('J'.$row)->getFormattedValue();
	            $web_cena = str_replace(",", "", $web_cena);
	            $mpcena = str_replace(",", "", $mpcena);

	            if($row!=1){
		            if($worksheet->getCell('A'.($row-1))->getValue()=='Model'){
		            	$begin = true;
		            }
	            }
	            if($begin){
					if(isset($model) && isset($sifra) && isset($naziv) && isset($mpcena) && is_numeric($mpcena) ){


						$sPolja = '';
						$sVrednosti = '';
						$sPolja .= " partner_id,";				$sVrednosti .= " " . $dobavljac_id . ",";
						$sPolja .= " sifra_kod_dobavljaca,";    $sVrednosti .= " '" . addslashes(Support::encodeTo1250(trim($sifra))) . "',";
						$sPolja .= " kolicina,";				$sVrednosti .= " " . number_format(1.00, 2, '.', '') . ",";
						$sPolja .= "tarifna_grupa_id,";			$sVrednosti .= " 0, ";
						$sPolja .= "pdv,";						$sVrednosti .= " 20, ";
						$sPolja .= " naziv,";					$sVrednosti .= " '" . pg_escape_string(ucfirst(Support::charset_decode_utf_8(mb_strtolower($model)))). "',";
						//$sPolja .= " podgrupa";					$sVrednosti .= " '" . addslashes(Support::encodeTo1250($podgrupa)) . "'";
						$sPolja .= " mpcena,";					$sVrednosti .= " " . Support::replace_empty_numeric($mpcena,1,$kurs,$valuta_id_nc) . ",";
						if(!empty($web_cena)){
						$sPolja .= " web_cena,";				$sVrednosti .= " " . Support::replace_empty_numeric($web_cena,1,$kurs,$valuta_id_nc) . ",";
						}else{
						$sPolja .= " web_cena,";				$sVrednosti .= " " . Support::replace_empty_numeric($mpcena,1,$kurs,$valuta_id_nc) . ",";						}
						$sPolja .= " cena_nc";					$sVrednosti .= " " . Support::replace_empty_numeric($mpcena,1,$kurs,$valuta_id_nc) . "";


						DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");		

					}else{
						$podgrupa = $model;
					}
	            }



			}
			Support::queryExecute($dobavljac_id,array('i','u'),array(),array());
			
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}

	}

	public static function executeShort($dobavljac_id,$kurs=null,$extension=null){

		if($extension==null){
			$products_file = "files/gorenjemka/gorenjemka_excel/gorenjemka.xlsx";
			$continue = false;
            if(File::exists($products_file)){
                $continue = true;
            }
		}else{
			$continue = true;
			$products_file = 'files/import.'.$extension;			
		}

		if($continue){
			Support::initQueryExecute();

			$valuta_id_nc = DB::select("SELECT valuta_id FROM vrsta_cena WHERE vrsta_cena_id = 1")[0]->valuta_id;
			if($kurs==null){
				$kurs = DB::select("SELECT ziralni FROM kursna_lista WHERE kursna_lista_id = (SELECT MAX(kursna_lista_id) FROM kursna_lista)")[0]->ziralni;
			}

	        $excelReader = PHPExcel_IOFactory::createReaderForFile($products_file);
	        $excelObj = $excelReader->load($products_file);
	        $worksheet = $excelObj->getSheet(0);
	        $lastRow = $worksheet->getHighestRow();

            $begin = false;
            $podgrupa = 'NULL';
	        for ($row = 1; $row <= $lastRow; $row++) {
	            $model = $worksheet->getCell('A'.$row)->getValue();
	            $sifra = $worksheet->getCell('B'.$row)->getValue();
	            $naziv = $worksheet->getCell('C'.$row)->getValue();
				$cena_nc = $worksheet->getCell('D'.$row)->getValue();
	            $rabat = $worksheet->getCell('F'.$row)->getValue();
	            $akcijski_rabat = $worksheet->getCell('G'.$row)->getValue();
	            $mpcena = $worksheet->getCell('H'.$row)->getValue();
	            $akcijska_mpcena = $worksheet->getCell('I'.$row)->getValue();

	            if($row!=1){
		            if($worksheet->getCell('A'.($row-1))->getValue()=='Model'){
		            	$begin = true;
		            }
	            }
	            if($begin){
					if(isset($model) && isset($sifra) && isset($naziv) && isset($rabat) && isset($cena_nc) && is_numeric($cena_nc) && isset($mpcena) && is_numeric($mpcena)){

						$rabat = (float) str_replace('%','',$rabat);
						if(isset($akcijski_rabat)){
							$akcijski_rabat = (float) str_replace('%','',$akcijski_rabat);
							$cena_nc = floatval($cena_nc)*(1-$akcijski_rabat/100);
						}else{
							$cena_nc = floatval($cena_nc)*(1-$rabat/100);
						}
						if(isset($akcijska_mpcena)){
							$mpcena = $akcijska_mpcena;
						}

						$sPolja = '';
						$sVrednosti = '';
						$sPolja .= " partner_id,";				$sVrednosti .= " " . $dobavljac_id . ",";
						$sPolja .= " sifra_kod_dobavljaca,";	$sVrednosti .= " '" . addslashes(Support::encodeTo1250(trim($sifra))) . "',";
						$sPolja .= " kolicina,";				$sVrednosti .= " " . number_format(1.00, 2, '.', '') . ",";
						$sPolja .= " mpcena,";					$sVrednosti .= " " . number_format(Support::replace_empty_numeric($mpcena,1,$kurs,$valuta_id_nc),2, '.', '') . ",";
						$sPolja .= " pmp_cena,";				$sVrednosti .= " " . number_format(Support::replace_empty_numeric($mpcena,1,$kurs,$valuta_id_nc),2, '.', '') . ",";
						$sPolja .= " cena_nc";					$sVrednosti .= " " . number_format(Support::replace_empty_numeric($cena_nc,1,$kurs,$valuta_id_nc),2, '.', '') . "";

						DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");		

					}else{
						$podgrupa = $model;
					}
	            }
			}

			//Support::queryShortExecute($dobavljac_id);
			//Brisemo fajl
			if($extension!=null){
				File::delete('files/import.'.$extension);
			}else{
                if(File::exists($products_file)){
                    File::delete($products_file);
                }				
			}
		}
	}


}