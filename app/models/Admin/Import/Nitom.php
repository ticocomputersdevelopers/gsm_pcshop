<?php
namespace Import;
use Import\Support;
use DB;
use AdminOptions;
use File;

class Nitom {

	public static function execute($dobavljac_id,$extension=null){
		Support::initQueryExecute();
		
		if($extension==null){
			if(Support::autoLink($dobavljac_id) != null){
				Support::autoDownload(Support::autoLink($dobavljac_id),'files/nitom/nitom_xml/nitom.xml');
			}
			$file_name = Support::file_name("files/nitom/nitom_xml/");
			
			if($file_name!==false){
				$products = simplexml_load_file("files/nitom/nitom_xml/".$file_name);
			}else{
				$products = array();
			}
		}else{
			$file = AdminOptions::base_url().'files/nitom/nitom_xml/nitom.xml';
			$products = simplexml_load_file($file);			
		}



		foreach ($products as $product) {
			
			//karakteristike
			//$specifikacija=$product->xpath('Specifikacija/Spec');			
			
			// foreach ($specifikacija as $el) {
				
			// 	$parent_group=$el->attributes()->title;
				
			// 		$sPolja = '';
			// 		$sVrednosti = '';
			// 		$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
			// 		$sPolja .= "sifra_kod_dobavljaca,";		$sVrednosti .= "" . Support::quotedStr(Support::encodeTo1250( $product->code)) . ",";	
			// 		$sPolja .= "karakteristika_naziv,";		$sVrednosti .= "" . Support::quotedStr(pg_escape_string(trim(Support::encodeTo1250(($parent_group))))) . ",";
			// 		$sPolja .= "karakteristika_vrednost";	$sVrednosti .= "'" . trim(Support::encodeTo1250(pg_escape_string($el))) . "'";
						
			// 		DB::statement("INSERT INTO dobavljac_cenovnik_karakteristike_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");
				
			// }
			
			//slike
			$images = $product->xpath('slike/slika');
			$flag_slika_postoji = "0";
			$i=0;
			foreach ($images as $slika){
				if($i==0){
					DB::statement("INSERT INTO dobavljac_cenovnik_slike_temp(partner_id,sifra_kod_dobavljaca,putanja,akcija)VALUES(".$dobavljac_id.",".Support::quotedStr($product->code).",'".Support::encodeTo1250($slika)."',1 )");
				}else{
					DB::statement("INSERT INTO dobavljac_cenovnik_slike_temp(partner_id,sifra_kod_dobavljaca,putanja,akcija)VALUES(".$dobavljac_id.",".Support::quotedStr($product->code).",'".Support::encodeTo1250($slika)."',0 )");
				}
				$flag_slika_postoji = "1";
				$i++;
			}	
			
			//proizvodi
			$sPolja = '';
			$sVrednosti = '';
			$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
			$sPolja .= "sifra_kod_dobavljaca,";		$sVrednosti .= "" . Support::quotedStr(Support::encodeTo1250($product->code)) . ",";
			$sPolja .= "naziv,";					$sVrednosti .= "" . Support::quotedStr(Support::encodeTo1250($product->naziv)) . ",";
			$sPolja .= "grupa,";					$sVrednosti .= "" . Support::quotedStr(Support::encodeTo1250($product->nadgrupa)) . ",";		
			$sPolja .= "podgrupa,";					$sVrednosti .= "" . Support::quotedStr(Support::encodeTo1250($product->grupa)) . ",";			
			$sPolja .= "opis,";						$sVrednosti .= "" . Support::quotedStr(Support::encodeTo1250($product->opis)) . ",";
			if(empty($product->opis)){
			$sPolja .= "flag_opis_postoji,"; 		$sVrednosti .= " 0,";	
			}else{
			$sPolja .= "flag_opis_postoji,"; 		$sVrednosti .= " 1,";	
			}
			$sPolja .= "web_flag_karakteristike,";	$sVrednosti .= " 0,";			
			$sPolja .= "kolicina,";					$sVrednosti .= "" . $product->kolicina . ",";			
			$sPolja .= "proizvodjac,";				$sVrednosti .= "" . Support::quotedStr(Support::encodeTo1250($product->proizvodjac)) . ",";			
			$sPolja .= "cena_nc,";					$sVrednosti .= "" . number_format(Support::replace_empty_numeric($product->vpcena,1,1,2),2, '.', '') . ",";			
			$sPolja .= "pmp_cena,";					$sVrednosti .= "" . number_format(Support::replace_empty_numeric($product->mpcena,1,1,2),2, '.', '') . ",";	
			$sPolja .= "pdv,";						$sVrednosti .= "" . $product->pdv.",";
			$sPolja .= "flag_slika_postoji";		$sVrednosti .= "" . $flag_slika_postoji."";
							
			DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");

		}


		Support::queryExecute($dobavljac_id,array('i','u'),array('i'),array('i','u'));
		
		//Brisemo fajl
		$file_name = Support::file_name("files/nitom/nitom_xml/");
		if($extension!=null){
			File::delete('files/import.'.$extension);
		}
		elseif($file_name!==false){
			File::delete("files/nitom/nitom_xml/".$file_name);
		}
	
	}

	public static function executeShort($dobavljac_id,$extension=null){
		Support::initQueryExecute();
		
		if($extension==null){
			if(Support::autoLink($dobavljac_id) != null){
				Support::autoDownload(Support::autoLink($dobavljac_id),'files/nitom/nitom_xml/nitom.xml');
			}
			$file_name = Support::file_name("files/nitom/nitom_xml/");
			
			if($file_name!==false){
				$products = simplexml_load_file("files/nitom/nitom_xml/".$file_name);
			}else{
				$products = array();
			}
		}else{
			$file = AdminOptions::base_url().'files/nitom/nitom_xml/nitom.xml';
			$products = simplexml_load_file($file);			
		}



		foreach ($products as $product) {
			$sPolja = '';
			$sVrednosti = '';
			$sPolja .= "partner_id,";				$sVrednosti .= "" . $dobavljac_id . ",";
			$sPolja .= "sifra_kod_dobavljaca,";		$sVrednosti .= "" . Support::quotedStr(Support::encodeTo1250($product->code)) . ",";
			$sPolja .= "kolicina,";					$sVrednosti .= "" . $product->kolicina . ",";	
			$sPolja .= "cena_nc,";					$sVrednosti .= "" . number_format(Support::replace_empty_numeric($product->vpcena,1,1,2),2, '.', '') . ",";			
			$sPolja .= "pmp_cena";					$sVrednosti .= "" . number_format(Support::replace_empty_numeric($product->mpcena,1,1,2),2, '.', '') . "";	
			
			DB::statement("INSERT INTO dobavljac_cenovnik_temp (" . $sPolja . ") VALUES (" . $sVrednosti . ")");						

		}
		
		//Support::queryShortExecute($dobavljac_id);
		
		//Brisemo fajl
		$file_name = Support::file_name("files/nitom/nitom_xml/");
		if($extension!=null){
			File::delete('files/import.'.$extension);
		}
		elseif($file_name!==false){
			File::delete("files/nitom/nitom_xml/".$file_name);
		}
	
	}

}