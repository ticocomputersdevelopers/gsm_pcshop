<?php

class Vat extends Eloquent
{
    protected $table = 'tarifna_grupa';

    protected $primaryKey = 'tarifna_grupa_id';
    
    public $timestamps = false;

	protected $hidden = array(
            'tarifna_grupa_id',
            'sifra',
            'naziv',
            'porez',
            'vrsta',
            'sifra_connect',
            'tip',
            'active',
            'default_tarifna_grupa'
    	);

	public function getIdAttribute()
	{
	    return isset($this->attributes['tarifna_grupa_id']) ? $this->attributes['tarifna_grupa_id'] : null;
	}
	public function getNameAttribute()
	{
	    return isset($this->attributes['naziv']) ? $this->attributes['naziv'] : null;
	}
    public function getValueAttribute()
    {
        return isset($this->attributes['porez']) ? floatval($this->attributes['porez']) : 0;
    }
    public function getDefaultAttribute()
    {
        return isset($this->attributes['default_tarifna_grupa']) ? $this->attributes['default_tarifna_grupa'] : 0;
    }

	protected $appends = array(
    	'id',
    	'name',
        'value',
        'default'
    	);

    public function articles(){
        return $this->hasMany('Article', 'tarifna_grupa_id');
    }

    public function getIdByName($value,$mapped=[]){
        if(isset($mapped[$value])){
            return $mapped[$value];
        }
        
        if(is_null($vatId = self::where('porez',$value)->pluck('tarifna_grupa_id'))){
            $nextId = self::max('tarifna_grupa_id')+1;

            $vat = new Vat();
            $vat->tarifna_grupa_id = $nextId;
            $vat->naziv = strval($value).'%';
            $vat->porez = $value;
            $vat->sifra = intval($value);
            $vat->save();
            $vatId = $vat->tarifna_grupa_id;
        }

        return $vatId;
    }

    public function mappedByName(){
        $mapped = [];
        foreach(self::select('tarifna_grupa_id','porez')->get() as $vat){
            $mapped[$vat->porez] = $vat->tarifna_grupa_id;
        }
        return $mapped;
    }

}